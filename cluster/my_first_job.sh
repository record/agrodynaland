#!/bin/sh

#$ -o ~/work/output.txt
#$ -e ~/work/error.txt
#$ -q workq
#$ -m bea
# My command lines I want to run on the cluster
vle -P VleAphidsDynaLand aphids.vpz &> log.txt
