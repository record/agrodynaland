#!/usr/bin/bash

mkdir ~/save/usr_1.1.dev 
mkdir ~/save/usr_1.1.dev/bin
mkdir ~/save/usr_1.1.dev/lib
mkdir ~/save/usr_1.1.dev/lib/pkgconfig
mkdir ~/work/.vle_1.1.dev

source ~/save/config_dynaland_vle_1.1.dev

cd $HOME/save
mkdir src
cd src

# BOOST
wget https://sourceforge.net/projects/boost/files/boost/1.58.0/boost_1_58_0.tar.bz2 --no-check-certificate
tar jxf boost_1_58_0.tar.bz2
cd boost_1_58_0

./bootstrap.sh
## (ajouter dans le fichier boost_1_58_0/project-config.jam
## using mpi ;)
echo "using mpi ;" >> project-config.jam
./b2 install --prefix=~/save/usr_1.1.dev/

# VLE
cd $HOME/save/src
git clone https://github.com/vle-forge/vle.git
cd vle
git checkout master1.1
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=~/save/usr_1.1.dev \
-DBOOST_INCLUDEDIR=~/save/usr_1.1.dev/include \
-DBOOST_LIBRARYDIR=~/save/usr_1.1.dev/lib \
-DBoost_NO_BOOST_CMAKE=ON \
-DBoost_NO_SYSTEM_PATHS=ON \
-DWITH_GTK=OFF \
-DWITH_QT=OFF \
-DWITH_GTKSOURCEVIEW=OFF \
..

make -j 48
make install
make test

vle --restart
cd 
perl -pi -e 's/--target all/--target all -- -j48/g' ~/work/.vle_1.1.dev/vle.conf

# éditer le fichier vle.conf
vle.packages.configure=cmake -DBoost_NO_BOOST_CMAKE=ON -DBoost_NO_SYSTEM_PATHS=ON -DBOOST_LIBRARYDIR=~/save/usr_1.1.dev/lib -DBOOST_INCLUDEDIR=~/save/usr_1.1.dev/include -DCMAKE_INSTALL_PREFIX='%1%' -DCMAKE_BUILD_TYPE=RelWithDebInfo -DWITH_DOC=OFF -DWITH_TEST=ON ..

# PACKAGES
cd $HOME/save/src
git clone https://github.com/vle-forge/packages.git
cd packages
git checkout tags/v1.1.2 -b v1.1.2

vle -P vle.output clean rclean configure build test
vle -P vle.extension.decision clean rclean configure build test
vle -P vle.extension.fsa clean rclean configure build test
vle -P vle.extension.difference-equation clean rclean configure build test
vle -P vle.extension.petrinet clean rclean configure build test
vle -P vle.extension.celldevs clean rclean configure build test
vle -P vle.extension.differential-equation clean rclean configure build test
vle -P vle.extension.cellqss clean rclean configure build test
vle -P vle.extension.dsdevs clean rclean configure build test
vle -P ext.muparser clean rclean configure build test
vle -P vle.examples clean rclean configure build test

# RECORDB
git clone http://mulcyber.toulouse.inra.fr/anonscm/git/recordb/recordb.git
cd recordb/pkgs

vle -P tester clean rclean configure build
vle -P glue clean rclean configure build test
vle -P meteo clean rclean configure build test

# STICS
# scp stics_sources.tar.gz user@genotoul.toulouse.inra.fr:save/src
cd $HOME/save/src
tar xzf stics_sources.tar.gz
cd stics_sources
vle -P NativeSticsBin clean rclean configure build
vle -P DynamicsStics clean rclean configure build test

# GDAL
wget http://download.osgeo.org/gdal/2.1.0/gdal-2.1.0.tar.gz
tar xzf gdal-2.1.0.tar.gz
cd gdal-2.1.0
./configure --prefix=~/save/usr_1.1.devmake -j48
make install

#DYNALAND
git clone http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
cd agrodynaland
vle -P VleAphidsDynaLand rclean clean configure build

# qsub -pe parallel_smp $[OMP_NUM_THREADS+1] my_first_job.sh

