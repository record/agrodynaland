# -*- coding: utf-8 -*-
"""
/***************************************************************************
 DynaLand
                                 A QGIS plugin
 DynaLand
                             -------------------
        begin                : 2017-03-23
        copyright            : (C) 2017 by INRA
        email                : guillaume.robaldo@inra.fr
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load DynaLand class from file DynaLand.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .dynaland import DynaLand
    return DynaLand(iface)
