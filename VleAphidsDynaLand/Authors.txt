## Copyright owners

- Guillaume Robaldo
  - Copyright © 2017
  - guillaume.robaldo@inra.fr

- INRA
  - Copyright © 2017
  - French National Institute for Agricultural Research
  - Institut National de la Recherche Agronomique
  - http://www.inra.fr

## Authors

### Integrator / Release manager

- guillaume.robaldo@inra.fr


### Main programmer and founder

- guillaume.robaldo@inra.fr

### Contributors