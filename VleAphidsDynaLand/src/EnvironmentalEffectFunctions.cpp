/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <vector>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_cdf.h>

double GDD(double Tmin, double Tmax, double Tbase){
	return std::max(0.0, ( (Tmax + Tmin) / 2.0 - Tbase) );
}

double DD(double Tmax, double Tmin, double TDlow , double TDup) {
	double DDday = 0;
	if (Tmax <= TDlow ) {
		DDday=0;
	} else {
		if ( Tmin >= TDup) {
			DDday = 0;
		}
		if ( (Tmax >= TDup) && (Tmin > TDlow) ) {
			DDday = TDup - Tmin;
		}
		if ( (Tmax >= TDup) && (Tmin <= TDlow) ) {
			DDday = TDup - TDlow;
		}
		if ( (Tmax < TDup) && (Tmin > TDlow) ) {
			DDday = Tmax - Tmin;
		}
		if ( Tmax < TDup && Tmin <= TDlow ) {
			DDday = Tmax - TDlow;
		}
	}
	return DDday;
}

double sigmaEffect(const double X, const double Xref, const double m, const double X0, const double X1, const double q){
	double W = m *	( std::pow(X - X0, q) * (X1 - X) ) / ( std::pow(Xref - X0, q) * (X1 - Xref ) );
	return W;
}

double minEffect(const double Xmin, const double Xref,
		const double m, const double X0, const double X1, const double q)
{
	double W = 0;
	double Xopt = (X0 + q * X1) / (1.0 + q);

	if(Xmin >= Xopt){
		return 1.0;
	}
	if(Xmin < X0){
		return 0.0;
	}

	double Wmax = sigmaEffect(Xopt, Xref, m, X0, X1, q);
	W = sigmaEffect(Xmin, Xref, m, X0, X1, q) / Wmax;

	return W;
}

double maxEffect(const double Xmax, const double Xref,
		const double m, const double X0, const double X1, const double q)
{
	double W = 0.0;
	double Xopt = (X0 + q * X1) / (1.0 + q);

	if(Xmax <= Xopt){
		return 1.0;
	}
	if(Xmax > X1){
		return 0.0;
	}

	double Wmax = sigmaEffect(Xopt, Xref, m, X0, X1, q);
	W = sigmaEffect(Xmax, Xref, m, X0, X1, q) / Wmax;

	return W;
}

double minimalTemperatureDispersionEffect(const double Tmin, const double Tref,
		const double m, const double T0, const double T1, const double q)
{
	return minEffect(Tmin, Tref, m, T0, T1, q);
}

double maximalTemperatureDispersionEffect(const double Tmax, const double Tref,
		const double m, const double T0, const double T1, const double q)
{
	return maxEffect(Tmax, Tref, m, T0, T1, q);
}

double minimalTemperatureSurvivalEffect(const double Tmin, const double Tref,
		const double m, const double T0, const double T1, const double q)
{
	return minEffect(Tmin, Tref, m, T0, T1, q);
}


double maximalTemperatureSurvivalEffect(const double Tmax, const double Tref,
		const double m, const double T0, const double T1, const double q)
{
	return maxEffect(Tmax, Tref, m, T0, T1, q);
}

double heavyRainSurvivalEffect(const double rain, const double Pref, const double m,
		const double P0, const double P1, const double q)
{
	return maxEffect(rain, Pref, m, P0, P1, q);
}

double temperatureFecondityDirectEffect(const double SDD, const double SDDref, const double m,
		const double SDD0, const double SDD1, const double q){
	return minEffect(SDD, SDDref, m, SDD0, SDD1, q);
}

double rainFecondityDirectEffect(const double rain, const double Pref, const double m,
		const double P0, const double P1, const double q){
	return maxEffect(rain, Pref, m, P0, P1, q);
}


double capacity2competition(double K, double n, double s0){
	double W;
	if(K == 0){
		W = 0.0;
	}else{
		if( ( n == 0) || ((K == 1) && (n == 1)) ){
			W = 1.0;
		}else{
			double k = n / K;
			double a = - std::log(s0) / n;
			W = std::exp(-a * k * n);
		}
	}
	return W;
}

void lm(const std::vector<double>& vx, const std::vector<double>& vy, std::vector<double>& results){
	int n = vx.size();
//	cout<<"n:"<<n<<endl;
	const double* x = vx.data();
	const double* y = vy.data();
	double c0, c1, cov00, cov01, cov11, sumsq;
	gsl_fit_linear(x, 1, y, 1, n, &c0, &c1, &cov00, &cov01, &cov11, &sumsq);

//	cout<<"Coefficients\tEstimate\tStd. Error\tt value\t\tPr(>|t|)"<<endl;

//	double stdev0 = sqrt(cov00);
//	double t0 = c0 / stdev0;
//	double pv0 = 0; //This is the p-value of the constant term
//	if( t0 < 0 ) {
//		pv0 = 2 * ( 1 - gsl_cdf_tdist_P(-t0,n-2) );
//	}else{
//		pv0 = 2 * ( 1 - gsl_cdf_tdist_P(t0,n-2) );
//	}

//	cout<<"Intercept\t"<<c0<<"\t"<<stdev0<<"\t\t"<<t0<<"\t"<<pv0<<endl;

	double stdev1 = sqrt(cov11);
	double t1 = c1 / stdev1;
	double pv1 = 0; //This is the p-value of the linear term
	if( t1 < 0 ){
		pv1 = 2.0 * ( 1.0 - gsl_cdf_tdist_P(-t1,n-2) );
	}else{
		pv1 = 2.0 * ( 1.0 - gsl_cdf_tdist_P(t1,n-2) );
	}

//	cout<<"x       \t"<<c1<<"\t"<<stdev1<<"\t"<<t1<<"\t\t"<<pv1<<endl;
//	cout<<"\n";
//	double dl=n-2;//degrees of liberty
//	double ym=0.25*(y[0]+y[1]+y[2]+y[3]); //Average of vector y
//	double sct=pow(y[0]-ym,2)+pow(y[1]-ym,2)+pow(y[2]-ym,2)+pow(y[3]-ym,2); // sct = sum of total squares
//	double R2=1-sumsq/sct;
//	cout<<"Multiple R-squared: "<<R2<<",    Adjusted R-squared: "<<1-double(n-1)/dl*(1-R2)<<endl;
//	double F=R2*dl/(1-R2);
//	double p_value=1-gsl_cdf_fdist_P(F,1,dl);
//	cout<<"F-statistic:  "<<F<<" on 1 and "<<n-2<<" DF,  p-value: "<<p_value<<endl;

	results.push_back(c1);
	results.push_back(pv1);
}

