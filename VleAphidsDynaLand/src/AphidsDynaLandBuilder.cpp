/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <vle/devs/Executive.hpp>
//#include <vle/devs/ExecutiveDbg.hpp>
#include <vle/vpz/Observable.hpp>
#include <vle/utils/DateTime.hpp>
#include <vle/utils/Trace.hpp>
#include <iostream>
#include <vector>
#include <map>
namespace vd = vle::devs;
namespace vv = vle::value;
namespace vz = vle::vpz;

namespace VleAphidsDynaLand {

class AphidsDynaLandBuilder : public vd::Executive
{
public:
	AphidsDynaLandBuilder(
    	const vd::ExecutiveInit& model,
    	const vd::InitEventList& evts)
    	: vd::Executive(model, evts)
    {
    	TraceModel("AphidsDynaLandBuilder constructing...");

    	if(!evts.exist("stics")){
    		throw vle::utils::VpzError("Missing condition parameter 'stics'");
    	}

		const vv::Set* stics = vv::toSetValue(evts.get("stics"));
		if(stics->empty()){
			throw vle::utils::VpzError("At least one configuration is required in condition parameter 'stics'");
		}

		vv::Set new_stics;
		for(vv::Set::const_iterator it = stics->begin();
				it != stics->end(); it++){
			vv::Map* stics_config = vv::toMapValue(*it);
			if(stics_config->exist("SticsIds")){
				const vv::Set* sticsIds = vv::toSetValue(stics_config->get("SticsIds"));
				if(sticsIds->size() != 2){
					throw vle::utils::VpzError("'SticsIds' must contains 2 integers for ids interval");
				}
				int id_from = sticsIds->getInt(0);
				int id_to = sticsIds->getInt(1);
				TraceModel("Configuring Stics for ids from " + std::to_string(id_from)
					+ " to " + std::to_string(id_to));
				for(int j = id_from; j <= id_to; j++){
					TraceModel("add id '" + std::to_string(j) + "'");
					stics_config->addInt("SticsId", j);
					vv::Value* new_map = stics_config->clone();
					new_stics.add(new_map);
				}
			}else{
				vv::Value* new_map = stics_config->clone();
				new_stics.add(new_map);
			}
		}
		stics = vv::toSetValue(new_stics.clone());
		int i = 0;
		std::set<int> sticsIds;
		for(vv::Set::const_iterator it = stics->begin();
				it != stics->end(); it++){
			++i;
			const vv::Map* stics_config = vv::toMapValue(*it);
			if(!stics_config->exist("SticsId")){
				throw vle::utils::VpzError("Missing key 'SticsId' or 'SticsIds' in condition parameter 'stics' n°" + i);
			}else{
				int sticsId = vv::toInteger(stics_config->get("SticsId"));
				if( (sticsIds.size() > 0) && sticsIds.find(sticsId) != sticsIds.end() ){
					throw vle::utils::VpzError("Duplicate STICS id '" + std::to_string(sticsId) + "'");
				}else{
					sticsIds.insert(sticsId);
				}
			}

			if(!stics_config->exist("SticsInputDir")){
				throw vle::utils::VpzError("Missing key 'SticsInputDir' in condition parameter 'stics' n°" + i);
			}

			if(!stics_config->exist("UserVars")){
				throw vle::utils::VpzError("Missing key 'UserVars' in condition parameter 'stics' n°" + i);
			}
			const vv::Set* UserVars = vv::toSetValue(stics_config->get("UserVars"));
			if(UserVars->empty()){
				throw vle::utils::VpzError("At least one 'UserVars' is required in condition parameter 'stics' n°" + i);
			}
		}

		const vv::Set* prunusIds = vv::toSetValue(evts.get("PrunusIds"));

    	createSTICS(stics);
    	createDynaLand(stics, prunusIds);
    	createConnections(stics);

//    	dump(std::cout);

    	TraceModel("MyBuilder constructed!");
    }

    virtual ~AphidsDynaLandBuilder()
    {
    	TraceModel("AphidsDynaLandBuilder destructing...");
    }

private:

    void createDynaLand(const vv::Set* stics, const vv::Set* prunusIds){
    	TraceModel("createDynaLand...");

		std::vector<std::string> dynaland_inputs;
		std::vector<std::string> dynaland_outputs;
		for(std::vector<std::string>::const_iterator it_out = meteo_outputs.begin();
				it_out != meteo_outputs.end(); ++it_out){
			std::string outputport = (*it_out);
			dynaland_inputs.push_back(outputport);
		}
    	/**
    	 * add meteo input port
    	 */
		for(vv::Set::const_iterator it = stics->begin();
						it != stics->end(); it++){
			const vv::Map* stics_config = vv::toMapValue(*it);
			int sticsId = vv::toInteger(stics_config->get("SticsId"));
			std::string sticsIdstr = std::to_string(sticsId);
			TraceModel("SticsId : " + sticsIdstr);
			// TODO: use vector of variable
			dynaland_outputs.push_back( ("force_lai_" + sticsIdstr) );
			dynaland_outputs.push_back( ("wlai_" + sticsIdstr) );
			dynaland_outputs.push_back( ("Isexe_" + sticsIdstr) );
			dynaland_outputs.push_back( ("cost_" + sticsIdstr) );
			dynaland_outputs.push_back( ("kmax_" + sticsIdstr) );

			const vv::Set* UserVars = vv::toSetValue(stics_config->get("UserVars"));
			TraceModel("UserVars : ");
			for(vv::Set::const_iterator it_var = UserVars->begin();
					it_var != UserVars->end(); it_var++){
				std::string sticsVar = vv::toString(*it_var);
				TraceModel("   " + sticsVar);
				dynaland_inputs.push_back( (sticsVar + "_" + sticsIdstr) );
			}
		}

		for(vv::Set::const_iterator it = prunusIds->begin();
								it != prunusIds->end(); it++){
			int prunusId = vv::toInteger(*it);
			std::string prunus_id = std::to_string(prunusId);
			// TODO: use vector of variable
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".pre_lai") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".lai") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".wlai") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".interest") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".Nfol") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".Nmobil") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".Bdry") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".Btotal") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".phasePheno") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".nJI") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".GDD") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".SDD") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".Tmoy_moy") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".Isexe") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".kmax") );
			dynaland_outputs.push_back( ("PRUNUS_" + prunus_id + ".cost") );

		}

    	/**
		* create observable
		*/
		observables().add(vle::vpz::Observable("obs_dynaland"));

		/**
		* create conditions
		*/
		std::vector<std::string> conds = {"cond_dynaland", "cond_DE"};
		vz::Condition& acond = conditions().get("cond_dynaland");
		acond.addValueToPort("stics", stics->toSet());


		/**
		 *  create model
		 */
		createModel(modelDynaLand,
					dynaland_inputs,
					dynaland_outputs,
					"dyn_dynaland",
					conds,
					"obs_dynaland");

		TraceModel("createModel Done!");

		/**
		 *  add output to stics view
		 */
		for(std::vector<std::string>::const_iterator it_out = dynaland_outputs.begin();
				it_out != dynaland_outputs.end(); ++it_out){
			addObservableToView(modelDynaLand, *it_out, "crops");
		}
		TraceModel("createDynaLand done!");
    }

    void createSTICS(const vv::Set* stics){
    	TraceModel("create STICS...");

    	/**
    	 * add meteo input port
    	 */
		for(std::vector<std::string>::const_iterator it_out = stics_meteo_outputs.begin();
				it_out != stics_meteo_outputs.end(); ++it_out){
			std::string outputport = (*it_out);
			stics_inputs.push_back(outputport);
		}

    	for(vv::Set::const_iterator it = stics->begin();
    					it != stics->end(); it++){

    		const vv::Map* stics_config = vv::toMapValue(*it);
			int sticsId = vv::toInteger(stics_config->get("SticsId"));
			std::string sticsIdstr = std::to_string(sticsId);

			TraceModel("SticsID : " + sticsIdstr);

			std::string modelStics = "STICS_" + sticsIdstr;

			/**
			* create observable
			*/
			observables().add(vle::vpz::Observable("obs_" + modelStics));

			/**
			* create conditions
			*/
			std::vector<std::string> conds = {"cond_DE", "cond_PkgName", "cond_dbgLog"};
			std::string conditionName = "cond_STICS_" + sticsIdstr;
			vz::Condition& acond = conditions().add(conditionName);
			conds.push_back(conditionName);

			for(vv::Map::const_iterator it_cond = stics_config->begin();
					it_cond != stics_config->end(); ++it_cond){
				const std::string& portname = it_cond->first;
				if(portname != "UserVars"){
					if(portname != "SticsId"){
						switch(it_cond->second->getType()){
							case vv::Value::MAP: acond.addValueToPort(portname, it_cond->second->toMap());break;
							case vv::Value::SET: acond.addValueToPort(portname, it_cond->second->toSet());break;
							case vv::Value::DOUBLE: acond.addValueToPort(portname, it_cond->second->toDouble());break;
							case vv::Value::BOOLEAN: acond.addValueToPort(portname, it_cond->second->toBoolean());break;
							case vv::Value::STRING: acond.addValueToPort(portname, it_cond->second->toString());break;
							case vv::Value::INTEGER: acond.addValueToPort(portname, it_cond->second->toInteger());break;
							default: acond.addValueToPort(portname, it_cond->second);break;
						}
					}else{
						std::string val = std::to_string(vv::toInteger(it_cond->second));
						acond.addValueToPort(portname, vv::String::create(val));
					}
				}
			}

			std::string sticsInputDir = vv::toString(stics_config->get("SticsInputDir"));
			TraceModel("SticsInputDir : " + sticsInputDir);

			std::vector<std::string> stics_outputs;
			TraceModel("UserVars : ");
			const vv::Set* UserVars = vv::toSetValue(stics_config->get("UserVars"));
			for(vv::Set::const_iterator it_var = UserVars->begin();
					it_var != UserVars->end(); it_var++){
				std::string sticsVar = vv::toString(*it_var);
				TraceModel("   " + sticsVar);
				stics_outputs.push_back( sticsVar );
			}

			/**
			 *  create model
			 */
			createModel(modelStics,
						stics_inputs,
						stics_outputs,
						"dyn_stics",
						conds,
						"obs_" + modelStics);

			/**
			 *  add result to stics view
			 */
			for(std::vector<std::string>::iterator it_out = stics_outputs.begin();
					it_out != stics_outputs.end(); ++it_out){
				TraceModel("output " + modelStics + ":" + *it_out);
				addObservableToView(modelStics, *it_out, "crops");
			}
    	}
    }

    void createConnections(const vv::Set* stics){

    	/**
		 * add connections between meteo and dynaland
		 */
		TraceModel("add connections between meteo and dynaland: ");
		for(std::vector<std::string>::const_iterator it_out = meteo_outputs.begin();
				it_out != meteo_outputs.end(); ++it_out){
			std::string outputport = (*it_out);
			std::string inputport = outputport;
			TraceModel("   " + outputport + " -> " + inputport);
			addConnection(modelMeteo,
						  outputport,
						  modelDynaLand,
						  inputport);
		}

    	for(vv::Set::const_iterator it = stics->begin();
    					it != stics->end(); it++){
    		const vv::Map* stics_config = vv::toMapValue(*it);

			int sticsId = vv::toInteger(stics_config->get("SticsId"));
			std::string sticsIdstr = std::to_string(sticsId);
			std::string modelStics = ("STICS_" + sticsIdstr);

			TraceModel("configure " + modelStics + "...");
			/**
			 * add connections between meteo and stics model
			 */
			TraceModel("add connections between meteo and stics model: ");
			for(std::vector<std::string>::const_iterator it_out = stics_meteo_outputs.begin();
					it_out != stics_meteo_outputs.end(); ++it_out){
				std::string outputport = (*it_out);
				std::string inputport = outputport;
				TraceModel("   " + outputport + " -> " + inputport);
				addConnection(modelMeteo,
							  outputport,
							  modelStics,
							  inputport);
			}

			/**
			* add connections between stics model and dynaland
			*/
			TraceModel("add connections between stics model and dynaland : ");
			const vv::Set* UserVars = vv::toSetValue(stics_config->get("UserVars"));
			for(vv::Set::const_iterator it_var = UserVars->begin();
					it_var != UserVars->end(); it_var++){
				std::string sticsVar = vv::toString(*it_var);
				TraceModel("   " + sticsVar + " -> " + sticsVar + "_" + sticsIdstr);
				addConnection(modelStics,
							  sticsVar,
							  modelDynaLand,
							  sticsVar + "_" + sticsIdstr);
			}

			/**
			* add connections between dynaland and stics model
			*/
			TraceModel("add connections between dynaland and stics model : ");
			TraceModel("   force_lai_" + sticsIdstr + " -> force_lai");
			addConnection(modelDynaLand,
						  "force_lai_" + sticsIdstr,
						  modelStics,
						  "force_lai");
    	}
    }



private:
	std::string modelMeteo = "meteo";
	std::string modelDynaLand = "DynaLand";
	std::vector<std::string> meteo_outputs = {"Co2", "ETP", "RG", "Rain", "Tmax", "Tmin", "Sun"};
	std::vector<std::string> stics_meteo_outputs = {"Co2", "ETP", "RG", "Rain", "Tmax", "Tmin"};
	std::vector<std::string> stics_inputs = {"force_lai"};
};

} // namespace VleSticsDynaLand

DECLARE_EXECUTIVE(VleAphidsDynaLand::AphidsDynaLandBuilder)
