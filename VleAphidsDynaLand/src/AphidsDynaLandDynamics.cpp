/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/Logger.hpp>
#include <dynaland/MainFunction.hpp>
#include <dynaland/Map.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/Individus.hpp>
#include <dynaland/Patch.hpp>
#include <dynaland/Process.hpp>
#include <dynaland/Matrix.hpp>
#include <dynaland/random/RandomGenerator.hpp>

#include <EnvironmentalEffectFunctions.cpp>

//#include <vle/extension/DifferenceEquation.hpp>
#include <vle/extension/difference-equation/Multiple.hpp>
//#include <vle/extension/difference-equation/MultipleDbg.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Trace.hpp>

#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <deque>
#include <map>
#include <algorithm>

#if defined(_WIN16) | defined(_WIN32) | defined(_WIN64)
#define PATH_SEP "\\"
#else
#define PATH_SEP "/"
#endif

namespace tmp = vle;
namespace vd = vle::devs;
namespace ve = vle::extension;
namespace vv = vle::value;
namespace vu = vle::utils;

using namespace std;
using namespace dynaland;

namespace VleAphidsDynaLand {

struct stics_var {
	ve::DifferenceEquation::Multiple::Var force_lai;
	ve::DifferenceEquation::Multiple::Var wlai;
	ve::DifferenceEquation::Multiple::Var Isexe;
	ve::DifferenceEquation::Multiple::Var cost;
	ve::DifferenceEquation::Multiple::Var kmax;
	std::map<std::string, ve::DifferenceEquation::Multiple::Sync> syncs;
};

struct prunus_var {
	ve::DifferenceEquation::Multiple::Var pre_lai;
	ve::DifferenceEquation::Multiple::Var lai;
	ve::DifferenceEquation::Multiple::Var wlai;
	ve::DifferenceEquation::Multiple::Var Nmobil;
	ve::DifferenceEquation::Multiple::Var interest;
	ve::DifferenceEquation::Multiple::Var Nfol;
	ve::DifferenceEquation::Multiple::Var GDD;
	ve::DifferenceEquation::Multiple::Var SDD;
	ve::DifferenceEquation::Multiple::Var Tmoy_moy;
	ve::DifferenceEquation::Multiple::Var Bdry;
	ve::DifferenceEquation::Multiple::Var Btotal;
	ve::DifferenceEquation::Multiple::Var nJI;
	ve::DifferenceEquation::Multiple::Var phasePheno;
	ve::DifferenceEquation::Multiple::Var Isexe;
	ve::DifferenceEquation::Multiple::Var cost;
	ve::DifferenceEquation::Multiple::Var kmax;

	// std::map<std::string, ve::DifferenceEquation::Multiple::Sync> syncs;
};

enum PRUNUS_PHENO {BEFORE_BOURGEON = 0, BOURGEON = 1, LEAF = 2, LEAF_SENE = 3};
enum EFFECT_TYPE {DISPERSION_DIST = 0, DISPERSION = 1, SURVIVAL = 2, FECONDITY = 3};

class AphidsDynaLandDynamics : public ve::DifferenceEquation::Multiple
{
public:
	AphidsDynaLandDynamics(
       const vd::DynamicsInit& atom,
       const vd::InitEventList& evts)
        : ve::DifferenceEquation::Multiple(atom, evts)
    {
		TraceModel("DynaLand constructing...");
//    	cout << "Running..." << endl;
//    	vu::Trace::setLogFile("test.log");
//    	vu::Trace::send("MetaConnectDyna", vu::TraceLevelOptions::TRACE_LEVEL_MODEL);

    	// VAR
    	if(evts.exist("stics")){
    		const vv::Set* vstics = vv::toSetValue(evts.get("stics"));
    		for(vv::Set::const_iterator it = vstics->begin();
    						it != vstics->end(); it++){
				const vv::Map* stics_config = vv::toMapValue(*it);
				int sticsId = vv::toInteger(stics_config->get("SticsId"));
				std::string sticsIdstr = std::to_string(sticsId);

				TraceModel("configure stics " + sticsIdstr + " : ");
				stics_var astics;
				// TODO: use vector of variable
				astics.force_lai = createVar( ("force_lai_" + sticsIdstr) );
				astics.wlai = createVar( ("wlai_" + sticsIdstr) );
				astics.Isexe = createVar( ("Isexe_" + sticsIdstr) );
				astics.cost = createVar( ("cost_" + sticsIdstr) );
				astics.kmax = createVar( ("kmax_" + sticsIdstr) );

				const vv::Set* UserVars = vv::toSetValue(stics_config->get("UserVars"));
				TraceModel("UserVars : ");
				for(vv::Set::const_iterator it_var = UserVars->begin();
						it_var != UserVars->end(); it_var++){
					std::string sticsVar = vv::toString(*it_var);
					TraceModel("   " + sticsVar);

					astics.syncs.insert(
						std::make_pair(sticsVar,
								createSync( (sticsVar + "_" + sticsIdstr) )));
				}

				stics.insert(std::make_pair(sticsIdstr, astics));

				newMeteoHistory(sticsIdstr);
			}
    	}
		newMeteoHistory("default");

		prunus_var aprunus;

		if(evts.exist("PrunusIds")){
			const vv::Set* prunusIds = vv::toSetValue(evts.get("PrunusIds"));
			for(vv::Set::const_iterator it = prunusIds->begin();
					it != prunusIds->end(); it++)
			{
				int prunusId = vv::toInteger(*it);
				std::string prunus_id = std::to_string(prunusId);
				// TODO: use vector of variable
				aprunus.pre_lai        = createVar( ("PRUNUS_" + prunus_id + ".pre_lai") );
				aprunus.lai        = createVar( ("PRUNUS_" + prunus_id + ".lai") );
				aprunus.wlai       = createVar( ("PRUNUS_" + prunus_id + ".wlai") );
				aprunus.Nmobil     = createVar( ("PRUNUS_" + prunus_id + ".Nmobil") );
				aprunus.Nfol       = createVar( ("PRUNUS_" + prunus_id + ".Nfol") );
				aprunus.interest   = createVar( ("PRUNUS_" + prunus_id + ".interest") );
				aprunus.GDD        = createVar( ("PRUNUS_" + prunus_id + ".GDD") );
				aprunus.SDD        = createVar( ("PRUNUS_" + prunus_id + ".SDD") );
				aprunus.Tmoy_moy   = createVar( ("PRUNUS_" + prunus_id + ".Tmoy_moy") );
				aprunus.Bdry       = createVar( ("PRUNUS_" + prunus_id + ".Bdry") );
				aprunus.Btotal     = createVar( ("PRUNUS_" + prunus_id + ".Btotal") );
				aprunus.nJI        = createVar( ("PRUNUS_" + prunus_id + ".nJI") );
				aprunus.phasePheno = createVar( ("PRUNUS_" + prunus_id + ".phasePheno") );
				aprunus.Isexe      = createVar( ("PRUNUS_" + prunus_id + ".Isexe") );
				aprunus.kmax       = createVar( ("PRUNUS_" + prunus_id + ".kmax") );
				aprunus.cost       = createVar( ("PRUNUS_" + prunus_id + ".cost") );
				prunus.insert(std::make_pair(prunus_id, aprunus));
			}
		}


    	for(std::vector<std::string>::const_iterator it_out = meteo_outputs.begin();
    						it_out != meteo_outputs.end(); ++it_out){
    		std::string outputport = (*it_out);
    		meteo.insert(std::make_pair(outputport, createSync(outputport)));
    	}

    	// PARAMETERS
    	readDynaLandCondition(evts);
    	TraceDynaLandCondition();

    	std::string data_dir = vu::Package("VleAphidsDynaLand").getDataDir(vu::VLE_PACKAGE_TYPE::PKG_BINARY);
    	TraceModel("DATA DIR: " + data_dir);
    	int duration = 0;
    	individus_last_id = 0;
    	if(evts.exist("parameters_file")){
    		TraceModel("parameters_file : " + parameters_file);
        	std::string param_file =  data_dir + PATH_SEP + parameters_file;
        	TraceModel("param_file : " + param_file);
    		MainFunction::initFromFile(param_file, data_dir, &pop01, &map01, &myProcess, &duration, &individus_last_id);
    	}else{
    		std::string s_female_file = data_dir + PATH_SEP + survival_female_file;
    		std::string f_female_file = data_dir + PATH_SEP + fecondity_female_file;

    		std::string file_landcover01 =  data_dir + PATH_SEP + file_landcover;
    		std::string file_nomenclature01 =  data_dir + PATH_SEP + file_nomenclature;
    		std::string file_pop01 = data_dir + PATH_SEP + file_population;

    		std::string dir_results01 = dir_results + PATH_SEP;

    		MatrixCsv msurvival_female = MatrixCsv(s_female_file, nb_classes);
    		MatrixCsv mfecondity_female = MatrixCsv(f_female_file, nb_classes);

    		// parameters
    		TraceModel(vle::fmt("seed: %1%") % seed);
    		TraceModel(vle::fmt("random_type: %1%") % random_type);

    		TraceModel(vle::fmt("survival_female_file: %1%") % survival_female_file);
    		TraceModel(vle::fmt("fecondity_female_file: %1%") % fecondity_female_file);

    		TraceModel(vle::fmt("perceptual_range: %1%") % perceptual_range);
    		TraceModel(vle::fmt("memory_size: %1%") % memory_size);
    		TraceModel(vle::fmt("persistence_memory: %1%") % persistence_memory);

    		TraceModel(vle::fmt("file_landcover: %1%") % file_landcover);
    		TraceModel(vle::fmt("file_nomenclature: %1%") % file_nomenclature);
    		TraceModel(vle::fmt("file_population: %1%") % file_population);
    		TraceModel(vle::fmt("dir_results: %1%") % dir_results);

    		TraceModel(vle::fmt("log_level: %1%") % log_level);

    		TraceModel(vle::fmt("dispersion_on: %1%") % dispersion_on);
    		TraceModel(vle::fmt("survival_on: %1%") % survival_on);
    		TraceModel(vle::fmt("reproduction_on: %1%") % reproduction_on);

    		TraceModel(vle::fmt("writeDynPop: %1%") % writeDynPop);
    		TraceModel(vle::fmt("writePatchPop: %1%") % writePatchPop);
    		TraceModel(vle::fmt("writePop: %1%") % writePop);
    		TraceModel(vle::fmt("writeVisits: %1%") % writeVisits);
    		TraceModel(vle::fmt("writePaths: %1%") % writePaths);
    		TraceModel(vle::fmt("writeDensity: %1%") % writeDensity);

    		MainFunction::init(&pop01, &map01, &myProcess, &duration, &individus_last_id,
    				 log_level,
    				 0,
    				 nb_classes,
    				 sex_ratio,
    				 sexual_reproduction,
					 pop_sex,

    				 dispersion_female,
    				 msurvival_female,
    				 mfecondity_female,
    				 1,

    				 dispersion_female,
    				 msurvival_female,
    				 mfecondity_female,
    				 1,

    				 1,
    				 1,
    				 1,

    				 fecondity_contrib_female,
    				 perceptual_range,
    				 memory_size,
    				 persistence_memory,
    				 persistence_target,
    				 disp_dmax,

    				 file_landcover01, file_nomenclature01, file_pop01,

    				 seed, random_type, dispersion_on, survival_on, reproduction_on, dir_results01);
    	}
        pop01.setFecondityMax(fecondity_max);
        vector<p_range> vdispers;
        for(int cl=0; cl<pop01.getNbClasses(); cl++){
        	vdispers.push_back({Dmin, Dmax});
        }
        pop01.dispersion_female = vdispers;

        TraceModel("DynaLand constructed!");
    }

    virtual ~AphidsDynaLandDynamics()
    {
    	TraceModel("DynaLand destructing...");

    	MainFunction::finish(&pop01, &map01, &myProcess);

    	TraceModel("DynaLand destructed");
    }

    virtual void compute(const vd::Time& /*time*/)
    {
    	TraceModel("DynaLand compute");

    	life_cycle_counter++;
    	myProcess.time += 1;

    	Logger::info("----------------------------------\n");
    	Logger::info("----- time %d\n\n", myProcess.time);

    	processMeteo();
    	processNJI();
    	processPrunus();

    	if(life_cycle_counter == life_cycle_duration){
    		processIsexeMap(&map01, &pop01);

    		myProcess.clearResults(&pop01);

    		if(myProcess.dispersion_on){
    			Logger::info("---------------\n");
    			Logger::info("DISPERSION\n");
    			map01.clearCosts();
    //			processEnvironmentalEffect(DISPERSION_DIST, &map01, &pop01);

    //			int step_max = pop01.getDispersionDmax();
    			int step_max = disp_dmax;
    			Logger::info("max step: %d\n", step_max);
    			for(int step=0; step < step_max; step++)
    			{
    				processEnvironmentalEffect(DISPERSION, &map01, &pop01);

    				Logger::info("\n--> SMS STEP %d\n", step);
    				int nb_dispersers = myProcess.dispersion(&map01, &pop01, step);
    				if(nb_dispersers == 0){
    					break;
    				}
    			}
    			pop01.print2();
    		}
    	}

    	processDeveloppement(&map01, &pop01);

    	if(life_cycle_counter == life_cycle_duration){


    		if(myProcess.survival_on){
    			Logger::info("---------------\n");
    			Logger::info("SURVIVAL\n");
    			processEnvironmentalEffect(SURVIVAL, &map01, &pop01);

    			myProcess.survival(&map01, &pop01);
    			pop01.print2();
    		}

    		if(myProcess.reproduction_on){
    			Logger::info("---------------\n");
    			Logger::info("REPRODUCTION\n");

    			processEnvironmentalEffect(FECONDITY, &map01, &pop01);

    			individus_last_id = myProcess.reproduction(&map01, &pop01, individus_last_id);
    			pop01.print2();
    		}

    		if(writeDynPop) myProcess.writeDynPop(myProcess.time, &pop01);
    		if(writePatchPop) myProcess.writePatchPop(myProcess.time, &map01, &pop01);
    		if(writePop) myProcess.writePop(&map01, &pop01);
//    		myProcess.writeCosts(&map01, &pop01);
//    		myProcess.writeKs(&map01, &pop01);
    		if(writeVisits) myProcess.writeVisits(&map01, &pop01);
    		if(writePaths) myProcess.writePaths(&map01, &pop01);
    		if(writeDensity) myProcess.writeDensity(&map01, &pop01);
    		if(writeKmaxPatch) myProcess.writeKPatches(&map01, &pop01);
    		life_cycle_counter = 0;

    	}

    	processSticsAsphidEffect(&map01, &pop01);
    	processPrunusAsphidEffect(&map01, &pop01);
    }


void readDynaLandCondition(const vd::InitEventList& evts){

	if(evts.exist("parameters_file")){
		parameters_file = vv::toString(evts.get("parameters_file"));
	}else{

		// parameters
		seed = vv::toInteger(evts.get("seed"));
		random_type = vv::toInteger(evts.get("random_type"));

		survival_female_file = vv::toString(evts.get("survival_female_file"));
		fecondity_female_file = vv::toString(evts.get("fecondity_female_file"));

		perceptual_range = vv::toInteger(evts.get("perceptual_range"));
		memory_size = vv::toInteger(evts.get("memory_size"));
		persistence_memory = vv::toDouble(evts.get("persistence_memory"));

		file_landcover = vv::toString(evts.get("file_landcover"));
		file_nomenclature = vv::toString(evts.get("file_nomenclature"));
		file_population = vv::toString(evts.get("file_population"));
		dir_results = vv::toString(evts.get("dir_results"));

		log_level = vv::toInteger(evts.get("log_level"));

		dispersion_on = vv::toInteger(evts.get("dispersion_on"));
		survival_on = vv::toInteger(evts.get("survival_on"));
		reproduction_on = vv::toInteger(evts.get("reproduction_on"));

		writeDynPop = vv::toBoolean(evts.get("writeDynPop"));
		writePatchPop = vv::toBoolean(evts.get("writePatchPop"));
		writePop = vv::toBoolean(evts.get("writePop"));
		writeVisits = vv::toBoolean(evts.get("writeVisits"));
		writePaths = vv::toBoolean(evts.get("writePaths"));
		writeDensity = vv::toBoolean(evts.get("writeDensity"));
		writeKmaxPatch = vv::toBoolean(evts.get("writeKmaxPatch"));
	}

	time_step = (int) vv::toDouble(evts.get("time-step"));

	// durée du cycle de vie
	life_cycle_duration = vv::toInteger(evts.get("life_cycle_duration"));

	// taille de l'historique des données météo pour mean/min/max
	meteo_mean_size = (unsigned int) vv::toInteger(evts.get("meteo_mean_size"));

	// taille de l'historique des données météo pour linear regression
	meteo_trend_size = (unsigned int) vv::toInteger(evts.get("meteo_trend_size"));

	meteo_history_size = std::max(meteo_mean_size, meteo_trend_size);

	// limites inférieure/supérieure de température de développement
	TDLow = vv::toDouble(evts.get("TDLow"));
	TDup = vv::toDouble(evts.get("TDup"));

	ageLow0 = vv::toInteger(evts.get("ageLow0"));
	ageUp0 = vv::toInteger(evts.get("ageUp0"));

	ageJ = vv::toInteger(evts.get("ageJ"));
	ageA = vv::toInteger(evts.get("ageA"));

	SDDadult = vv::toDouble(evts.get("SDDadult"));
	SDDdie = vv::toDouble(evts.get("SDDdie"));

	// effet de la ressource
	Nj = vv::toDouble(evts.get("Nj"));
	Na = vv::toDouble(evts.get("Na"));
	Ncapt = vv::toDouble(evts.get("Ncapt"));
	sP = vv::toInteger(evts.get("sP"));

	// Coefficient de compétition
	s0 = vv::toDouble(evts.get("s0"));

	// surface
	surf_cellule = vv::toDouble(evts.get("surf_cellule")); // TODO get it from map
	surf = surf_cellule / 10000;

	/**
	 * Le taux de dispersion
	 */
	Dmin = vv::toDouble(evts.get("Dmin"));
	Dmax = vv::toDouble(evts.get("Dmax"));

	disp_dmax = vv::toInteger(evts.get("disp_dmax"));

	cost_stics0 = vv::toDouble(evts.get("cost_stics0"));
	cost_stics1 = vv::toDouble(evts.get("cost_stics1"));

	cost_prunus0 = vv::toDouble(evts.get("cost_prunus0"));
	cost_prunus1 = vv::toDouble(evts.get("cost_prunus1"));

	/**
	 * Survie des oeufs (S11)
	 */

	// survie max
	Smax11 = vv::toDouble(evts.get("Smax11"));

	// Effet des températures minimales
	S11_Tref_tmin = vv::toDouble(evts.get("S11_Tref_tmin"));
	S11_m_tmin = vv::toDouble(evts.get("S11_m_tmin"));
	S11_T0_tmin = vv::toDouble(evts.get("S11_T0_tmin"));
	S11_T1_tmin = vv::toDouble(evts.get("S11_T1_tmin"));
	S11_q_tmin = vv::toDouble(evts.get("S11_q_tmin"));

	// Effet des températures maximales
	S11_Tref_tmax = vv::toDouble(evts.get("S11_Tref_tmax"));
	S11_m_tmax = vv::toDouble(evts.get("S11_m_tmax"));
	S11_T0_tmax = vv::toDouble(evts.get("S11_T0_tmax"));
	S11_T1_tmax = vv::toDouble(evts.get("S11_T1_tmax"));
	S11_q_tmax = vv::toDouble(evts.get("S11_q_tmax"));

	// Effet des fortes précipitations
	S11_Pref = vv::toDouble(evts.get("S11_Pref"));
	S11_Pm = vv::toDouble(evts.get("S11_Pm"));
	S11_P0 = vv::toDouble(evts.get("S11_P0"));
	S11_P1 = vv::toDouble(evts.get("S11_P1"));
	S11_Pq = vv::toDouble(evts.get("S11_Pq"));

	/**
	 * Survie des oeufs à juvéniles aptères asexués	(S12)
	 */

	// Survie max
	Smax12 = vv::toDouble(evts.get("Smax12"));

	// Effet des températures minimales
	S12_Tref_tmin = vv::toDouble(evts.get("S12_Tref_tmin"));
	S12_m_tmin = vv::toDouble(evts.get("S12_m_tmin"));
	S12_T0_tmin = vv::toDouble(evts.get("S12_T0_tmin"));
	S12_T1_tmin = vv::toDouble(evts.get("S12_T1_tmin"));
	S12_q_tmin = vv::toDouble(evts.get("S12_q_tmin"));

	// Effet des températures maximales
	S12_Tref_tmax = vv::toDouble(evts.get("S12_Tref_tmax"));
	S12_m_tmax = vv::toDouble(evts.get("S12_m_tmax"));
	S12_T0_tmax = vv::toDouble(evts.get("S12_T0_tmax"));
	S12_T1_tmax = vv::toDouble(evts.get("S12_T1_tmax"));
	S12_q_tmax = vv::toDouble(evts.get("S12_q_tmax"));

	/**
	 * Survie des juvéniles à adultes (S23, S45, S67 et S89) et des adultes (S33, S55, S77 et S99)
	 */
	Smax33 = vv::toDouble(evts.get("Smax33"));
	Smax55 = vv::toDouble(evts.get("Smax55"));
	Smax23 = vv::toDouble(evts.get("Smax23"));
	Smax45 = vv::toDouble(evts.get("Smax45"));



	// Effet des températures minimales
	S33_Tref_tmin = vv::toDouble(evts.get("S33_Tref_tmin"));
	S33_m_tmin = vv::toDouble(evts.get("S33_m_tmin"));
	S33_T0_tmin = vv::toDouble(evts.get("S33_T0_tmin"));
	S33_T1_tmin = vv::toDouble(evts.get("S33_T1_tmin"));
	S33_q_tmin = vv::toDouble(evts.get("S33_q_tmin"));

	// Effet des températures maximales
	S33_Tref_tmax = vv::toDouble(evts.get("S33_Tref_tmax"));
	S33_m_tmax = vv::toDouble(evts.get("S33_m_tmax"));
	S33_T0_tmax = vv::toDouble(evts.get("S33_T0_tmax"));
	S33_T1_tmax = vv::toDouble(evts.get("S33_T1_tmax"));
	S33_q_tmax = vv::toDouble(evts.get("S33_q_tmax"));

	/**
	 * 	Fécondité des adultes produisant aptères et oeufs (F32 , F52 , F78 et F91)
	 */
	Fmax32 = vv::toInteger(evts.get("Fmax32"));
	Fmax52 = vv::toInteger(evts.get("Fmax52"));
	Fmax91 = vv::toInteger(evts.get("Fmax91"));

	// Effet direct des températures
	F32_TDlow = vv::toDouble(evts.get("F32_TDlow"));
	F32_TDup = vv::toDouble(evts.get("F32_TDup"));
	F32_p = vv::toInteger(evts.get("F32_p"));
	F32_m = vv::toDouble(evts.get("F32_m"));
	F32_SDD0 = vv::toDouble(evts.get("F32_SDD0"));
	F32_SDD1 = vv::toDouble(evts.get("F32_SDD1"));
	F32_q = vv::toDouble(evts.get("F32_q"));

	F32_SDDmax = ((double)F32_p) * (F32_TDup - F32_TDlow);
	F32_SDDref = F32_SDDmax / 2;

	// Effet direct des précipitations
	F32_Pref = vv::toDouble(evts.get("F32_Pref"));
	F32_Pm = vv::toDouble(evts.get("F32_Pm"));
	F32_P0 = vv::toDouble(evts.get("F32_P0"));
	F32_P1 = vv::toDouble(evts.get("F32_P1"));
	F32_Pq = vv::toDouble(evts.get("F32_Pq"));

	Ftot_3 = vv::toInteger(evts.get("Ftot_3"));
	Ftot_5 = vv::toInteger(evts.get("Ftot_5"));
	Ftot_9 = vv::toInteger(evts.get("Ftot_9"));
	Ftot_7 = Ftot_5;

	fecondity_max = {0, 0, Ftot_3, 0, Ftot_5, 0, Ftot_7, 0, Ftot_9};

	Fmax34 = vv::toInteger(evts.get("Fmax34"));
	Fmax36 = Fmax34;

	// Différénciation de la production de descendants sexués et asexués
//	nDay = vv::toInteger(evts.get("nDay"));
	Seuil_photo = vv::toDouble(evts.get("Seuil_photo"));
	p_threshold = vv::toDouble(evts.get("p_threshold"));
	linearTrend = vv::toInteger(evts.get("linearTrend"));

	LAI_ref = vv::toDouble(evts.get("LAI_ref"));
	LAI_m = vv::toDouble(evts.get("LAI_m"));
	LAI_0 = vv::toDouble(evts.get("LAI_0"));
	LAI_1 = vv::toDouble(evts.get("LAI_1"));
	LAI_q = vv::toDouble(evts.get("LAI_q"));

	// Prunier
	Prunus_GDD_Tbase = vv::toDouble(evts.get("Prunus_GDD_Tbase"));
	Prunus_GDD_feuil = vv::toDouble(evts.get("Prunus_GDD_feuil"));
	Prunus_dT = vv::toDouble(evts.get("Prunus_dT"));
	Prunus_GDD_bourgeon = Prunus_GDD_feuil - Prunus_dT;

	Prunus_Nmobil = vv::toDouble(evts.get("Prunus_Nmobil"));
	Prunus_Ncapt = vv::toDouble(evts.get("Prunus_Ncapt"));
	Prunus_Nfol_base = vv::toDouble(evts.get("Prunus_Nfol_base")); // 5.34 * std::pow(10, -6)
	Prunus_Nfol =  Prunus_Nfol_base * surf_cellule; // (exprimée en kg d’N par cellules si le prunier occuper 100% de la cellule). Cette valeur permet d’acceuillir au max 1000 juvéniles par mš.

	Prunus_LAI_max = vv::toDouble(evts.get("Prunus_LAI_max"));
	Prunus_a = vv::toDouble(evts.get("Prunus_a"));
	Prunus_b = vv::toDouble(evts.get("Prunus_b"));
	Prunus_SLA = vv::toDouble(evts.get("Prunus_SLA"));
	Prunus_LDMC = vv::toDouble(evts.get("Prunus_LDMC"));
	Prunus_LNC = vv::toDouble(evts.get("Prunus_LNC"));

	Prunus_interest_max = vv::toDouble(evts.get("Prunus_interest_max"));
	Prunus_interest_min = vv::toDouble(evts.get("Prunus_interest_min"));
	Prunus_c = vv::toDouble(evts.get("Prunus_c"));
	Prunus_d = vv::toDouble(evts.get("Prunus_d"));

	Prunus_GDD_sen1 = vv::toDouble(evts.get("Prunus_GDD_sen1"));
	Prunus_GDD_sen2 = vv::toDouble(evts.get("Prunus_GDD_sen2"));
	Prunus_Tseuil = vv::toDouble(evts.get("Prunus_Tseuil"));
	Prunus_e = vv::toDouble(evts.get("Prunus_e"));
	Prunus_f = vv::toDouble(evts.get("Prunus_f"));
}

void TraceDynaLandCondition(){
	TraceModel(vle::fmt("parameters_file: %1%") % parameters_file);

	TraceModel("\ncycle de vie et historiques:");
	TraceModel(vle::fmt("time_step: %1%") % time_step);
	TraceModel(vle::fmt("life_cycle_duration: %1%") % life_cycle_duration);
	TraceModel(vle::fmt("meteo_mean_size: %1%") % meteo_mean_size);
	TraceModel(vle::fmt("meteo_trend_size: %1%") % meteo_trend_size);
	TraceModel(vle::fmt("meteo_history_size: %1%") % meteo_history_size);

	TraceModel("\nlimites inférieure/supérieure de température de développement:");
	TraceModel(vle::fmt("TDLow: %1%") % TDLow);
	TraceModel(vle::fmt("TDup: %1%") % TDup);
	TraceModel(vle::fmt("ageLow0: %1%") % ageLow0);
	TraceModel(vle::fmt("ageUp0: %1%") % ageUp0);
	TraceModel(vle::fmt("ageJ: %1%") % ageJ);
	TraceModel(vle::fmt("ageA: %1%") % ageA);
	TraceModel(vle::fmt("SDDadult: %1%") % SDDadult);
	TraceModel(vle::fmt("SDDdie: %1%") % SDDdie);

	TraceModel("\neffet de la ressource:");
	TraceModel(vle::fmt("Nj: %1%") % Nj);
	TraceModel(vle::fmt("Na: %1%") % Na);
	TraceModel(vle::fmt("Ncapt: %1%") % Ncapt);
	TraceModel(vle::fmt("s0: %1%") % s0);
	TraceModel(vle::fmt("sP: %1%") % sP);

	TraceModel("\nsurface cellule:");
	TraceModel(vle::fmt("surf_cellule: %1%") % surf_cellule);
	TraceModel(vle::fmt("surf: %1%") % surf);

	TraceModel("\ntaux de dispersion:");
	TraceModel(vle::fmt("Dmin: %1%") % Dmin);
	TraceModel(vle::fmt("Dmax: %1%") % Dmax);

	TraceModel(vle::fmt("cost_stics0: %1%") % cost_stics0);
	TraceModel(vle::fmt("cost_stics1: %1%") % cost_stics1);
	TraceModel(vle::fmt("cost_prunus0: %1%") % cost_prunus0);
	TraceModel(vle::fmt("cost_prunus1: %1%") % cost_prunus1);

	TraceModel("\nSurvie des oeufs (S11):");
	TraceModel(vle::fmt("Smax11: %1%") % Smax11);

	TraceModel("Effet des températures minimales:");
	TraceModel(vle::fmt("S11_Tref_tmin: %1%") % S11_Tref_tmin);
	TraceModel(vle::fmt("S11_m_tmin: %1%") % S11_m_tmin);
	TraceModel(vle::fmt("S11_T0_tmin: %1%") % S11_T0_tmin);
	TraceModel(vle::fmt("S11_T1_tmin: %1%") % S11_T1_tmin);
	TraceModel(vle::fmt("S11_q_tmin: %1%") % S11_q_tmin);

	TraceModel("Effet des températures maximales:");
	TraceModel(vle::fmt("S11_Tref_tmax: %1%") % S11_Tref_tmax);
	TraceModel(vle::fmt("S11_m_tmax: %1%") % S11_m_tmax);
	TraceModel(vle::fmt("S11_T0_tmax: %1%") % S11_T0_tmax);
	TraceModel(vle::fmt("S11_T1_tmax: %1%") % S11_T1_tmax);
	TraceModel(vle::fmt("S11_q_tmax: %1%") % S11_q_tmax);

	TraceModel("Effet des fortes précipitations:");
	TraceModel(vle::fmt("S11_Pref: %1%") % S11_Pref);
	TraceModel(vle::fmt("S11_Pm: %1%") % S11_Pm);
	TraceModel(vle::fmt("S11_P0: %1%") % S11_P0);
	TraceModel(vle::fmt("S11_P1: %1%") % S11_P1);
	TraceModel(vle::fmt("S11_Pq: %1%") % S11_Pq);

	TraceModel("\nSurvie des oeufs à juvéniles aptères asexués	(S12):");
	TraceModel(vle::fmt("Smax12: %1%") % Smax12);

	TraceModel("Effet des températures minimales:");
	TraceModel(vle::fmt("S12_Tref_tmin: %1%") % S12_Tref_tmin);
	TraceModel(vle::fmt("S12_m_tmin: %1%") % S12_m_tmin);
	TraceModel(vle::fmt("S12_T0_tmin: %1%") % S12_T0_tmin);
	TraceModel(vle::fmt("S12_T1_tmin: %1%") % S12_T1_tmin);
	TraceModel(vle::fmt("S12_q_tmin: %1%") % S12_q_tmin);

	TraceModel("Effet des températures maximales:");
	TraceModel(vle::fmt("S12_Tref_tmax: %1%") % S12_Tref_tmax);
	TraceModel(vle::fmt("S12_m_tmax: %1%") % S12_m_tmax);
	TraceModel(vle::fmt("S12_T0_tmax: %1%") % S12_T0_tmax);
	TraceModel(vle::fmt("S12_T1_tmax: %1%") % S12_T1_tmax);
	TraceModel(vle::fmt("S12_q_tmax: %1%") % S12_q_tmax);


	TraceModel("\nSurvie des juvéniles à adultes (S23, S45, S67 et S89) et des adultes (S33, S55, S77 et S99):");
	TraceModel(vle::fmt("Smax33: %1%") % Smax33);
	TraceModel(vle::fmt("Smax55: %1%") % Smax55);
	TraceModel(vle::fmt("Smax23: %1%") % Smax23);
	TraceModel(vle::fmt("Smax45: %1%") % Smax45);

	TraceModel("Effet des températures minimales:");
	TraceModel(vle::fmt("S33_Tref_tmin: %1%") % S33_Tref_tmin);
	TraceModel(vle::fmt("S33_m_tmin: %1%") % S33_m_tmin);
	TraceModel(vle::fmt("S33_T0_tmin: %1%") % S33_T0_tmin);
	TraceModel(vle::fmt("S33_T1_tmin: %1%") % S33_T1_tmin);
	TraceModel(vle::fmt("S33_q_tmin: %1%") % S33_q_tmin);

	TraceModel("Effet des températures maximales:");
	TraceModel(vle::fmt("S33_Tref_tmax: %1%") % S33_Tref_tmax);
	TraceModel(vle::fmt("S33_m_tmax: %1%") % S33_m_tmax);
	TraceModel(vle::fmt("S33_T0_tmax: %1%") % S33_T0_tmax);
	TraceModel(vle::fmt("S33_T1_tmax: %1%") % S33_T1_tmax);
	TraceModel(vle::fmt("S33_q_tmax: %1%") % S33_q_tmax);

	TraceModel("\nFécondité des adultes produisant aptères et oeufs (F32 , F52 , F78 et F91):");
	TraceModel(vle::fmt("Fmax32: %1%") % Fmax32);
	TraceModel(vle::fmt("Fmax52: %1%") % Fmax52);
	TraceModel(vle::fmt("Fmax91: %1%") % Fmax91);
	TraceModel(vle::fmt("Fmax34: %1%") % Fmax34);
	TraceModel(vle::fmt("Fmax36: %1%") % Fmax36);

	TraceModel("Effet direct températures:");
	TraceModel(vle::fmt("F32_TDlow: %1%") % F32_TDlow);
	TraceModel(vle::fmt("F32_TDup: %1%") % F32_TDup);
	TraceModel(vle::fmt("F32_p: %1%") % F32_p);
	TraceModel(vle::fmt("F32_m: %1%") % F32_m);
	TraceModel(vle::fmt("F32_SDD0: %1%") % F32_SDD0);
	TraceModel(vle::fmt("F32_SDD1: %1%") % F32_SDD1);
	TraceModel(vle::fmt("F32_q: %1%") % F32_q);
	TraceModel(vle::fmt("F32_SDDmax: %1%") % F32_SDDmax);
	TraceModel(vle::fmt("F32_SDDref: %1%") % F32_SDDref);

	TraceModel("Effet direct précipitations:");
	TraceModel(vle::fmt("F32_Pref: %1%") % F32_Pref);
	TraceModel(vle::fmt("F32_Pm: %1%") % F32_Pm);
	TraceModel(vle::fmt("F32_P0: %1%") % F32_P0);
	TraceModel(vle::fmt("F32_P1: %1%") % F32_P1);
	TraceModel(vle::fmt("F32_Pq: %1%") % F32_Pq);

	TraceModel("fecondity max:");
	TraceModel(vle::fmt("Ftot_3: %1%") % Ftot_3);
	TraceModel(vle::fmt("Ftot_5: %1%") % Ftot_5);
	TraceModel(vle::fmt("Ftot_7: %1%") % Ftot_7);
	TraceModel(vle::fmt("Ftot_9: %1%") % Ftot_9);

	TraceModel("\nDifférénciation de la production de descendants sexués et asexués:");
//	TraceModel(vle::fmt("nDay: %1%") % nDay);
	TraceModel(vle::fmt("Seuil_photo: %1%") % Seuil_photo);
	TraceModel(vle::fmt("p_threshold: %1%") % p_threshold);
	TraceModel(vle::fmt("linearTrend: %1%") % linearTrend);

	TraceModel("\nEffet de la densité de puceron sur le LAI:");
	TraceModel(vle::fmt("LAI_ref: %1%") % LAI_ref);
	TraceModel(vle::fmt("LAI_m: %1%") % LAI_m);
	TraceModel(vle::fmt("LAI_0: %1%") % LAI_0);
	TraceModel(vle::fmt("LAI_1: %1%") % LAI_1);
	TraceModel(vle::fmt("LAI_q: %1%") % LAI_q);

	TraceModel("\nModèle prunier:");
	TraceModel(vle::fmt("Prunus_GDD_Tbase: %1%") % Prunus_GDD_Tbase);
	TraceModel(vle::fmt("Prunus_GDD_feuil: %1%") % Prunus_GDD_feuil);
	TraceModel(vle::fmt("Prunus_dT: %1%") % Prunus_dT);
	TraceModel(vle::fmt("Prunus_GDD_bourgeon: %1%") % Prunus_GDD_bourgeon);
	TraceModel(vle::fmt("Prunus_Nmobil: %1%") % Prunus_Nmobil);
	TraceModel(vle::fmt("Prunus_Ncapt: %1%") % Prunus_Ncapt);
	TraceModel(vle::fmt("Prunus_Nfol_base: %1%") % Prunus_Nfol_base);
	TraceModel(vle::fmt("Prunus_Nfol: %1%") % Prunus_Nfol);

	TraceModel(vle::fmt("Prunus_LAI_max: %1%") % Prunus_LAI_max);
	TraceModel(vle::fmt("Prunus_a: %1%") % Prunus_a);
	TraceModel(vle::fmt("Prunus_b: %1%") % Prunus_b);
	TraceModel(vle::fmt("Prunus_SLA: %1%") % Prunus_SLA);
	TraceModel(vle::fmt("Prunus_LDMC: %1%") % Prunus_LDMC);
	TraceModel(vle::fmt("Prunus_LNC: %1%") % Prunus_LNC);

	TraceModel(vle::fmt("Prunus_interest_max: %1%") % Prunus_interest_max);
	TraceModel(vle::fmt("Prunus_interest_min: %1%") % Prunus_interest_min);
	TraceModel(vle::fmt("Prunus_c: %1%") % Prunus_c);
	TraceModel(vle::fmt("Prunus_d: %1%") % Prunus_d);

	TraceModel(vle::fmt("Prunus_GDD_sen1: %1%") % Prunus_GDD_sen1);
	TraceModel(vle::fmt("Prunus_GDD_sen2: %1%") % Prunus_GDD_sen2);
	TraceModel(vle::fmt("Prunus_Tseuil: %1%") % Prunus_Tseuil);
	TraceModel(vle::fmt("Prunus_e: %1%") % Prunus_e);
	TraceModel(vle::fmt("Prunus_f: %1%") % Prunus_f);
}

void newMeteoHistory(std::string id){
	TraceModel("create meteo history for id : '" + id + "'");
	std::deque<double> tmin, tmax, tmoy, tmin_moy, tmax_moy, tmoy_moy, tmin_min, tmax_max, rain, rain_max, rain_moy, dd, sdd, gdd;
	map<std::string, std::deque<double>> meteo_data;
	tmin_moy.push_back(0);
	tmax_moy.push_back(0);
	tmoy_moy.push_back(0);
	rain_max.push_back(0);
	rain_moy.push_back(0);
	sdd.push_back(0);
	gdd.push_back(0);
	meteo_data.insert(std::make_pair("Tmin", tmin));
	meteo_data.insert(std::make_pair("Tmax", tmax));
	meteo_data.insert(std::make_pair("Tmoy", tmoy));
	meteo_data.insert(std::make_pair("Rain", rain));
	meteo_data.insert(std::make_pair("SDD", sdd));
	meteo_data.insert(std::make_pair("GDD", gdd));
	meteo_data.insert(std::make_pair("Tmin_moy", tmin_moy));
	meteo_data.insert(std::make_pair("Tmax_moy", tmax_moy));
	meteo_data.insert(std::make_pair("Tmoy_moy", tmoy_moy));
	meteo_data.insert(std::make_pair("Rain_max", rain_max));
	meteo_data.insert(std::make_pair("Rain_moy", rain_moy));
	meteo_data.insert(std::make_pair("DD", dd));
	meteo_history.insert(std::make_pair(id, meteo_data));
}

bool isSticsCell(const shared_ptr<Cell>& aCell){
	return !(stics.find(std::to_string(aCell->getIdOcc())) == stics.end());
}

bool isPrunusCell(const shared_ptr<Cell>& aCell){
	return !(prunus.find(std::to_string(aCell->getIdOcc())) == prunus.end());
}

void processNJI(){
	for(std::map<std::string, stics_var>::iterator it = stics.begin(); it != stics.end(); ++it){
		stics_var v = it->second;
		if(v.syncs["QNplante"]() > 0){
			nJI++;
			TraceModel("nJI: " + std::to_string(nJI));
			break;
		}
	}
}

enum STAGE {keeped = 0, upgraded = 1, dead = 2};

STAGE DDdeveloppement(int classe, int age, double SDD,
		int ageLow0, int ageUp0, int ageJ, int ageA,
		double SDDadult, double SDDdie){
	switch(classe) {
		case 0 :
			if (age <= ageLow0) {
				return keeped;
			} else {
				if ( age > ageUp0 ) {
					return dead;
				} else {
					return upgraded;
				}
			}
			break;
		case 1 :
		case 3 :
		case 5 :
		case 7 :
			if ( age <= ageJ ) {
				if (SDD < SDDadult ) {
					return keeped;
				} else {
					return upgraded;
				}
			} else {
				return dead;
			}
			break;
		case 2 :
		case 4 :
		case 6 :
		case 8 :
			if ( age <= ageA ) {
				if (SDD < SDDdie ) {
					return keeped;
				} else {
					return dead;
				}
			} else {
				return dead;
			}
			break;
		default:
			throw runtime_error("developpement stage undefined for classe " + std::to_string(classe));
	}
	return keeped;
}

void processMeteo(){
	TraceModel("processMeteo...");
	double Tmin, Tmax, Tmoy, rain, DDday, GDDday;
	if(sun_history.size() == meteo_history_size){
		sun_history.pop_front();
	}
	sun_history.push_back(meteo["Sun"]());

	for(std::map<std::string, std::map<std::string, std::deque<double>>>::iterator it = meteo_history.begin();
			it != meteo_history.end(); it++){
		std::string id = it->first;
//		TraceModel("process history for id= " + id);
		if(id == "default"){
//			TraceModel("Using default meteo");
			Tmin = meteo["Tmin"]();
			Tmax = meteo["Tmax"]();
		}else{
//			TraceModel("Using stics meteo");
			Tmin = stics[id].syncs["tcultmin"]();
			Tmax = stics[id].syncs["tcultmax"]();
		}
		rain = meteo["Rain"]();
		Tmoy = (Tmin + Tmax) / (double)2.0;
		DDday = DD(Tmax, Tmin, TDLow, TDup);

		std::map<std::string, std::deque<double>> meteo_data = it->second;
		if(meteo_data["Tmin"].size() == meteo_history_size){
			meteo_data["Tmin"].pop_front();
			meteo_data["Tmax"].pop_front();
			meteo_data["Tmoy"].pop_front();
			meteo_data["Rain"].pop_front();
			meteo_data["DD"].pop_front();
		}
		meteo_data["Tmin"].push_back(Tmin);
		meteo_data["Tmax"].push_back(Tmax);
		meteo_data["Tmoy"].push_back(Tmoy);
		meteo_data["Rain"].push_back(rain);
		meteo_data["DD"].push_back(DDday);
//		meteo_data["GDD"].push_back(GDDday);

		int nb_records = std::min((int)meteo_data["Tmin"].size(), (int)meteo_mean_size);
		double sum_Tmin = std::accumulate(meteo_data["Tmin"].end() - nb_records, meteo_data["Tmin"].end(), (double)0.0);
		double sum_Tmax = std::accumulate(meteo_data["Tmax"].end() - nb_records, meteo_data["Tmax"].end(), (double)0.0);
		double sum_Tmoy = std::accumulate(meteo_data["Tmoy"].end() - nb_records, meteo_data["Tmoy"].end(), (double)0.0);

		double Tmin_moy = sum_Tmin / (double)nb_records;
		double Tmax_moy = sum_Tmax / (double)nb_records;
		double Tmoy_moy = sum_Tmoy / (double)nb_records;
		meteo_data["Tmin_moy"][0] = Tmin_moy;
		meteo_data["Tmax_moy"][0] = Tmax_moy;
		meteo_data["Tmoy_moy"][0] = Tmoy_moy;

		GDDday = GDD(Tmin, Tmax, Prunus_GDD_Tbase);
		meteo_data["GDD"][0] = meteo_data["GDD"][0] + GDDday;
		TraceModel("GDD Sum: " + std::to_string(meteo_data["GDD"][0]));

		double sum_rain = std::accumulate(meteo_data["Rain"].end() - nb_records, meteo_data["Rain"].end(), (double)0.0);
		double rain_moy = sum_rain / (double)nb_records;
		meteo_data["Rain_moy"][0] = rain_moy;

		double SDD = std::accumulate(meteo_data["DD"].end() - nb_records, meteo_data["DD"].end(), (double)0.0);
		meteo_data["SDD"][0] = SDD;

		double rain_max = *(std::max_element(meteo_data["Rain"].end() - nb_records, meteo_data["Rain"].end()));
		meteo_data["Rain_max"][0] = rain_max;

//		TraceModel("Meteo history size : " + std::to_string(meteo_data["Tmin"].size()));
//		TraceModel("T min : " + std::to_string(Tmin));
//		TraceModel("T max : " + std::to_string(Tmax));
		TraceModel("T moy : " + std::to_string(Tmoy));
//		TraceModel("t min moy : " + std::to_string(Tmin_moy));
//		TraceModel("t max moy : " + std::to_string(Tmax_moy));
//		TraceModel("rain moy : " + std::to_string(rain_moy));
//		TraceModel("rain max : " + std::to_string(rain_max));

		meteo_history[id] = meteo_data;
	}
	TraceModel("processMeteo end");
}

void processDeveloppement(Map* amap, Population* pop){
	TraceModel("processDeveloppement start");

	vector<shared_ptr<Individus>> full_pop;
	vector<shared_ptr<Cell>> cells = amap->getCells();

	#pragma omp parallel for
	for(unsigned int cell_index = 0; cell_index < cells.size(); cell_index++){

		shared_ptr<Cell> cell = cells[cell_index];
		int N = cell->getNbIndividus();

		if( N > 0 ) {
			Logger::info("\n--> %s\n", cell->info().c_str());

			double Tmax = 0, Tmin = 0;
			if(isSticsCell(cell)){
				std::string sid = std::to_string(cell->getIdOcc());
				Tmax = stics[sid].syncs["tcultmax"]();
				Tmin = stics[sid].syncs["tcultmin"]();
//				Logger::debug("Using STIC MICRO CLIMAT\n");
			}else{
				Tmax = meteo["Tmax"]();
				Tmin = meteo["Tmin"]();
			}

//			Logger::debug("Tmax: %f, Tmin: %f\n", Tmax, Tmin);
			double DDday = DD(Tmax, Tmin, TDLow, TDup);

			vector<shared_ptr<Individus>> deads;
			vector<weak_ptr<Individus>> cell_pop;

			const vector<shared_ptr<Individus>>& individus = cell->getIndividus2();

			for(vector<shared_ptr<Individus>>::const_iterator it2 = individus.begin(); it2 != individus.end(); it2++)
			{
				shared_ptr<Individus> individu = (*it2);
				individu->addTemperatureGrowth(DDday);
				individu->addAge(1);

				int classe = individu->getClass();
				int age = individu->getAge();
				int sex = individu->getSex();
				double sdd = individu->getTemperatureGrowthSum();
				STAGE newClasse = DDdeveloppement(classe, age, sdd,
						ageLow0, ageUp0, ageJ, ageA,
						SDDadult, SDDdie);

				if(classe == 0){
					individu->setInitAgeOnUpgrade(true);
				}else{
					individu->setInitAgeOnUpgrade(false);
				}
//				Logger::debug("classe: %d, age: %d, sdd: %f, STAGE : %d\n", classe, age, sdd, (int)newClasse);
				if( newClasse == dead){
					 if( myProcess.survival_on ){
						#pragma omp critical
						 pop->nb_dead[sex][classe]++;
					 }else{
						cell_pop.push_back(individu);

						#pragma omp critical
						full_pop.push_back(individu);
					 }
				}else{
					if( newClasse == upgraded ){
						individu->setCanUpgrade(true);
					}else{
						individu->setCanUpgrade(false);
					}
					cell_pop.push_back(individu);

					#pragma omp critical
					full_pop.push_back(individu);
				}
			}
			cell->setIndividus(cell_pop);
		}
	}
	pop->setIndividus(full_pop);

	TraceModel("processDeveloppement end");
}


int processPhenotype(const deque<double>& Tmoy_hist, const deque<double>& Sun_hist, const double Sun){
//	TraceModel("processPhenotype start");
	int Isexe = 0;

	if(linearTrend){

		int trend_size = std::min((const int)meteo_trend_size, (const int)Tmoy_hist.size());
		if(trend_size > 1){
			std::vector<double> time;
			std::vector<double> dtTmoy;
			std::vector<double> lmTmoy;

			int Tmoy_idx0 = (Tmoy_hist.size() - trend_size);
			double Tmoy0 = Tmoy_hist[Tmoy_idx0];

			int t = 0;
			for(std::deque<double>::const_iterator it = (Tmoy_hist.begin() + Tmoy_idx0);
					it != Tmoy_hist.end(); it++){
				dtTmoy.push_back( ((*it) - Tmoy0) );
				t++;
				time.push_back(t);
			}
			lm(time, dtTmoy, lmTmoy);

			TraceModel("Tmoy linear trend:");
			TraceModel("slope: " + std::to_string(lmTmoy[0]) + ", p-value: " + std::to_string(lmTmoy[1]));

			if( (lmTmoy[0] >= 0) || (lmTmoy[1] > p_threshold) ){
				TraceModel("lm Tmoy > 0 || p-value > 0.05");
				Isexe = 0;

				lmTmoy.clear();
				time.clear();
			}else{
				TraceModel("NOT (lm Tmoy > 0 || p-value > 0.05)");
				Isexe = 1;

				std::vector<double> dtSun;
				std::vector<double> lmSun;

				int Sun_idx0 = (Sun_hist.size() - trend_size);
				double Sun0 = Sun_hist[Sun_idx0];

				for(std::deque<double>::const_iterator it = (Sun_hist.begin() + Sun_idx0);
						it != Sun_hist.end(); it++){
					dtSun.push_back( ((*it) - Sun0) );
				}

				lm(time, dtSun, lmSun);

				TraceModel("Sun linear trend:");
				TraceModel("slope: " + std::to_string(lmSun[0]) + ", p-value: " + std::to_string(lmSun[1]));

				if(lmSun[0] >= 0 || lmSun[1] > p_threshold){
					TraceModel("lm Sun > 0 || p-value > 0.05");
					Isexe = 0;
				}
				if(Sun > Seuil_photo){
					TraceModel("Sun > Seuil_photo");
					Isexe = 0;
				}
			}
		}else{
			TraceModel("Not enougth data to perform linear regression -> Isexe=0");
		}
	}else{
		double last_sun = 0;
		if(Sun_hist.size() > 1){
			last_sun = Sun_hist[Sun_hist.size() - 2];
		}
		if(Sun < last_sun && last_sun < Seuil_photo){
			Isexe=1;
		}
	}
//	TraceModel("processPhenotype end");
	return Isexe;
}

double processKmaxStics(double Bfresh, double Bdry, double BdryP, double Nplant, double Pjuv){
	double Btotal = Bfresh + Bdry; // MAFRAISFEUILLE + MAFEUILVERTE
	double Kmax = 0;
	if(Btotal > 0) {
		double Nmobil = Bfresh / Btotal;
		double Nfol = surf * Nplant * (Bdry / BdryP); // * 1000.0;
		TraceModel(vle::fmt("Nmobil: %1%, Nfol: %2%") % Nmobil % Nfol);
		Kmax = processKmax(Nmobil, Ncapt, Nfol, Pjuv);
	}
	return Kmax;
}

double processKmax(double Nmobil, double Ncapt, double Nfol, double Pjuv){
	double Kmax = 0;
	double tmp = ((double)sP) * Nj * Pjuv + Na * (1.0 - Pjuv);
	if(tmp > 0){
		Kmax = ( Ncapt * Nmobil * Nfol ) / tmp;
	}
	return Kmax;
}


int processDispersionDistanceEnvironmentalEffect(double rain, double SDD){
	TraceModel("processDispersionDistanceEnvironmentalEffect");
	double W32_SDD = temperatureFecondityDirectEffect(SDD, F32_SDDref, F32_m, F32_SDD0, F32_SDD1, F32_q);
	double W32_P = rainFecondityDirectEffect(rain, F32_Pref, F32_Pm, F32_P0, F32_P1, F32_Pq);
	double W_dist = W32_P * W32_SDD;
	double dist = ((double)disp_dmax) * W_dist;
	TraceModel("Dmax: " + std::to_string((int)dist));
	return (int)dist;
}

void processDispersionEnvironmentalEffect(double Tmin, double Tmax, double rain,
		double rain_max, double Kmax, double abundance, double SDD,
		std::vector<double>& dispersionEffect){
	TraceModel("processDispersionEnvironmentalEffect");
	double K_disp = 0;
	if(Kmax > 0){
		double W33_Tmin = minimalTemperatureSurvivalEffect(
					Tmin, S33_Tref_tmin, S33_m_tmin, S33_T0_tmin, S33_T1_tmin, S33_q_tmin);
		double W33_Tmax = maximalTemperatureSurvivalEffect(
					Tmax, S33_Tref_tmax, S33_m_tmax, S33_T0_tmax, S33_T1_tmax, S33_q_tmax);
		double W11_Pmax = heavyRainSurvivalEffect(rain_max, S11_Pref, S11_Pm, S11_P0, S11_P1, S11_Pq);
		K_disp = Kmax * W33_Tmin * W33_Tmax * W11_Pmax;
	}

	double dd_disp = 1.0 - capacity2competition(K_disp, abundance, s0);

	double W32_SDD = temperatureFecondityDirectEffect(SDD, F32_SDDref, F32_m, F32_SDD0, F32_SDD1, F32_q);
	double W32_P = rainFecondityDirectEffect(rain, F32_Pref, F32_Pm, F32_P0, F32_P1, F32_Pq);
	double Wdisp = dd_disp * W32_P * W32_SDD;
	double D = Dmin + (Dmax - Dmin) * Wdisp;

	TraceModel(vle::fmt("dd_disp: %1%, D: %2%") % dd_disp % D);
	dispersionEffect.clear();
	dispersionEffect.push_back(0);
	dispersionEffect.push_back(0);
	dispersionEffect.push_back(0);
	dispersionEffect.push_back(0);
	dispersionEffect.push_back(D); // adulte, ailé, asexué <-> stade 5 <-> classe 4
	dispersionEffect.push_back(0);
	dispersionEffect.push_back(D); // adulte, ailé, sexué  <-> stade 7 <-> classe 6
	dispersionEffect.push_back(0);
	dispersionEffect.push_back(0);
}

void processSurvivalEnvironmentalEffect(double Tmin, double Tmax, double rain_max, double Kmax,
		double abundance, Matrix &survivalEffect)
{
	TraceModel("processSurvivalEnvironmentalEffect");
//	Matrix survivalEffect(9, 9);
//	Matrix fecondityEffect(9, 9);

	// Survie des oeufs (S11)
	double W11_Tmin = minimalTemperatureSurvivalEffect(
			Tmin, S11_Tref_tmin, S11_m_tmin, S11_T0_tmin, S11_T1_tmin, S11_q_tmin);
	double W11_Tmax = maximalTemperatureSurvivalEffect(
			Tmax, S11_Tref_tmax, S11_m_tmax, S11_T0_tmax, S11_T1_tmax, S11_q_tmax);
	double W11_Pmax = heavyRainSurvivalEffect(rain_max, S11_Pref, S11_Pm, S11_P0, S11_P1, S11_Pq);
	double W11 = W11_Tmin * W11_Tmax * W11_Pmax;
	double S11 = Smax11 * W11;

	// Survie des oeufs à juvéniles aptères asexués (S12)
	double W12_Tmin = minimalTemperatureSurvivalEffect(
			Tmin, S12_Tref_tmin, S12_m_tmin, S12_T0_tmin, S12_T1_tmin, S12_q_tmin);
	double W12_Tmax = maximalTemperatureSurvivalEffect(
			Tmax, S12_Tref_tmax, S12_m_tmax, S12_T0_tmax, S12_T1_tmax, S12_q_tmax);
	double W12_Pmax = W11_Pmax;
	double W12 = W12_Tmin * W12_Tmax * W12_Pmax;
	double S12 = Smax12 * W12;

	// Survie des juvéniles (S22, S44, S66 et S88)
	double W22_Tmin = W12_Tmin;
	double W22_Tmax = W12_Tmax;
	double W22_Pmax = W11_Pmax;

	// double Kmax = processKmax(Bfresh, Bdry, BdryP, Nplant, Pjuv);

	double K22 = Kmax * W22_Tmin * W22_Tmax * W22_Pmax;
	double W22 = capacity2competition(K22, abundance, s0);
	double S22 = Smax22 * W22;
	double S44 = S22;
	double S66 = S22;
	double S88 = S22;

	// Survie des juvéniles à adultes (S23, S45, S67 et	S89) et des adultes (S33, S55, S77 et S99)
	double K33 = 0;
	double W33_Tmin = 0;
	double W33_Tmax = 0;
	double W33_Pmax = 0;
	if(Kmax > 0){
		W33_Tmin = minimalTemperatureSurvivalEffect(
				Tmin, S33_Tref_tmin, S33_m_tmin, S33_T0_tmin, S33_T1_tmin, S33_q_tmin);
		W33_Tmax = maximalTemperatureSurvivalEffect(
					Tmax, S33_Tref_tmax, S33_m_tmax, S33_T0_tmax, S33_T1_tmax, S33_q_tmax);
		W33_Pmax = W11_Pmax;
		K33 = Kmax * W33_Tmin * W33_Tmax * W33_Pmax;
	}
	double W33 = capacity2competition(K33, abundance, s0);

	double S33 = Smax33 * W33;
	double S99 = S33;
	double S55 = Smax55 * W33;
	double S77 = S55;
	double S23 = Smax23 * W33;
	double S89 = S23;
	double S45 = Smax45 * W33;
	double S67 = S45;



	TraceModel("Populate matrix effects");

	survivalEffect.clear(0.0);

	// Survie des oeufs (S11)
	survivalEffect.at(0, 0) = S11;

	// Survie des oeufs à juvéniles aptères asexués (S12)
	survivalEffect.at(0, 1) = S12;

	// Survie des juvéniles (S22, S44, S66 et S88)
	survivalEffect.at(1, 1) = S22;
	survivalEffect.at(3, 3) = S44;
	survivalEffect.at(5, 5) = S66;
	survivalEffect.at(7, 7) = S88;

	// Survie des juvéniles à adultes (S23, S45, S67 et	S89) et des adultes (S33, S55, S77 et S99)
	survivalEffect.at(1, 2) = S23;
	survivalEffect.at(3, 4) = S45;
	survivalEffect.at(5, 6) = S67;
	survivalEffect.at(7, 8) = S89;

	survivalEffect.at(2, 2) = S33;
	survivalEffect.at(4, 4) = S55;
	survivalEffect.at(6, 6) = S77;
	survivalEffect.at(8, 8) = S99;



	TraceModel("processSurvivalEnvironmentalEffect for one cell end");
}

void processIsexeMap(Map* amap, Population* pop){
	TraceModel("processIsexeMap start");
	IsexeMap.clear();
	std::vector<std::shared_ptr<Patch>> patches = amap->getPatches();
	for(std::vector<std::shared_ptr<Patch>>::iterator it = patches.begin();
		it != patches.end(); ++it){
		std::shared_ptr<Patch> apatch = (*it);
		std::string id = std::to_string(apatch->getId());
//		std::cout << "Process Isexe Map for patch " << id << std::endl;
		if(!apatch->isEmpty()){
			shared_ptr<Cell> cell = apatch->getCells()[0];
			bool isSticsCell = this->isSticsCell(cell);
			bool isPrunusCell = this->isPrunusCell(cell);
			std::string sid = "default";
			if( isSticsCell ){
				sid = std::to_string(cell->getIdOcc());
			}
			int Isexe = 0;
			if(IsexeMap.find(sid) != IsexeMap.end()){
				Isexe = IsexeMap[sid];
			}else{
				double Sun = meteo["Sun"]();
				deque<double> Tmoy_hist = meteo_history[sid]["Tmoy"];
				Isexe = processPhenotype(Tmoy_hist, sun_history, Sun);
				IsexeMap.insert(std::make_pair(sid, Isexe));
			}
			int connect = 0;
			if( isSticsCell || isPrunusCell ){
				if( Isexe == 1 ){ // Isexe = 1, la connectivité du tournesol est maximale (connect=10) alors que pour la cellule boisée avec hôte primaire elle n’est que de 3.
					if( isSticsCell ){
						connect = cost_stics1;
					}
					if( isPrunusCell ){
						connect = cost_prunus1;
					}
				}else{ // A l’inverse lorque Isexe = 0, la connectivité du tournesol est de 5 (pas de tournesol donc espace nu sur lequel le puceron peut voler)
					   // alors que pour la cellule boisée avec hôte primaire elle devient maximale (connectivité = 10).
					if( isSticsCell ){
						connect = cost_stics0;
					}
					if( isPrunusCell ){
						connect = cost_prunus0;
					}
				}
				std::vector<std::shared_ptr<Cell>> cells = apatch->getCells();
				for(std::vector<std::shared_ptr<Cell>>::iterator it2 = cells.begin();
						it2 != cells.end(); ++it2){
					(*it2)->setProperty(COST, connect);
				}
				if(isSticsCell){
//					std::cout << "stics :" << std::endl;
					stics[id].cost = connect;
					stics[id].Isexe = Isexe;
				}else{
					if(isPrunusCell){
//						std::cout << "prunus :" << std::endl;
						prunus[id].cost = connect;
						prunus[id].Isexe = Isexe;
					}
				}
//				std::cout << "Isexe :" << Isexe << std::endl;
//				std::cout << "connect :" << connect << std::endl;
			}
		}
	}
	TraceModel("processIsexeMap end");
}

void processFecondityEnvironmentalEffect(string sid, double Tmin, double Tmax, double rain, double rain_max, double Kmax, int abundance, double SDD,
		Matrix &fecondityEffect)
{
	// Fécondité des adultes produisant aptères et oeufs (F32 , F52 , F78 et F91)
	double W11_Pmax = heavyRainSurvivalEffect(rain_max, S11_Pref, S11_Pm, S11_P0, S11_P1, S11_Pq);

	double W33_Tmin = 0;
	double W33_Tmax = 0;
	double W33_Pmax = 0;
	if(Kmax > 0){
		W33_Tmin = minimalTemperatureSurvivalEffect(
				Tmin, S33_Tref_tmin, S33_m_tmin, S33_T0_tmin, S33_T1_tmin, S33_q_tmin);
		W33_Tmax = maximalTemperatureSurvivalEffect(
					Tmax, S33_Tref_tmax, S33_m_tmax, S33_T0_tmax, S33_T1_tmax, S33_q_tmax);
		W33_Pmax = W11_Pmax;
	}

	double K32 = 0;
	if(Kmax > 0){
		double W32_Tmin = W33_Tmin;
		double W32_Tmax = W33_Tmax;
		double W32_Pmax = W33_Pmax;
		K32 = Kmax * W32_Tmin * W32_Tmax * W32_Pmax;
	}
	double dd32 = capacity2competition(K32, abundance, s0);
	double W32_SDD = temperatureFecondityDirectEffect(SDD, F32_SDDref, F32_m, F32_SDD0, F32_SDD1, F32_q);
	double W32_P = rainFecondityDirectEffect(rain, F32_Pref, F32_Pm, F32_P0, F32_P1, F32_Pq);
	double W32 = dd32 * W32_P * W32_SDD;
	double F32 = Fmax32 * W32;
	double F52 = Fmax52 * W32;
	double F78 = F52;
	double F91 = Fmax91 * W32;

	// Fécondité des adultes aptères produisant des individus ailés asexués (F34 ) et sexués (F36 )
	double W34_Tmin = W33_Tmin;
	double W34_Tmax = W33_Tmax;
	double W34_Pmax = W11_Pmax;
	double K34 = Kmax * W34_Tmin * W34_Tmax * W34_Pmax;
	double dd34 = 1 - capacity2competition(K34, abundance, s0);
	double W34_SDD = W32_SDD;
	double W34_P = W32_P;

	double Isexe = IsexeMap[sid];
	TraceModel("Isexe Phenotype: " + std::to_string(Isexe));

	double W34 = dd34 * W34_P * W34_SDD * (1.0 - Isexe);
	double W36 = dd34 * W34_P * W34_SDD * Isexe;
	double F34 = Fmax34 * W34;
	double F36 = Fmax36 * W36;

	TraceModel("Populate matrix effects");

	fecondityEffect.clear(0.0);

	// Fécondité des adultes produisant aptères et oeufs (F32 , F52 , F78 et F91)
	fecondityEffect.at(2, 1) = F32;
	fecondityEffect.at(4, 1) = F52;
	fecondityEffect.at(6, 7) = F78;
	fecondityEffect.at(8, 0) = F91;

	// Fécondité des adultes aptères produisant des individus ailés asexués (F34) et sexués (F36 )
	fecondityEffect.at(2, 3) = F34;
	fecondityEffect.at(2, 5) = F36;
}

void processEnvironmentalEffect(EFFECT_TYPE effect_type, Map* amap, Population* pop){
	TraceModel("processEnvironmentalEffect start");

	vector<shared_ptr<Individus>> full_pop;
	vector<shared_ptr<Cell>> cells = amap->getCells();

	#pragma omp parallel for
	for(unsigned int cell_index = 0; cell_index < cells.size(); cell_index++){

		shared_ptr<Cell> cell = cells[cell_index];
		int Ntot = cell->getNbIndividus();

		if( Ntot > 0 ) {
			Logger::info("\n--> %s\n", cell->info().c_str());

			vector<shared_ptr<Individus>> individus = cell->getIndividus2();
			double N = getNbIndividus(individus, pop->getId());
			double Pjuv = 0;
			if(N > 0){
				double Njuv = getNbJuvenils(individus, pop->getId());
				Pjuv = Njuv / N;
			}

			std::string sid = "default";
			double Bfresh = 0;
			double Bdry = 0;
			double BdryP = 0;
			double Nplant = 0;
			double Kmax = 0;
			bool isSticsCell = this->isSticsCell(cell);
			bool isPrunusCell = this->isPrunusCell(cell);

			if(isSticsCell){
				sid = std::to_string(cell->getIdOcc());
				TraceModel("STIC CELL : " + sid);

				Bfresh = stics[sid].syncs["mafraisfeuille"](); // quantité de biomasse foliaire fraîche
				Bdry = stics[sid].syncs["mafeuilverte"](); // quantité de biomasse sêche folaire (feuille vivante)
				BdryP = stics[sid].syncs["masec(n)"](); // quantité de biomasse sêche de la partie épigée de la plante.
				Nplant = stics[sid].syncs["QNplante"]();
				Kmax = processKmaxStics(Bfresh, Bdry, BdryP, Nplant, Pjuv);
			}else{
				if(isPrunusCell){
					sid = std::to_string(cell->getIdOcc());
					TraceModel("PRUNUS CELL : " + sid);
					Kmax = processKmaxPrunier(sid, Pjuv);
					sid = "default";
				}else{
					TraceModel("OTHER CELL : default");
					Kmax = 0;
				}
			}
			cell->setProperty(CARRYING_CAPACITY, Kmax);
			TraceModel("KMAX: " + std::to_string(Kmax));
			std::map<std::string, std::deque<double>> meteo_data = meteo_history[sid];
			double Tmax = meteo_data["Tmax_moy"][0];
			double Tmin = meteo_data["Tmin_moy"][0];
			double rain = meteo_data["Rain_moy"][0];
			double rain_max = meteo_data["Rain_max"][0];
			int nbCl = pop01.getNbClasses();
			Matrix matrix_effect(nbCl, nbCl);
			std::vector<double> vector_effect(nbCl);
			double SDD = meteo_data["SDD"][0];
			std::string txt;
			int dmax = 0;
			switch(effect_type){
				case DISPERSION_DIST:
					dmax = processDispersionDistanceEnvironmentalEffect(rain, SDD);
					cell->setDispersionDmaxEffect(dmax);
					TraceModel("DISPERSION DMAX EFFECT:" + std::to_string(dmax));
					break;

				case DISPERSION:

					processDispersionEnvironmentalEffect(Tmin, Tmax, rain, rain_max,
							Kmax, N, SDD, vector_effect);
					TraceModel("populate cell survival matrix effet");
					cell->setDispersionEffect(vector_effect);
					TraceModel("DISPERSION EFFECT:");
					txt = "";
					for(unsigned int j=0; j<vector_effect.size(); j++){
						txt += std::to_string(vector_effect[j]) + ", ";
					}
					TraceModel(txt);
					break;

				case SURVIVAL:
					processSurvivalEnvironmentalEffect(Tmin, Tmax, rain_max,
							Kmax, N, matrix_effect);
					TraceModel("populate cell survival matrix effet");
					cell->setSurvivalEffect(matrix_effect);
					TraceModel("SURVIVAL EFFECT:");
					TraceModel("\n" + matrix_effect.print());
					break;

				case FECONDITY:
					processFecondityEnvironmentalEffect(sid, Tmin, Tmax, rain, rain_max,
							Kmax, N, SDD, matrix_effect);
					TraceModel("populate cell feconfity matrix effet");
					cell->setFecondityEffect(matrix_effect);
					TraceModel("FECONDITY EFFECT:");
					TraceModel("\n" + matrix_effect.print());
					break;
			}
		}
	}
	TraceModel("processEnvironmentalEffect end");
}

double getNbIndividus(const std::vector<std::shared_ptr<Individus>>& individus, int pop_id){
	vector<population_class> pop_classes;
	pop_classes.push_back({0, FEMALE});
	int Neggs = Individus::filterByClasses(individus, pop_id, pop_classes, INDIVIDUS_STATE(-1)).size();
	return((double)(individus.size() - Neggs));
}

double getNbJuvenils(const std::vector<std::shared_ptr<Individus>>& individus, int pop_id){
	vector<population_class> pop_classes;
	pop_classes.push_back({1, FEMALE});
	pop_classes.push_back({3, FEMALE});
	pop_classes.push_back({5, FEMALE});
	pop_classes.push_back({7, FEMALE});

	int Njuv = Individus::filterByClasses(individus, pop_id, pop_classes, INDIVIDUS_STATE(-1)).size();
	return (double)Njuv;
}

void processSticsAsphidEffect(Map* amap, Population* pop){
	TraceModel("processStcisAsphidEffect start");
	for(std::map<std::string, stics_var>::iterator it = stics.begin(); it != stics.end(); ++it){
		std::string stics_id = it->first;
		int sid = stoi(stics_id);
		stics_var v = it->second;

		TraceModel("search patch with id : " + stics_id);
		std::shared_ptr<Patch> patch = amap->getPatch(sid);
		TraceModel("found patch with id : " + std::to_string(sid));
		TraceModel("nb cells : " + std::to_string(patch->getCells().size()));

		std::vector<std::shared_ptr<Individus>>  indivs_tot = patch->getIndividus();
		double N = getNbIndividus(indivs_tot, pop->getId());

		double wlai = 1;
		double Kmax = 0;

		double Njuv = getNbJuvenils(indivs_tot, pop->getId());
		double Bfresh = v.syncs["mafraisfeuille"](); // quantité de biomasse foliaire fraîche
		double Bdry = v.syncs["mafeuilverte"](); // quantité de biomasse sêche folaire (feuille vivante)
		double BdryP = v.syncs["masec(n)"](); // quantité de biomasse sêche de la partie épigée de la plante.
		double Nplant = v.syncs["QNplante"]();
		double Pjuv = 0;
		if(N > 0){
			Pjuv = Njuv / N;
		}
		TraceModel(vle::fmt("Number of individus: %1%, juvenils: %2%, Pjuv: %3%") % N % Njuv % Pjuv);
		TraceModel(vle::fmt("Bfresh: %1%, Bdry: %2%, BdryP: %3%, Nplant: %4%") % Bfresh % Bdry % BdryP % Nplant);

		Kmax = processKmaxStics(Bfresh, Bdry, BdryP, Nplant, Pjuv);
		double Xmax = 0;
		if(Kmax > 0){
			Xmax = (double) N / Kmax;
			wlai = maxEffect(Xmax, LAI_ref, LAI_m, LAI_0, LAI_1, LAI_q);
		}
		TraceModel(vle::fmt("Xmax: %1%, wlai: %2%:") % Xmax % wlai);

		v.wlai = wlai;
		v.force_lai = v.syncs["lai(n)"]() * wlai;
		v.kmax = Kmax;
		patch->setKmax(Kmax);
	}
	TraceModel("processAsphidEffect end");
}

void processPrunusAsphidEffect(Map* amap, Population* pop){
	TraceModel("processPrunusAsphidEffect start");
	for(std::map<std::string, prunus_var>::iterator it = prunus.begin(); it != prunus.end(); ++it){
		std::string prunus_id = it->first;
		int sid = stoi(prunus_id);
		prunus_var v = it->second;

		TraceModel("search patch with id : " + prunus_id);
		std::shared_ptr<Patch> patch = amap->getPatch(sid);
		TraceModel("found patch with id : " + std::to_string(sid));
		TraceModel("nb cells : " + std::to_string(patch->getCells().size()));

		std::vector<std::shared_ptr<Individus>>  indivs_tot = patch->getIndividus();
		double N = getNbIndividus(indivs_tot, pop->getId());

		double wlai = 1;

		double Njuv = getNbJuvenils(indivs_tot, pop->getId());
		double Pjuv = 0;
		if(N > 0){
			Pjuv = Njuv / N;
		}
		TraceModel(vle::fmt("Number of individus: %1%, juvenils: %2%, Pjuv: %3%") % N % Njuv % Pjuv);
		double Kmax = processKmaxPrunier(prunus_id, Pjuv);
		double Xmax = 0;
		if(Kmax > 0){
			Xmax = N / Kmax;
			wlai = maxEffect(Xmax, LAI_ref, LAI_m, LAI_0, LAI_1, LAI_q);
		}
		TraceModel(vle::fmt("Kmax: %1%, Xmax: %2%, wlai: %3%") % Kmax % Xmax % wlai);

		v.wlai = wlai;
		v.lai = v.pre_lai() * wlai;
		v.kmax = Kmax;
		patch->setKmax(Kmax);
	}
	TraceModel("processPrunusAsphidEffect end");
}

void processPrunus(){
	TraceModel("processPrunus start");
	for(std::map<std::string, prunus_var>::iterator it = prunus.begin();
			it != prunus.end(); ++it)
	{
		std::string prunus_id = it->first;
//		int sid = stoi(prunus_id);
		prunus_var v = it->second;
		double nJ = (double)nJI;
		double GDD = meteo_history["default"]["GDD"][0];
		double SDD = meteo_history["default"]["SDD"][0];
		double LAI = 0.0;
		double Nfol = 0.0;
		double Nmobil = 0.0;
		double Bdry = 0.0;
		double Btotal = 0.0;
		double interest = 1.0;
		double Tmoy_moy = meteo_history["default"]["Tmoy_moy"][0];
		if( GDD < Prunus_GDD_bourgeon ){
			v.phasePheno = BEFORE_BOURGEON;
		}else{
			if( GDD >= Prunus_GDD_bourgeon  && GDD < Prunus_GDD_feuil ){
				Nfol = Prunus_Nfol;
				Nmobil = Prunus_Nmobil;
				v.phasePheno = BOURGEON;
			}else{ // GDD >= Prunus_GDD_feuil
				if( GDD >= Prunus_GDD_sen1 || ( Tmoy_moy < Prunus_Tseuil && GDD >= Prunus_GDD_sen2 ) ){
					LAI = v.lai(-1) / (1.0 + std::exp(Prunus_e * nJ + Prunus_f));
					v.phasePheno = LEAF_SENE;
				}else{
					LAI = (double)1.0 + std::exp(Prunus_a * nJ + Prunus_b);
					v.phasePheno = LEAF;
				}

				Bdry = LAI / Prunus_SLA;
				Btotal = Bdry / Prunus_LDMC;
				Nfol = Btotal * Prunus_LNC * surf_cellule;
				if(Btotal > 0){
					Nmobil = (Btotal - Bdry) / Btotal;
				}
				interest = Prunus_interest_min
						+ (Prunus_interest_max - Prunus_interest_min)
						/ ( (double)1.0 + std::exp( Prunus_c * nJ + Prunus_d ) );
			}
		}
		v.pre_lai = LAI;
		v.interest = interest;
		v.Nfol = Nfol;
		v.Nmobil = Nmobil;
		v.Bdry = Bdry;
		v.Btotal = Btotal;
		v.GDD = GDD;
		v.SDD = SDD;
		v.Tmoy_moy = Tmoy_moy;
		v.nJI = nJ;
	}
	TraceModel("processPrunus end");
}

double processKmaxPrunier(std::string sid, double Pjuv){
	TraceModel("processKmaxPrunier");
	prunus_var v = prunus[sid];
	TraceModel(vle::fmt("Nmobil: %1%, Nfol: %2%, interest: %3%") % v.Nmobil() % v.Nfol() % v.interest());
	double Kmax = processKmax(v.Nmobil(), Prunus_Ncapt, v.Nfol(), Pjuv) * v.interest();
	return Kmax;
}

virtual void initValue(const vd::Time& /*time*/)
{
	TraceModel("initValue...");


	for(std::map<std::string, stics_var>::iterator it = stics.begin(); it != stics.end(); ++it){
		TraceModel("initialize " + it->first);
		stics_var v = it->second;
		v.force_lai = 0;
	}
	TraceModel("initValue end!");
}

protected:

	Process myProcess;
	Map map01;
	Population pop01;
	int individus_last_id;

	int nJI = 0; // le nombre de jour variant de 1 (au moment où la quantité d’N foliaire du tournesol est >0) à x jours.

	// VAR, SYNC
	std::map<std::string, stics_var> stics;
	std::map<std::string, prunus_var> prunus;
	std::map<std::string, ve::DifferenceEquation::Multiple::Sync> meteo;

	std::vector<std::string> meteo_outputs = {"Co2", "ETP", "RG", "Rain", "Tmax", "Tmin", "Sun"};

	std::map< std::string, std::map<std::string, std::deque<double>> > meteo_history;
	std::deque<double> sun_history;

	std::map<std::string, int> IsexeMap;

	// taille de l'historique des données météo
	unsigned int meteo_history_size = 0;

	/**
	 *  vpz params
	 */
	string parameters_file;

	// parameters
	int seed = 123456789;
	int random_type = 0;

	int log_level = 3;

	int dispersion_on = 1;
	int survival_on = 0;
	int reproduction_on = 0;
	int pop_sex = 0;

	int nb_classes = 9;
	double sex_ratio = 1;
	int sexual_reproduction = 0;
	vector<p_range> dispersion_female = {{0.2, 0.9}, {0.2, 0.9}, {0.2, 0.9}, {0.2, 0.9}, {0.2, 0.9}, {0.2, 0.9}, {0.2, 0.9}, {0.2, 0.9}, {0.2, 0.9}};
	std::string survival_female_file = "./Lusignan/survival_female.csv";
	std::string fecondity_female_file = "./Lusignan/fecondity_female.csv";
	double fecondity_contrib_female = 1;

	int perceptual_range = 1;
	int memory_size = 3;
	double persistence_memory = 3;
	double persistence_target = 1;

	std::string file_landcover = "./Lusignan/Lusignan.tif";
	std::string file_nomenclature = "./Lusignan/Lusignan_nomenclature.csv";
	std::string file_population = "./Lusignan/population_10_cl4.shp";
	std::string dir_results = "./results";

	bool writeDynPop = true;
	bool writePatchPop = true;
	bool writePop = true;
	bool writeVisits = true;
	bool writePaths = true;
	bool writeDensity = false;
	bool writeKmaxPatch = false;

	int time_step;

	int life_cycle_counter = 0;

	// durée du cycle de vie
	int life_cycle_duration = 5;

	// taille de l'historique des données météo pour mean/min/max
	unsigned int meteo_mean_size = 5;

	// taille de l'historique des données météo pour linear regression
	unsigned int meteo_trend_size = 15;

	// limites inférieure/supérieure de température de développement
	double TDLow = 5, TDup = 30;

//	cl0 : oeuf
//	cl1 : juvenile, aptère, F
//	cl2 : adulte, aptère, F
//	cl3 : juvenile, ailé, F
//	cl4 : adulte, ailé, F
//	cl5 : juvenile, ailé, sexué, F, M
//	cl6 : adulte, ailé, sexué, F, M
//	cl7 : juvenile, aptère, sexué, F
//	cl8 : adulte, aptère, sexué, F

	// âge minimum d’un oeuf. Au-dessu, l’oeuf peut potentiellement devenir un juvénile.
	// âge maximum d’un oeuf. Au-dessu, l’oeuf meurt.
	int ageLow0 = 30, ageUp0 = 200;

	// âge max d’un individu juvenile. Au-dessu, l’individu meurt.
	int ageJ = 20;

	// âge max d’un individu adulte. Au-dessu, l’individu meurt.
	int ageA = 60;

	// somme des températures de développement que le passage juvénile à adulte nécessite.
	double SDDadult = 150;

	// somme des températures de développement que peut accumuler un individu adulte avant de mourir.
	double SDDdie = 400;

	// effet de la ressource
	double Nj = 5.34 * pow(10, -9); // kg/ind
	double Na = 7.35 * pow(10, -9); // kg/ind
	double Ncapt = 1; // (les pucerons ont accès à toute la ressource).

	int sP = 1; // paramètre définissant l’équivalence entre le nombre de puceron simulé	et réel (si sP = 1 alors 1 individu simulé correspond à 1 individu réel).

	// Coefficient de compétition
	double s0 = 0.001;

	double surf_cellule = 25*25;
	double surf = surf_cellule / 10000;

	/**
	 * Le taux de dispersion
	 */
	double Dmin = 0.2;
	double Dmax = 0.9;

	// Effet des températures minimales (**** idem W33 ****)

	// Effet des températures maximales (**** idem W33 ****)

	// Effet des fortes précipitations (**** idem W11 ****)

	// Effet direct des températures (**** idem W32 ****)

	// Effet direct des précipitations (**** idem W32 ****)

	/**
	 * Distance de dispersion
	 */
	int disp_dmax = 100; // cellules (pour une taille de pixel de 25m soit une distance max à parcourir de 2500m).

	double cost_stics0 = 5;
	double cost_stics1 = 1;

	double cost_prunus0 = 7;
	double cost_prunus1 = 8;

	/**
	 * Survie des oeufs (S11)
	 */

	// survie max
	double Smax11 = 0.9;

	// Effet des températures minimales
	double S11_Tref_tmin = -17.5;
	double S11_m_tmin = 0.5;
	double S11_T0_tmin = -35;
	double S11_T1_tmin = 17.5;
	double S11_q_tmin = 2;

	// Effet des températures maximales
	double S11_Tref_tmax = 0;
	double S11_m_tmax = 0.5;
	double S11_T0_tmax = -18;
	double S11_T1_tmax = 30;
	double S11_q_tmax = 2;

	// Effet des fortes précipitations
	double S11_Pref = -100;
	double S11_Pm = 0.5;
	double S11_P0 = -185;
	double S11_P1 = 100;
	double S11_Pq = 2;

	/**
	 * Survie des oeufs à juvéniles aptères asexués	(S12)
	 */

	// Survie max
	double Smax12 = 0.8;

	// Effet des températures minimales
	double S12_Tref_tmin = -10;
	double S12_m_tmin = 0.5;
	double S12_T0_tmin = -25;
	double S12_T1_tmin = 30.5;
	double S12_q_tmin = 2;

	// Effet des températures maximales
	double S12_Tref_tmax = -10;
	double S12_m_tmax = 0.5;
	double S12_T0_tmax = -18;
	double S12_T1_tmax = 30;
	double S12_q_tmax = 2;

	// Effet des fortes précipitations (**** idem W11 ****)

	/**
	 * Survie des juvéniles (S22, S44, S66 et S88)
	 */

	// Survie max
	double Smax22 = 0.85;

	// Effet des températures minimales (**** idem W12 ****)

	// Effet des températures maximales (**** idem W12 ****)

	// Effet des fortes précipitations (**** idem W11 ****)


	/**
	 * Survie des juvéniles à adultes (S23, S45, S67 et S89) et des adultes (S33, S55, S77 et S99)
	 */
	double Smax33 = 0.8;
	double Smax55 = 0.9;
	double Smax23 = 0.8;
	double Smax45 = 0.75;

//	double Smax99 = Smax33;
//	double Smax77 = Smax55;
//	double Smax89 = Smax23;
//	double Smax67 = Smax45;


	// Effet des températures minimales
	double S33_Tref_tmin = -6.9;
	double S33_m_tmin = 0.5;
	double S33_T0_tmin = -20;
	double S33_T1_tmin = 43;
	double S33_q_tmin = 2;

	// Effet des températures maximales
	double S33_Tref_tmax = 0;
	double S33_m_tmax = 0.5;
	double S33_T0_tmax = -8;
	double S33_T1_tmax = 35;
	double S33_q_tmax = 2;

	// Effet des fortes précipitations (**** idem W11 ****)

	/**
	 * 	Fécondité des adultes produisant aptères et oeufs (F32 , F52 , F78 et F91)
	 */
	int Fmax32 = 15;
	int Fmax52 = 10;
	int Fmax91 = 5;

	// Effet des températures minimales (**** idem W33 ****)

	// Effet des températures maximales (**** idem W33 ****)

	// Effet des fortes précipitations (**** idem W11 ****)

	// Effet direct des températures sur la fécondité (activité)
	double F32_TDlow = 5;
	double F32_TDup = 30;
	int F32_p = 5;
	double F32_m = 0.5;
	double F32_SDD0 = 0;
	double F32_SDD1 = 187.5;
	double F32_q = 2;

	double F32_SDDmax = F32_p * (F32_TDup - F32_TDlow);
	double F32_SDDref = F32_SDDmax / 2;

	// Effet direct des précipitations sur la fécondité
	double F32_Pref = 15;
	double F32_Pm = 0.5;
	double F32_P0 = -45;
	double F32_P1 = 30;
	double F32_Pq = 2;

	int Ftot_3 = 110; // descendants/adulte
	int Ftot_5 = 40; //descendants/adulte =Ftot 78
	int Ftot_9 = 5; // oeufs; adulte sexué
	int Ftot_7 = Ftot_5;

	std::vector<int> fecondity_max = {0, 0, Ftot_3, 0, Ftot_5, 0, Ftot_7, 0, Ftot_9};

	/**
	 * Fécondité des adultes aptères produisant des individus ailés asexués (F34 ) et sexués (F36 )
	 */
	int Fmax34 = 10;
	int Fmax36 = Fmax34;

	// Effet des températures minimales (**** idem W33 ****)

	// Effet des températures maximales (**** idem W33 ****)

	// Effet des fortes précipitations (**** idem W11 ****)

	// Effet direct des températures sur la fécondité (activité) (**** idem W32 ****)

	// Effet direct des précipitations sur la fécondité (**** idem W32 ****)

	// Différénciation de la production de descendants sexués et asexués
	int linearTrend = 0; // prend en compte ou non la tendance des températures et de la photo période
//	int nDay = 15; // jours
	double Seuil_photo = 13; // heures d’ensoleillement par jour.
	double p_threshold = 0.05;


	// Effet de la densité de puceron sur le LAI
	double LAI_ref = 0.5;
	double LAI_m = 0.5;
	double LAI_0 = -2;
	double LAI_1 = 1;
	double LAI_q = 2;
//	double LAI_opt = 0;


	// Prunier
//	double Prunus_GDD_j = 0; // au 1er janvier de l’année
	double Prunus_GDD_Tbase = 10; // °C
	double Prunus_GDD_feuil = 80; // °C
	double Prunus_dT = 25; // °C
	double Prunus_GDD_bourgeon = Prunus_GDD_feuil - Prunus_dT;
//	double Prunus_Nj = 5.34 * std::pow(10, -9); //  kg/ind (Newman et al. 2003).
//	double Prunus_Na = 7.35 * std::pow(10, -9); // kg/ind (Newman et al. 2003).
	double Prunus_Nmobil = 1; // On fixe Nmobil = 1 car l’azote mobilisable est déjà pris en compte dans la valeur Nfol.
	double Prunus_Ncapt = 0.25; // (signifie de 25% de la surface de la cellule est occupé par des plantes).
//	double cell_surf = 25 * 25; // surface de la cellule (m^2).
	double Prunus_Nfol_base = 2.67 * std::pow(10, -6); // 5.34 * std::pow(10, -6)
	double Prunus_Nfol = Prunus_Nfol_base * surf_cellule; // (exprimée en kg d’N par cellules si le prunier occuper 100% de la cellule). Cette valeur permet d’acceuillir au max 1000 juvéniles par mš.

//	double Prunus_SeuilJ = 40; // jours
	double Prunus_LAI_max = 3; // (peut aller jusqu’à 5 mais je prend une valeur de 3 car en environnement forestier)
	double Prunus_a = -0.1325417; // si j=1(coefficient permettant d’avoir un LAI max atteint en 40 jours ; ajusté par NLS)
	double Prunus_b = 2.6509; // (coefficient permettant d’avoir un LAI max atteint en 40 jours ; ajusté par NLS)
	double Prunus_SLA = 11.3; // mš feuille/kg de MS feuille (trait publi CAZA)
	double Prunus_LDMC = 0.368; // kg de MS feuille/kg feuille (trait publi CAZA)
	double Prunus_LNC = 0.0194; // kg d’N/kg de feuille (trait publi CAZA ; normalement c’est un contenu rapporté au poids feuille MS ; on extrapole ici à la biomasse totale)

//	double Prunus_Nj = 5.34 * std::pow(10, -9); // kg/ind (Newman et al. 2003).
//	double Prunus_Na = 7.35 * std::pow(10, -9); // kg/ind (Newman et al. 2003).
	double Prunus_interest_max = 1;
	double Prunus_interest_min = 0.15;
	double Prunus_c = 0.1768137; // (coefficient permettant d’avoir un intérêt min atteint en 30 jours ; ajusté par NLS)
	double Prunus_d = -2.6522059; // (coefficient permettant d’avoir un intérêt min atteint en 30 jours ; ajusté par NLS)

	double Prunus_GDD_sen1 = 1300; //°C
	double Prunus_GDD_sen2 = 1000; //°C
	double Prunus_Tseuil = 10; //°C
	double Prunus_e = 0.1768137; // (coefficient permettant d’avoir un LAI=0 atteint en 30 jours ; ajusté par NLS)
	double Prunus_f = -2.6522059; // (coefficient permettant d’avoir un LAI=0 atteint en 30 jours ; ajusté par NLS)
};

} // namespace VleSticsDynaLand

DECLARE_DYNAMICS(VleAphidsDynaLand::AphidsDynaLandDynamics)
