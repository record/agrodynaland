#setwd("/home/grobaldo/Documents/Projets/METACONNECT/AgroDynaLand/VleAphidsDynaLand/R")

clim <- read.csv("Lusignan-2012-2016.csv", header = TRUE, sep=";")
clim <- clim[2:nrow(clim), ]
clim$Tmoy <- (clim$TX + clim$TN) / 2
clim$a1 <- c(0)
clim$b1 <- c(0)
clim$p1 <- c(0)
clim$a2 <- c(0)
clim$b2 <- c(0)
clim$p2 <- c(0)
clim$t <- c(0)
clim$Isexe <- c(0)
nDay <- 15
Seuil_photo <- 13
step <- 365
clim2 <- clim[NULL,]
for(i in 3:step){
  n <- min(nDay, i)
  clim2 <- clim2[NULL,]
  clim2 <- clim[(i-n+1):i,]
  clim2$t <- 0:(n-1)
  Tmoy <- clim2[1, c("Tmoy")]
  clim2$dT <- clim2$Tmoy - Tmoy
  clim2_lm <- lm(data=clim2, dT~t)
  clim2_coef <- coefficients(clim2_lm)
  clim[i, ]$a1 <- clim2_coef[2]
  clim[i, ]$b1 <- clim2_coef[1]
  clim[i, ]$p1 <- summary(clim2_lm)$coefficients[2,4]
  
  if(clim[i, ]$a1 >= 0 || clim[i, ]$p1 > 0.05){
    Isexe = 0
  }else{
    Isexe = 1
    Sun0 <- clim2[1, c("day_length")]
    clim2$dSun <- clim2$day_length - Sun0
    clim2_lm <- lm(data=clim2, dSun~t)
    clim2_coef <- coefficients(clim2_lm)
    clim[i, ]$a2 <- clim2_coef[2]
    clim[i, ]$b2 <- clim2_coef[1]
    clim[i, ]$p2 <- summary(clim2_lm)$coefficients[2,4]
    if(clim[i, ]$a2 >= 0 || clim[i, ]$p2 > 0.05){
      Isexe = 0
    }
    Sun <- clim2[nrow(clim2), c("day_length")]
    if(Sun > Seuil_photo){
      Isexe = 0
    }
    clim[i, ]$Isexe = Isexe
  }
}

print(paste("Isexe value(s):", unique(clim$Isexe)))
