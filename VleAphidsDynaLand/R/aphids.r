# /*******************************************************************************
#  *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
#  *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
#  *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
#  *
#  *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
#  *  Copyright (c) 2017 INRA http://www.inra.fr
#  *
#  *  See the AUTHORS or Authors.txt file for copyright owners and
#  *  contributors
#  *
#  *  This program is free software: you can redistribute it and/or modify
#  *  it under the terms of the GNU General Public License as published by
#  *  the Free Software Foundation, either version 3 of the License, or (at
#  *  your option) any later version.
#  *
#  *  This program is distributed in the hope that it will be useful, but
#  *  WITHOUT ANY WARRANTY; without even the implied warranty of
#  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  *  General Public License for more details.
#  *
#  *  You should have received a copy of the GNU General Public License
#  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  *******************************************************************************/
library(rvle)

# load model
model <- new("Rvle", "aphids.vpz", "VleAphidsDynaLand")

# list conditions
rvle.listConditions(rvleHandle=model@sim)

# list paramters
cond <- "cond_dynaland"
data_cond <- c()
for(p in rvle.listConditionPorts(rvleHandle=model@sim, condition=cond)){
  v <- rvle.getConditionPortValues(rvleHandle=model@sim, condition=cond, port=p)
  print(paste(p, v, sep=": "))
  data_cond <- rbind(data_cond, c(p, v))
}
data_cond
write.table(file = "aphids_params.csv", x = data_cond, sep=";", dec=".", col.names = c("param", "value"), row.names=FALSE)
# change parameters
rvle.setIntegerCondition(rvleHandle=model@sim, condition=cond, port="sP", value=10)

# get simulation duration
print(paste("simulation duration", rvle.getDuration(rvleHandle=model@sim), sep=": "))

# run model
dir.create("results")
file.remove( list.files(path = "./results", full.names = TRUE) )
res <- run(model)  # don't care about warning because there is no memory results

# read result

# read file result
list.files(path="./results")
