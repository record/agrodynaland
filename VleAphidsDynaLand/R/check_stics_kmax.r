#Bfresh = stics[sid].syncs["mafraisfeuille"](); // quantité de biomasse foliaire fraîche
#Bdry = stics[sid].syncs["mafeuilverte"](); // quantité de biomasse sêche folaire (feuille vivante)
#BdryP = stics[sid].syncs["masec(n)"](); // quantité de biomasse sêche de la partie épigée de la plante.
#Nplant = stics[sid].syncs["QNplante"]();

#Top model:STICS_49.mafraisfeuille	Top model:STICS_49.mafeuilverte	Top model:STICS_49.masec(n)	Top model:STICS_49.QNplante
#3.6282269955	0.3834307194	1.7384136915	45.5369033813
Bfresh = 3.6282269955
Bdry = 0.3834307194
BdryP = 1.7384136915
Nplant = 45.5369033813
Btotal = Bfresh + Bdry
Nmobil = Bfresh / Btotal
surf_cellule = 25
surf = surf_cellule / 10000

Nfol = surf * Nplant * (Bdry / BdryP) * 1000
Pjuv = 0
Nj = 5.34 * 10^-9
Na = 7.35 * 10^-9
Ncapt = 1
tmp = Nj * Pjuv + Na * (1 - Pjuv);
if(tmp > 0){
  Kmax = ( Ncapt * Nmobil * Nfol ) / tmp;
}

