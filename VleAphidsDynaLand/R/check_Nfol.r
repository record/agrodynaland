Bdry <- 1.29675 # t.ha^1
BdryP <- 4.6959 # t.ha^1
B <- (Bdry / BdryP) # NA
Nplant <- 96.8433 # kg.N.ha^-1
N <- Nplant / 10000 # kg.N.m^-2
Nfol <- N * B # kg.N.m^-2
surf_cell <- 25 # m^2
Nfol_cell <- surf_cell * Nfol # kg.N

Bfresh <- 12.5849 # t.ha^1
Btotal <- Bfresh + Bdry # t.ha^1
Nmobil <- Bfresh / Btotal # NA
sP <- 1
Pjuv <- 0 # % individus
Nj <- 2.67 * 10^-9 # kg.N.individu^1 # 5.34
Na <- 7.35 * 10^-9 # kg.N.individu^1
tmp <- sP * Nj * Pjuv + Na * (1.0 - Pjuv)  # kg.N.individu^1
Ncapt <- 0.25 # %
if(tmp > 0){
  Kmax = ( Ncapt * Nmobil * Nfol_cell ) / tmp # individu
}
