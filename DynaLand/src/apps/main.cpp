/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/global.hpp>
#include <dynaland/Logger.hpp>
#include <dynaland/MainFunction.hpp>
#include <dynaland/Map.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/Process.hpp>

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <ctime>

using namespace dynaland;

int main(int argc, char *argv[]) {
	Logger::info("RUNNING...\n\n");
	Logger::debug("argc: %d\n", argc);
	double c_elapsed_secs;
	double t_elapsed_secs;
	clock_t c_begin = clock();

	double t_begin = MainFunction::get_wtime();

	Process myProcess;
	Map map01;
	Population pop01;
	int individus_last_id = 0;
	int duration = 0;

	std::string data_dir = std::string(".") + PATH_SEP + std::string("data");
	std::string parameters_file = "properties.ini";

	if(argc==2){
		Logger::debug("argv: %s\n", argv[1]);
		parameters_file = argv[1];
	}else{
		parameters_file = data_dir + PATH_SEP + parameters_file;
	}

	MainFunction::initFromFile(parameters_file, data_dir, &pop01, &map01, &myProcess, &duration, &individus_last_id);

	clock_t c_init = clock();
	double t_init =  MainFunction::get_wtime();

	c_elapsed_secs = double(c_init - c_begin) / CLOCKS_PER_SEC;
	t_elapsed_secs = t_init - t_begin;
	Logger::debug("init done! elapsed time : %f s (cpu: %f s)\n", t_elapsed_secs, c_elapsed_secs);

	for(int t = 1; t <= duration; t++){
		Logger::info("----------------------------------\n");
		Logger::info("----- time %d\n\n", t);

		myProcess.clearResults(&pop01);

		MainFunction::compute(&pop01, &map01, &myProcess, &individus_last_id);

		if(MainFunction::writeDynPop) myProcess.writeDynPop(t, &pop01);
		if(MainFunction::writePatchPop) myProcess.writePatchPop(t, &map01, &pop01);
		if(MainFunction::writePop) myProcess.writePop(&map01, &pop01);
		if(MainFunction::writePaths) myProcess.writePaths(&map01, &pop01);
		if(MainFunction::writeDensity) myProcess.writeDensity(&map01, &pop01);
		if(MainFunction::writeVisits) myProcess.writeVisits(&map01, &pop01);

	}
	Logger::info("----------------------------------\n");
	MainFunction::finish(&pop01, &map01, &myProcess);

	clock_t c_end = clock();
	double t_end = MainFunction::get_wtime();
	c_elapsed_secs = double(c_end - c_begin) / CLOCKS_PER_SEC;
	t_elapsed_secs = t_end - t_begin;
	Logger::info("\nDONE! elapsed time : %f s (cpu: %f s)\n", t_elapsed_secs, c_elapsed_secs);
	return(0);
}


