/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/Cell.hpp>
#include <dynaland/global.hpp>
#include <dynaland/Individus.hpp>
#include <dynaland/Logger.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/Patch.hpp>

#include <vector>
#include <deque>
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <algorithm>

using namespace dynaland;

namespace dynaland {

Individus::Individus(){
	this->id = 0;
	this->age = 0;
	this->sdd = 0;
	this->sex = SEX(-1);
	this->pop_class = 0;
	this->population = 0;
	this->state = SEDENTARY;
	this->path.clear();
	this->canBeUpgraded = true;
	this->reinitAgeOnUpgrade = false;
	this->dispersionDmax = 0;
}

Individus::Individus(const Population* population,
		const shared_ptr<Cell>& cell, const int id, const int age,
		const SEX& sex, const int pop_class) {
	this->id = id;
	this->age = age;
	this->sdd = 0;
	this->sex = sex;
	this->pop_class = pop_class;
	this->population = population;
	this->cell = cell;
	this->state = SEDENTARY;
	this->path.clear();
	this->canBeUpgraded = true;
	this->reinitAgeOnUpgrade = false;
	this->dispersionDmax = population->getDispersionPathMaxCells();
}

Individus::~Individus(){
	this->path.clear();
	this->population = 0;
}

int Individus::getId(){
	return id;
}

bool operator== (const Individus& individu1, const Individus& individu2){
	return((individu1.id == individu2.id));
}

bool operator== (const weak_ptr<Individus>& individu1, const weak_ptr<Individus>& individu2){
	return((individu1.lock()->id == individu2.lock()->id));
}

int Individus::getAge(){
	return age;
}

SEX Individus::getSex(){
	return sex;
}

int Individus::getMemorySize(){
	return this->getPopulation()->getMemorySize();
}

int Individus::getPerceptualRange(){
	return this->getPopulation()->getPerceptualRange();
}

double Individus::getPersistenceMemory(){
	return this->getPopulation()->getDirectionalMemoryPersistence();
}

double Individus::getPersistenceTarget(){
	return this->getPopulation()->getDirectionalGoalPersistence();
}

int Individus::getPathMaxCells(){
	return this->getPopulation()->getDispersionPathMaxCells();
}

int Individus::getState(){
	return state;
}

const Population* Individus::getPopulation(){
	return this->population;
}

const std::vector<std::pair<int, std::weak_ptr<Cell>>>& Individus::getPath(){
	return this->path;
}

std::shared_ptr<Cell> Individus::getCell(){
	return this->cell.lock();
}

std::shared_ptr<Cell> Individus::getPathLastCell(){
	if(this->getPath().size() > 1){
		return(this->getPath().at(this->getPath().size() - 2).second.lock());
	}else{
		return 0;
	}
}

std::shared_ptr<Patch> Individus::getPatch(){
	return (getCell())->getPatch();
}

std::shared_ptr<Patch> Individus::getLastPatch(){
	if(this->getPath().size() > 1){
		return this->getPath()[0].second.lock()->getPatch();
	}else{
		return this->getPatch();
	}
}

void Individus::setState(const int state){
	this->state = state;
}

void Individus::setId(const int id){
	this->id = id;
}

void Individus::setAge(const int age){
	this->age = age;
}

void Individus::setSex(const SEX& sex){
	this->sex = sex;
}

int Individus::getClass(){
	return this->pop_class;
}

void Individus::setTemperatureGrowthSum(double sdd){
	this->sdd = sdd;
}

double Individus::getTemperatureGrowthSum(){
	return this->sdd;
}

void Individus::addTemperatureGrowth(double DDday){
	this->sdd += DDday;
}

void Individus::upgradeTo(int pop_class){
	this->pop_class = pop_class;
	if(initAgeOnUpgrade()){
		this->age = 0;
		setInitAgeOnUpgrade(false);
	}
}

bool Individus::initAgeOnUpgrade(){
	return reinitAgeOnUpgrade;
}

void Individus::setInitAgeOnUpgrade(bool b){
	this->reinitAgeOnUpgrade = b;
}

void Individus::addAge(int age){
	this->age += age;
}

bool Individus::canUpgrade(){
	return this->canBeUpgraded;
}

void Individus::setCanUpgrade(bool up){
	this->canBeUpgraded = up;
}

const vector<shared_ptr<Cell>> Individus::getMemorizedCells(){
	vector<shared_ptr<Cell>> cells;
	int path_size = this->getPath().size();
	int max_cells = min(path_size, this->getMemorySize()+1);
	for(int i = 2; i <= max_cells; i++){
		cells.push_back(this->getPath().at(path_size - i).second.lock());
	}
	return cells;
}

vector<shared_ptr<Individus>> Individus::getHarem(){
	return this->harem;
}


void Individus::setPopulation(const Population* population){
	this->population = population;
}

void Individus::setClass(const int pop_class){
	if(pop_class < this->getPopulation()->getNbClasses()){
		this->pop_class = pop_class;
	}else{
		string exp = "Wrong class number : " + pop_class;
		throw std::runtime_error(exp);
	}
}

void Individus::clearPath(){
	this->path.clear();
}

void Individus::setCell(const shared_ptr<Cell>& cell){
	this->cell = cell;
}

void Individus::addPathCell(const int step, const shared_ptr<Cell>& cell){
	this->path.push_back(std::make_pair(step, cell));
}

void Individus::clearHarem(){
	this->harem.clear();
}

void Individus::addHaremIndividus(const shared_ptr<Individus>& individus){
	this->harem.push_back(individus);
}

void Individus::removeHaremIndividus(const shared_ptr<Individus>& individus){
	harem.erase(std::remove(harem.begin(), harem.end(), individus), harem.end());
}

void Individus::addChild(shared_ptr<Individus> child){
	this->children.push_back(child);
}

int Individus::getNbChildren(){
	return this->children.size();
}

shared_ptr<Individus> Individus::reproduce(
		const shared_ptr<Individus>& partenaire,
		const int id, const SEX& sex, const int pop_class){
	shared_ptr<Individus> child = make_shared<Individus>(Individus(this->getPopulation(), this->getCell(), id, 0, sex, pop_class));
	return child;
}

vector<shared_ptr<Individus>> Individus::filter(
		const vector<shared_ptr<Individus>>& individus,
		const int pop_id,
		const int pop_class,
		const SEX& sex,
		const INDIVIDUS_STATE& state){
	vector<shared_ptr<Individus>> result;
	for(vector<shared_ptr<Individus>>::const_iterator it = individus.begin(); it != individus.end(); it++) {
		const shared_ptr<Individus>& individu  =(*it);
		if( ( pop_id < 0 || individu->getPopulation()->getId() == pop_id )
				&& ( pop_class < 0 || individu->getClass() == pop_class )
				&& ( sex < 0 || individu->getSex() == sex )
				&& ( state < 0 || individu->getState() == state ) ) {
			result.push_back(individu);
		}
	}
	return result;
}

vector<shared_ptr<Individus>> Individus::filterByClasses(
		const vector<shared_ptr<Individus>>& individus,
		const int pop_id,
		const vector<population_class>& pop_class,
		const INDIVIDUS_STATE& state)
{
	vector<shared_ptr<Individus>> result;
	vector<shared_ptr<Individus>> sub_pop;
	for(vector<population_class>::const_iterator it = pop_class.begin();
		it != pop_class.end(); it++)
	{
		population_class pc = (*it);
		sub_pop = Individus::filter(individus, pop_id, pc.pop_class, pc.sex, state);
		result.insert(result.end(), sub_pop.begin(), sub_pop.end());
	}
	return result;
}

void Individus::print(){
	Logger::debug("id: %d, class: %d, sex: %d, age: %d\n", id, pop_class, sex, age);
}

int Individus::getDispersionDmax(){
	return this->dispersionDmax;
}

void Individus::setDispersionDmax(int dmax){
	this->dispersionDmax = dmax;
}

} // namespace dynaland
