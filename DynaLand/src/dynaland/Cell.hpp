/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef CELL_H_
#define CELL_H_

#include <dynaland/global.hpp>
#include <dynaland/Matrix.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <memory>

using namespace dynaland;

namespace dynaland {

class Individus;
class Population;
class Map;
class Patch;

class Cell {

public:
	Cell();
	Cell(Map* landscape, const gpoint& xy, const int id, const bool isBorder);
	Cell(Map* landscape, const gpoint& xy, const int id, const int id_occ,
			const double k, const double cost, const bool isBorder);
	virtual ~Cell();

	friend bool operator== (const Cell& cell1, const Cell& cell2);
	friend bool operator== (const std::shared_ptr<Cell>& cell1, const std::shared_ptr<Cell>& cell2);
	static bool isBefore(const std::shared_ptr<Cell>& cell1, const std::shared_ptr<Cell>& cell2) {
		return cell1->id < cell2->id;
	}
	int getId();
	int getIdOcc();

	bool isBorder();

	const gpoint getCoord();
	const std::vector< std::weak_ptr<Individus> >& getIndividus();
	std::vector< std::shared_ptr<Individus> > getIndividus2();
	std::vector<const Population*> getPopulations();
	int getNbIndividus();
	double getProperty(const CELL_PROPERTY& property);
	Map* getMap();

	void clearCosts();
	void initCosts(const int& perceptual_range, const CELL_PROPERTY& property);
	void initNeighbors();

	std::vector<double> getNeighborsCosts(const int& perceptual_range);
	void setNeighborsCosts(const std::vector<double>& costs);
	std::shared_ptr<Patch> getPatch();
	std::vector< std::weak_ptr<Cell> > getNeighborsCells();
	void setNeighborsCells(const std::vector< std::weak_ptr<Cell> >& neighbors);
	void setNeighborsCells(const std::vector< std::shared_ptr<Cell> >& neighbors);

	void addIndividus(const std::weak_ptr<Individus>& individus);
	void removeIndividus(const std::weak_ptr<Individus>& individus);
	void setIndividus(const std::vector< std::weak_ptr<Individus> >& individus);

	void incrementVisit();
	void initVisit();
	void incrementVisitTot();

	void setProperty(const CELL_PROPERTY& property, const double& value);

	void print();
	std::string info();
	void print(const CELL_PROPERTY& property);
	void printAbundance();
	void printIndividus();

	Matrix getFecondityEffect();
	Matrix getSurvialEffect();
	std::vector<double> getDispersionEffect();

	void setFecondityEffect(const Matrix& m);
	void setSurvivalEffect(const Matrix& m);
	void setDispersionEffect(const std::vector<double>& v);
	void setDispersionDmaxEffect(int dmax);

public:
	int id, id_occ;
	double cost, k, visits, visits_tot;
	bool border;
	gpoint xy;
	std::vector< std::weak_ptr<Individus> > list_individus;

	std::vector<double> neighbors_costs;
	std::vector< std::weak_ptr<Cell> > neighbors_cells;

	Matrix fecondityEffect;
	Matrix survivalEffect;
	std::vector<double> dispersionEffect;

	Map* landscape;
};

} //namespace dynaland

#endif /* CELL_H_ */
