/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/Cell.hpp>
#include <dynaland/global.hpp>
#include <dynaland/Individus.hpp>
#include <dynaland/Logger.hpp>
#include <dynaland/Map.hpp>
#include <dynaland/Matrix.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/Process.hpp>
#include <dynaland/random/RandomGenerator.hpp>

#include <vector>
#include <string>
#include <memory>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <omp.h>
#include <stdexcept>

using namespace std;
using namespace dynaland;

namespace dynaland {

Process::Process(){
	this->dispersion_on = 0;
	this->survival_on = 0;
	this->reproduction_on = 0;
	this->time = 0;
}

Process::Process(int seed, int random_type, int dispersion_on, int survival_on, int reproduction_on, std::string dir_results){
	this->myRandom = dynaland::random::RandomGenerator(seed, random_type);
	this->dispersion_on = dispersion_on;
	this->survival_on = survival_on;
	this->reproduction_on = reproduction_on;
	this->dir_results = dir_results;
	this->time = 0;
}

Process::~Process(){}

int Process::dispersion(Map* amap, Population* pop, int step){
	Logger::debug("Process::dispersion start\n");

	const vector<int>& directions = {E, NE, N, NW, W, SW, S, SE};
	vector<shared_ptr<Cell>> cells = amap->getCells();
	vector<dispersion_step> steps;

//	int path_max_cells = pop->getDispersionPathMaxCells();
//	for(int step=0; step < path_max_cells; step++)
//	{
//		Logger::info("\n--> SMS STEP %d\n", step);

		#pragma omp parallel for
		for(unsigned int cell_index = 0; cell_index < cells.size(); cell_index++)
		{
			shared_ptr<Cell> cell = cells[cell_index];
			double N = cell->getNbIndividus();
			double k = cell->getProperty(CARRYING_CAPACITY);
			if(step == 0){
				cell->initVisit();
			}
			if( N > 0 ) {
				Logger::info("\n--> %s\n", cell->info().c_str());
//				cell->print();
				double dd = 1;
				std::vector<double> vdisp = cell->getDispersionEffect();
				if(vdisp.size() == 0){
					if(pop->getDispersionDD()){
						dd = 1 - exp(-k * N);
					}else{
						dd = 1;
					}
				}
				const vector<weak_ptr<Individus>>& sub_population2 = cell->getIndividus();
				vector<weak_ptr<Individus>> cell_pop;

				for(unsigned int indiv_index = 0; indiv_index < sub_population2.size(); indiv_index++){
					shared_ptr<Individus> individus = sub_population2[indiv_index].lock();

					double p_dd = 0;
					p_range p = pop->getDispersion(individus->getClass(), individus->getSex());
					if(vdisp.size() == 0){
						if( p.p_max != p.p_min){
							p_dd = p.p_min + (p.p_max - p.p_min) * dd - (individus->getState() == DISPERSION) * p.p_min;
						}else{
							p_dd = p.p_max;
						}
					}else{
						p_dd = vdisp[individus->getClass()] - (individus->getState() == DISPERSION) * p.p_min;
					}
					if(step == 0 || individus->getState() == SEDENTARY){
						individus->clearPath();
						individus->setState(SEDENTARY);
					}
					int dmax = individus->getDispersionDmax();
					Logger::debug("p_dd: %f, dmax: %d\n", p_dd, dmax);
					if(p_dd > 0 && dmax > 0 && myRandom.Bernoulli(p_dd)){

						shared_ptr<Cell> cellPrevious = individus->getPathLastCell();
						if(cellPrevious){
							Logger::debug("last path cell: (%d, %d)\n", cellPrevious->getCoord().x, cellPrevious->getCoord().y);
						}else{
							Logger::debug("last path cell: none\n");
						}
						if(cellPrevious && cellPrevious == cell){
							cellPrevious->print();
							throw(std::runtime_error("same cell"));
						}
						vector<shared_ptr<Cell>> memorizedCells = individus->getMemorizedCells();

						vector<double> probas = amap->computeSMSprobabilities(
										cell,
										cellPrevious,
										0,
										pop->getDirectionalMemoryPersistence(),
										pop->getDirectionalGoalPersistence(),
										memorizedCells,
										pop->getPerceptualRange());

						double p_cell = myRandom.Random();
//						Logger::debug("p: %f\n", p_cell);
						double proba_sum = 0;
						int new_path_cell = 0;
						for(unsigned int d = directions[0]; d <= directions.size(); d++){
							proba_sum += probas[d-1];
							Logger::debug("p_%d: %f, sum: %f\n", d-1, probas[d-1], proba_sum);
						}
						proba_sum = 0;
						for(unsigned int d = directions[0]; d <= directions.size(); d++){
							proba_sum += probas[d-1];
//							Logger::debug("p_{}: {}, sum: {}", d-1, probas[d-1], proba_sum);
							if(p_cell < proba_sum){
								new_path_cell = 1;
								if(individus->getState() == SEDENTARY){
									individus->addPathCell(step, cell);
								}
								individus->setState(DISPERSION);
								shared_ptr<Cell> selectedCell = amap->getCellNeighbor(cell->getCoord(), DIRECTION(d));
								if(!selectedCell->isBorder()){
									individus->addPathCell(step, selectedCell);
								}
//								Logger::debug("selected cell: %d (%d, %d)\n", d, selectedCell->getCoord().x, selectedCell->getCoord().y);

								#pragma omp critical
								{
//									Logger::debug("disperse indiv %d\n", individus->getId());
									dispersion_step disp_st = {step, individus, cell, selectedCell};
									steps.push_back(disp_st);
								}
								break;
							}
						}
						if(new_path_cell == 0){
							cell_pop.push_back(sub_population2[indiv_index]);
//							throw(std::runtime_error("path cell not found!"));
							Logger::info("path cell not found!\n");
						}
					}else{
//						Logger::debug("  not dispersing\n");
						cell_pop.push_back(sub_population2[indiv_index]);
					}
					individus->setDispersionDmax( (dmax--) );
				}
				cell->setIndividus(cell_pop);
			}
		}

		int nb_dispersers = steps.size();
		Logger::info("--> %d disperser(s)\n", nb_dispersers);

//		#pragma omp parallel for
		for(unsigned int step_index = 0; step_index < steps.size(); step_index++){
			dispersion_step step = steps[step_index];
			shared_ptr<Cell> selectedCell = step.to;
			shared_ptr<Individus> individus = step.who;

			if(selectedCell->isBorder()){
				Logger::debug("disperse indiv outside landscape...\n");
				int border_side1 = myRandom.IRandom(0, 1);
				int border_side2 = myRandom.IRandom(0, 1);
				gpoint xy;
				if(border_side1 == 0){
					xy.x = std::max(1, (border_side2 * (amap->ncol - 2)));
					xy.y = myRandom.IRandom(1, (amap->nrow - 1) );
				}else{
					xy.x = myRandom.IRandom(1, (amap->ncol - 1) );
					xy.y = std::max(1, (border_side2 * (amap->nrow - 2)));
				}

				shared_ptr<Cell> newselectedCell = amap->getCell(xy);
				individus->addPathCell(step.i, newselectedCell);
				Logger::debug("disperse indiv %d from (%d, %d) to (%d, %d)\n",
						step.who->getId(),
						step.from->getCoord().x, step.from->getCoord().y,
						newselectedCell->getCoord().x, newselectedCell->getCoord().y
				);
				newselectedCell->addIndividus(individus);
				newselectedCell->incrementVisit();
				newselectedCell->incrementVisitTot();
				individus->setCell(newselectedCell);
			}else{
				Logger::debug("disperse indiv %d from (%d, %d) to (%d, %d)\n",
						step.who->getId(),
						step.from->getCoord().x, step.from->getCoord().y,
						selectedCell->getCoord().x, selectedCell->getCoord().y
				);
				selectedCell->addIndividus(individus);
				selectedCell->incrementVisit();
				selectedCell->incrementVisitTot();
				individus->setCell(selectedCell);
			}
		}

		steps.clear();

//	}
	Logger::debug("Process::dispersion end\n");
	return nb_dispersers;
}

void Process::survival(Map* amap, Population* pop){
	Logger::debug("Process::survival start\n");

	vector<shared_ptr<Individus>> full_pop;
	vector<shared_ptr<Cell>> cells = amap->getCells();

	#pragma omp parallel for
	for(unsigned int cell_index = 0; cell_index < cells.size(); cell_index++){

		shared_ptr<Cell> cell = cells[cell_index];
		int N = cell->getNbIndividus();

		if( N > 0 ) {
			Logger::info("\n--> %s\n", cell->info().c_str());

			vector<shared_ptr<Individus>> deads;
			vector< std::pair<shared_ptr<Individus>, int> > upgradeds;
			vector<weak_ptr<Individus>> cell_pop;
			double k = 0, dd = 1;
			Matrix p_surv = cell->getSurvialEffect();

			if(p_surv.size() == 0){
				if(pop->getSurvivalDD()){
					k = cell->getProperty(CARRYING_CAPACITY);
					dd = exp(-k * N);
				}
			}

			//vector<shared_ptr<Individus>> individus = cell->getIndividus2();
			const vector<shared_ptr<Individus>>& individus = cell->getIndividus2();

			for(int pop_class = (pop->getNbClasses()-1);
				pop_class >= 0; pop_class--)
			{
				for(int sex = SEX(FEMALE); sex <= SEX(MALE); sex++){

					vector<shared_ptr<Individus>> sub_population = Individus::filter(individus, pop->getId(), pop_class, SEX(sex), INDIVIDUS_STATE(-1));
					if(sub_population.size() > 0){

						double p_auto_dd = 0;
						if(cell->getSurvialEffect().size() == 0){
							double p_auto = pop->getAutoSurvival(pop_class, SEX(sex));
							p_auto_dd = p_auto * dd;
						}else{
							p_auto_dd = p_surv.at(pop_class, pop_class);
						}
						Logger::debug("auto survival %d -> %d : %f\n", pop_class, pop_class, p_auto_dd);
						for(vector<shared_ptr<Individus>>::iterator it2 = sub_population.begin(); it2 != sub_population.end(); it2++)
						{
							shared_ptr<Individus> individu = (*it2);

							bool notUpgraded = true;
							bool notDead = true;

							if(individu->canUpgrade()) {
								for(int pop_class_to = (pop->getNbClasses()-1);
										pop_class_to >= 0; pop_class_to--){

									if( (pop_class_to != pop_class) && notUpgraded){

										double p_survi_dd = 0;
										if(cell->getSurvialEffect().size() == 0){
											double p_survi = pop->getSurvival(pop_class, pop_class_to, SEX(sex));
											p_survi_dd = p_survi * dd;
										}else{
											p_survi_dd = p_surv.at(pop_class, pop_class_to);
										}
										Logger::debug("survival %d -> %d : %f\n", pop_class, pop_class_to, p_survi_dd);

										if( p_survi_dd > 0 && myRandom.Bernoulli(p_survi_dd) ){
											notUpgraded = false;

											upgradeds.push_back(std::make_pair(individu, pop_class_to));

											#pragma omp critical
											pop->nb_survival[sex][pop_class_to]++;

											break;
										}
									}
								}
							}
							if( notUpgraded  ){
								if( !myRandom.Bernoulli(p_auto_dd) ){
									notDead = false;

									#pragma omp critical
									pop->nb_dead[sex][pop_class]++;
								}else{
									#pragma omp critical
									pop->nb_auto_survival[sex][pop_class]++;
								}
							}
							if(notDead){
								cell_pop.push_back(individu);

								#pragma omp critical
								full_pop.push_back(individu);
							}
						}
					}
				}
			}
			for(vector< std::pair< shared_ptr<Individus>, int> >::iterator it3 = upgradeds.begin();
				it3 != upgradeds.end(); it3++){
				shared_ptr<Individus> individus = (*it3).first;
//				individus->setClass( (*it3).second );

				individus->upgradeTo((*it3).second);
			}

			cell->setIndividus(cell_pop);
		}
	}
	pop->setIndividus(full_pop);
	Logger::info("--> %s\n", pop->info1().c_str());

	Logger::debug("Process::survival end\n");
}


int Process::reproduction(Map* amap, Population* pop, int last_individus_id){
	Logger::debug("Process::reproduction start\n");
	if(pop->getSexualReproduction()){
		return sexual_reproduction(amap, pop, last_individus_id);
	}else{
		return asexual_reproduction(amap, pop, last_individus_id);
	}
	Logger::debug("Process::reproduction end\n");
//	return id;
	return 0;
}

int Process::sexual_reproduction(Map* amap, Population* pop, int last_individus_id){
	Logger::debug("Process::sexual_reproduction start\n");

	vector<shared_ptr<Individus>> pop_male, pop_female;
	vector< vector<population_class> > pop_classes_male, pop_classes_female;
	vector<shared_ptr<Individus>> children;
	vector<shared_ptr<Individus>> individus;
	shared_ptr<Individus> male, female;
	double f_male_dd, f_female_dd, f_dd;
	double f_contrib_female;
	SEX child_sex;
	int nb_couples, nb_children;
	int harem_size;
	int id = last_individus_id;
	double k, dd;
	int N;

	shared_ptr<Cell> cell;
	vector<shared_ptr<Cell>> cells = amap->getCells();

	bool empty_male = true;
	bool empty_female = true;
	for(int i = 0; i < pop->getNbClasses(); ++i)
	{
		vector<population_class> classes_male;
		vector<population_class> classes_female;
		vector<double> f_male = pop->getFeconditiesForTarget(i, SEX(MALE));
		vector<double> f_female = pop->getFeconditiesForTarget(i, SEX(FEMALE));
		for(int j = 0; j < pop->getNbClasses(); ++j){
			if( f_male[j]  > 0){
				classes_male.push_back({j, SEX(MALE)});
				empty_male = false;
			}
			if( f_female[j]  > 0){
				classes_female.push_back({j, SEX(FEMALE)});
				empty_female = false;
			}
		}
		pop_classes_female.push_back(classes_female);
		pop_classes_male.push_back(classes_male);
	}

	f_contrib_female = pop->getFecontidyContrib();


	if(empty_male || empty_female ){
		Logger::debug("WARNING : no reproduction is available\n");
	}else{
		for(vector<shared_ptr<Cell>>::iterator it = cells.begin();
			it != cells.end(); it++)
		{
			cell = (*it);
			N = cell->getNbIndividus();

			if( N > 0 ) {

				Logger::info("\n--> %s\n", cell->info().c_str());

				children.clear();

				if(pop->getFecondityDD()){
					k = cell->getProperty(CARRYING_CAPACITY);
					dd = exp(-k * N);
				}else{
					dd = 1;
				}
				individus = cell->getIndividus2();

				for(int class_target = 0; class_target < pop->getNbClasses(); ++class_target)
				{

					vector<population_class> classes_male = pop_classes_male[class_target];
					vector<population_class> classes_female = pop_classes_female[class_target];

					pop_male = Individus::filterByClasses(individus, pop->getId(), classes_male, INDIVIDUS_STATE(-1));
					pop_female = Individus::filterByClasses(individus, pop->getId(), classes_female, INDIVIDUS_STATE(-1));
					nb_couples = min(pop_male.size(), pop_female.size());

					if(nb_couples == 0){
						Logger::debug("WARNING : no couple available to generate individu(s) of class %d !\n", class_target);
					}else{
						Logger::debug("%d couple(s) available(s) to generate individu(s) of class %d !\n", nb_couples, class_target);
						for(int i = 0; i < nb_couples; i++){
							//pop_male.at(i)->addHaremIndividus(pop_female.at(i));
							pop_female.at(i)->addHaremIndividus(pop_male.at(i));
						}

						for(vector<shared_ptr<Individus>>::iterator it2 = pop_female.begin(); it2 != pop_female.end(); it2++){
							female = (*it2);
							harem_size = female->getHarem().size();
							if( harem_size > 0 ) {
								male = female->getHarem().at( myRandom.IRandom(0, (harem_size - 1)) );
								female->removeHaremIndividus(male);
								f_male_dd = pop->getFecondity(male->getClass(), class_target, MALE) * dd;
								f_female_dd = pop->getFecondity(female->getClass(), class_target, FEMALE) * dd;
								f_dd = f_contrib_female * f_female_dd + (1 - f_contrib_female) * f_male_dd;
								nb_children = myRandom.Poisson(f_dd);

								for(int i = 0; i < nb_children; i++){
									if( ( pop->getInitialSexRatio() == 1 ) || ( myRandom.Bernoulli(pop->getInitialSexRatio()) ) ){
										child_sex = FEMALE;
									}else{
										child_sex = MALE;
									}
									id++;
									shared_ptr<Individus> child = female->reproduce(male, id, child_sex, class_target);
									female->addChild(child);
									male->addChild(child);
									children.push_back(child);
									pop->nb_born[child_sex][class_target]++;
								}
							}
						}
					}
				}

				for(vector<shared_ptr<Individus>>::iterator it3 = children.begin(); it3 != children.end(); it3++){
					pop->addIndividus((*it3));
					cell->addIndividus((*it3));
					Logger::debug("new child, class : %d, sex : %d\n", (*it3)->getClass(), (*it3)->getSex());
				}
			}
		}
	}
	Logger::info("--> %s\n", pop->info1().c_str());
	Logger::debug("Process::sexual_reproduction end\n");
	return id;
}

int Process::asexual_reproduction(Map* amap, Population* pop, int last_individus_id){
	Logger::debug("Process::asexual_reproduction start\n");
	vector<shared_ptr<Individus>> children;
	vector<shared_ptr<Individus>> individus;
	vector<double> fecondities;
	shared_ptr<Cell> cell;
	vector<shared_ptr<Cell>> cells = amap->getCells();
	int id = last_individus_id;
	double k, dd, f_dd;
	int N, nb_children;
	SEX child_sex;

	for(vector<shared_ptr<Cell>>::iterator it = cells.begin();
				it != cells.end(); it++)
	{
		cell = (*it);
		N = cell->getNbIndividus();

		if( N > 0 ) {

			Logger::info("\n--> %s\n", cell->info().c_str());

			children.clear();

			k = 0;
			dd = 1;
			Matrix p_fecond;
			if(cell->getFecondityEffect().size() == 0){
				if(pop->getFecondityDD()){
					k = cell->getProperty(CARRYING_CAPACITY);
					dd = exp(-k * N);
				}
			}else{
				p_fecond = cell->getFecondityEffect();
			}
			individus = cell->getIndividus2();

			for(vector<shared_ptr<Individus>>::iterator it2 = individus.begin(); it2 != individus.end(); it2++){
				const shared_ptr<Individus> individu = (*it2);
//				child_sex = individu->getSex();
				int pop_class = individu->getClass();
				if( individu->getNbChildren() < pop->getFecondityMax(pop_class) ){
					if(cell->getFecondityEffect().size() == 0){
						fecondities = pop->getFeconditiesForSource(pop_class, child_sex);
					}else{
						fecondities.clear();
						for(int i=0; i<pop->getNbClasses(); ++i){
							fecondities.push_back(p_fecond.at(pop_class, i));
						}
					}
					for(unsigned int class_target = 0; class_target < fecondities.size(); ++class_target){
						double f = fecondities[class_target];
						f_dd = f * dd;
						Logger::debug("fecondity %d -> %d : %f\n", pop_class, class_target, f_dd);
						if(f_dd > 0){
							nb_children = myRandom.Poisson(f_dd);

							for(int n = 0; n < nb_children; n++){
								if( ( pop->getInitialSexRatio() == 1 ) || ( myRandom.Bernoulli(pop->getInitialSexRatio()) ) ){
									child_sex = FEMALE;
								}else{
									child_sex = MALE;
								}
								id++;
								shared_ptr<Individus> child = individu->reproduce(individu, id, child_sex, class_target);
								individu->addChild(child);
								children.push_back(child);
								pop->nb_born[child_sex][class_target]++;
							}
						}
					}
				}
			}

			for(vector<shared_ptr<Individus>>::iterator it3 = children.begin(); it3 != children.end(); it3++){
				pop->addIndividus((*it3));
				cell->addIndividus((*it3));
				Logger::debug("new child, class : %d, sex : %d\n", (*it3)->getClass(), (*it3)->getSex());
			}
		}
	}
	Logger::info("--> %s\n", pop->info1().c_str());
	Logger::debug("Process::asexual_reproduction end\n");
	return id;
}

void Process::setStatus(int dispersion_on, int survival_on, int reproduction_on){
	this->dispersion_on = dispersion_on;
	this->survival_on = survival_on;
	this->reproduction_on = reproduction_on;
}


void Process::setDirResults(string dir_results){
	this->dir_results = dir_results;
}

std::string Process::getPopFileResults(Population* pop){
	std::string file_results = getDirResults() + "dyn_pop" + std::to_string(pop->id) + ".csv";
	return file_results;
}

std::string Process::getPatchPopFileResults(Population* pop){
	std::string file_results = getDirResults() + "dyn_patch_pop" + std::to_string(pop->id) + ".csv";
	return file_results;
}

std::string Process::getDirResults(){
	return this->dir_results;
}

void Process::clearResults(Population* pop){
	pop->clearResults();
}

void Process::initResults(Population* pop){
	pop->initResults();

	ofstream afile;
	std::string file_results = getPopFileResults(pop);
	afile.open(file_results.c_str());
	afile << pop->getFormatedResultsHeader();
	afile.close();
}


void Process::initPatchResults(Population* pop){

	ofstream afile;
	std::string file_results = getPatchPopFileResults(pop);
	std::stringstream results;
	results << "time" << "\t";
	results << "patch" << "\t";
	switch(pop->pop_sex) {
	case 0:
			results << "female" << "\t";
			for(int i = 0; i < pop->getNbClasses(); i++){
				results << "female " << i << "\t";
			}
			break;
	case 1:
			results << "male" << "\t";
			for(int i = 0; i < pop->getNbClasses(); i++){
				results << "male " << i << "\t";
			}
			break;
	case 2:
		results << "female" << "\t";
		results << "male" << "\t";
		results << "total" << "\t";
		for(int i = 0; i < pop->getNbClasses(); i++){
			results << "female " << i << "\t";
			results << "male " << i << "\t";
			results << "total " << i << "\t";
		}
		break;
	}
	results << "\n";

	afile.open(file_results.c_str());
	afile << results.str();
	afile.close();
}

void Process::writeDynPop(const int t, Population* pop){
	ofstream afile;
	std::string file_results = getPopFileResults(pop);
	afile.open(file_results.c_str(), fstream::app);
	afile << pop->getFormatedResults(t);
	afile.close();
}

void Process::writeKPatches(Map* amap, Population* pop){
	std::string map_results = getDirResults() + "kmax_" + std::to_string(10000 + this->time) + ".tif";
	Logger::debug("writeKpatches in %s\n", map_results.c_str());
	std::vector<double> values;
	values = amap->getValues(KMAX);
	amap->writeRaster(map_results, values);
}

void Process::writePatchPop(const int t, Map* amap, Population* pop){
	ofstream afile;
	std::string file_results = getPatchPopFileResults(pop);
	std::stringstream results;
	std::vector<std::shared_ptr<Patch>> patches = amap->getPatches();
	Logger::debug("pacthes size: %d\n", patches.size());
	for(std::vector<std::shared_ptr<Patch>>::iterator it = patches.begin();
			it != patches.end(); ++it){
		Patch* apatch = it->get();
		std::vector<std::shared_ptr<Individus>> individus = apatch->getIndividus();
		results << t << "\t";
		results << apatch->getId() << "\t";
		switch(pop->pop_sex) {
		case 0:
			results << individus.size() << "\t";
			for(int cl = 0; cl < pop->getNbClasses(); cl++){
				results << Individus::filter(individus, pop->getId(), cl, FEMALE, INDIVIDUS_STATE(-1)).size() << "\t";
			}
			break;
		case 1:
			results << individus.size() << "\t";
			for(int cl = 0; cl < pop->getNbClasses(); cl++){
				results << Individus::filter(individus, pop->getId(), cl, MALE, INDIVIDUS_STATE(-1)).size() << "\t";
			}
			break;
		case 2:
			results << Individus::filter(individus, pop->getId(), -1, FEMALE, INDIVIDUS_STATE(-1)).size() << "\t";
			results << Individus::filter(individus, pop->getId(), -1, MALE, INDIVIDUS_STATE(-1)).size() << "\t";
			results << Individus::filter(individus, pop->getId(), -1, SEX(-1), INDIVIDUS_STATE(-1)).size() << "\t";
			for(int cl = 0; cl < pop->getNbClasses(); cl++){
				results << Individus::filter(individus, pop->getId(), cl, FEMALE, INDIVIDUS_STATE(-1)).size() << "\t";
				results << Individus::filter(individus, pop->getId(), cl, MALE, INDIVIDUS_STATE(-1)).size() << "\t";
				results << Individus::filter(individus, pop->getId(), cl, SEX(-1), INDIVIDUS_STATE(-1)).size() << "\t";
			}
			break;
		}
		results << "\n";
	}
	afile.open(file_results.c_str(), fstream::app);
	afile << results.str();
	afile.close();
}

void Process::writePop(Map* amap, Population* pop){
	std::string map_results = getDirResults() + "population" + std::to_string(pop->id) + "_" + std::to_string(10000 + this->time) + ".shp";

	Logger::debug("writeShapeResults in %s\n", map_results.c_str());
	amap->writeShapeIndividus(map_results, pop, this->time);
}

void Process::writeCosts(Map* amap, Population* pop){
	std::string map_results = getDirResults() + "cost" + "_" + std::to_string(10000 + this->time) + ".tif";

	Logger::debug("writeCosts in %s\n", map_results.c_str());
	std::vector<double> values;
	values = amap->getValues(COST);
	amap->writeRaster(map_results, values);
}

void Process::writeKs(Map* amap, Population* pop){
	std::string map_results = getDirResults() + "k_" + std::to_string(10000 + this->time) + ".tif";

	Logger::debug("writeKs in %s\n", map_results.c_str());
	std::vector<double> values;
	values = amap->getValues(CARRYING_CAPACITY);
	amap->writeRaster(map_results, values);
}

void Process::writeDensity(Map* amap, Population* pop){
	std::string map_results = getDirResults() + "density_" + std::to_string(10000 + this->time) + ".tif";

	Logger::debug("writeKs in %s\n", map_results.c_str());
	std::vector<double> values;
	values = amap->getValues(ABUNDANCE);
	amap->writeRaster(map_results, values);
}

void Process::writeVisits(Map* amap, Population* pop){
	std::string map_results = getDirResults() + "visits_pop" + std::to_string(pop->id) + "_" + std::to_string(10000 + this->time) + ".tif";

	Logger::debug("writeVisits in %s\n", map_results.c_str());
	std::vector<double> values = amap->getValues(VISITS);
	amap->writeRaster(map_results, values);
}

void Process::writeVisitsTot(Map* amap, Population* pop){
	std::string map_results = getDirResults() + "visits_pop" + std::to_string(pop->id) + ".tif";

	Logger::debug("writeVisitsTot in %s\n", map_results.c_str());
	std::vector<double> values = amap->getValues(VISITS_TOT);
	amap->writeRaster(map_results, values);
}

void Process::writePaths(Map* amap, Population* pop){
	std::string map_results = getDirResults() + "population" + std::to_string(pop->id) + "_Paths_" + std::to_string(10000 + this->time) + ".shp";

	Logger::debug("writeShapeResults in %s\n", map_results.c_str());
	amap->writeShapePaths(map_results, pop, this->time);
}

} // namespace dynaland
