/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef DYNALAND_SRC_DYNALAND_PATCH_HPP_
#define DYNALAND_SRC_DYNALAND_PATCH_HPP_

#include <dynaland/Cell.hpp>
#include <dynaland/global.hpp>

#include <vector>

using namespace dynaland;

namespace dynaland {

class Patch {
public:
	Patch();
	Patch(Map* landscape, const int id, const std::vector<std::shared_ptr<Cell>> cells);
	virtual ~Patch();

	std::vector<std::shared_ptr<Cell>> getCells();
	void setProperty(const CELL_PROPERTY& property, const double& value);
	double getMeanProperty(const CELL_PROPERTY& property);
	int getId();
	bool isEmpty();
	std::vector<std::shared_ptr<Individus>> getIndividus();
	double getKmax();
	void setKmax(double kmax);

//	std::vector<std::shared_ptr<Individus>> filter(const int pop_id,
//			const std::vector<population_class>& pop_classes,
//			const INDIVIDUS_STATE& state);
//
//	int getNbIndividus();


protected:
	int id;
	double Kmax;
	std::vector<std::shared_ptr<Cell>> cells;
	Map* landscape;
};

} // namespace dynaland

#endif /* DYNALAND_SRC_DYNALAND_PATCH_HPP_ */
