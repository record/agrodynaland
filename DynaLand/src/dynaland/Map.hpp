/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef MAP_H_
#define MAP_H_

#include <dynaland/Cell.hpp>
#include <dynaland/global.hpp>
#include <dynaland/Patch.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <memory>
#include <map>
#include <ogr_feature.h>

using namespace dynaland;

namespace dynaland {

class Map {

protected:
	void loadRaster(const std::string& raster_file);
	void loadNomenclature(const std::string& filename);
	void checkGDALdriver(const std::string& pszFormat);
	gpoint point2grid(double* adfGeoTransform, double x, double y);
	ppoint grid2point(double* adfGeoTransform, int x, int y);
	int getFieldIndex(const char* shape_file, OGRFeatureDefn *poFDefn, const char* field_name);
	int getFieldValue(const char* shape_file, OGRFeature *poFeature, const char* field_name);

public:
	Map();

	virtual ~Map();

	void loadMap(const std::string& raster_file, const std::string& nomenclature_file);
	int loadIndividus(const std::string& shape_file, Population* population);
	void initPatches();
	std::map<std::string, double> getNomenclature(const unsigned int id);

	bool isCellBorder(gpoint& xy);

	std::shared_ptr<Cell> getCell(const gpoint& xy);
	std::shared_ptr<Cell> getCell(const int id);

	std::vector<std::shared_ptr<Cell>> getCells(const gpoint& xy_min, const gpoint& xy_max);
	std::vector<std::shared_ptr<Cell>> getCells();
	std::vector<std::shared_ptr<Cell>> getCellsByOcc(const int id_occ);
	std::shared_ptr<Patch> getPatch(const int id);
	std::vector<std::shared_ptr<Patch>> getPatches();
	void clearCosts();

	std::shared_ptr<Cell> getCellNeighbor(
			const gpoint& xy,
			const DIRECTION& direction);

	std::vector<std::shared_ptr<Cell>> getCellNeighbors(
			const gpoint& xy,
			const DIRECTION& direction,
			const int& perceptual_range);

	std::vector<std::shared_ptr<Cell>> getCellNeighbors(const gpoint& xy);

	std::vector<double> computeNeighborsCosts(
			const gpoint& xy,
			const int& perceptual_range,
			const CELL_PROPERTY& property);

	std::vector<double> computeSMSprobabilities(
			const std::shared_ptr<Cell>& cell,
			const std::shared_ptr<Cell>& previous,
			const std::shared_ptr<Cell>& goal,
			const double& mem_dir_persistence,
			const double& goal_dir_persistence,
			const std::vector<std::shared_ptr<Cell>>& memorizedCells,
			const int& perceptual_range);

	std::vector<double> computeDirectionalBias(
			const std::shared_ptr<Cell>& cell_source,
			const std::shared_ptr<Cell>& cell_target,
			const double& directional_persitence,
			const CELL_PROPERTY& bias_type);

	double cellsMean(const std::vector<std::shared_ptr<Cell>>& cells,
			const CELL_PROPERTY& property);

	std::string print(const CELL_PROPERTY& property);
	std::string printAbundance();

	std::vector<double> getValues(const CELL_PROPERTY& property);
	std::vector<double> getAbundances();
	void writeRaster(const std::string& map_filename, std::vector<double> values);
	void createShapeFields(OGRLayer *poLayer);
	void writeShapeIndividus(const std::string& map_filename, Population* population, int time);
	void writeShapePaths(const std::string& map_filename, Population* population, int time);

protected:
	std::vector<std::shared_ptr<Cell>> cells;
	std::vector<std::shared_ptr<Patch>> patches;

public:
	int ncol, nrow;
	std::string raster_file;
	std::map< int, std::map<std::string, double> > nomenclature;
	double adfGeoTransform[6];
	const char *projectionRef;
	OGRSpatialReference spatialReference;
    //	adfGeoTransform[0] /* top left x */
    //	adfGeoTransform[1] /* w-e pixel resolution */
    //	adfGeoTransform[2] /* 0 */
    //	adfGeoTransform[3] /* top left y */
    //	adfGeoTransform[4] /* 0 */
    //	adfGeoTransform[5] /* n-s pixel resolution (negative value) */
};

} // namespace dynaland

#endif /* MAP_H_ */
