/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef DYNALAND_SRC_DYNALAND_MATRIX_HPP_
#define DYNALAND_SRC_DYNALAND_MATRIX_HPP_

#include <string>
#include <vector>

namespace dynaland {

using namespace dynaland;

class Matrix {

public:

	unsigned int row;
	unsigned int col;
	std::vector<double> values;
	Matrix();
	Matrix(const unsigned int row, const unsigned int col);

	double& at(const unsigned int x, const unsigned int y);
	double my_stod(const std::string &valueAsString);
	std::string print();
	int size();
	void clear(double);
};

class MatrixCsv:public Matrix {

public:
	std::string filename;
	unsigned int nb_classe;
	MatrixCsv(const std::string, const unsigned int a);
	void csv2matrix();

};

} // namespace dynaland

#endif /* DYNALAND_SRC_DYNALAND_MATRIX_HPP_ */
