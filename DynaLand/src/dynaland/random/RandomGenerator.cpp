/*
 * random_tools.cpp
 *
 *  Created on: 9 nov. 2016
 *      Author: grobaldo
 */


#include <dynaland/random/RandomGenerator.hpp>
#include <dynaland/random/mersenne/stocc.h>

#include <random>
#include <stdlib.h>
#include <vector>

namespace dynaland { namespace random {

RandomGenerator::RandomGenerator(){
	seed = 0;
	random_type = 0;
}

RandomGenerator::RandomGenerator(int seed, int random_type){
	this->seed = seed;
	this->random_type = random_type;

	if(random_type){
		this->generator = std::mt19937(seed);
		srand(seed);
	}else{
		this->aleatoire = StochasticLib1(seed);
	}
}

RandomGenerator::~RandomGenerator(){
}

int RandomGenerator::Bernoulli(double p){
	if( random_type ){
		std::bernoulli_distribution distrib(p);
		return( distrib(generator) );
	} else {
		return( aleatoire.Bernoulli(p) );
	}
}

int RandomGenerator::IRandom(int min, int max){
	if( random_type ){
		std::uniform_int_distribution<> distrib = std::uniform_int_distribution<>{min, max};
		return( distrib(generator) );
	}else{
		return ( aleatoire.IRandom(min, max) );
	}
}

int RandomGenerator::IRandom(double p){
	return( (IRandom(0, 1000) / 1000.0) < p );
}

int RandomGenerator::Poisson(double p){
	if( random_type ){
		std::poisson_distribution<int> distrib(p);
		return( distrib(generator) );
	} else {
		return( aleatoire.Poisson(p) );
	}
}

double RandomGenerator::Random(){
	if( random_type ){
		std::uniform_real_distribution<double> distrib(0., 1.);
		return( distrib(generator) );
	}else{
		return( aleatoire.Random());
	}
}

int RandomGenerator::Random(std::vector<double> probas){
	double p = (IRandom(0, 1000) / 1000.0);
	double sum_p = 0;
	for(unsigned int i = 0; i < probas.size(); i++){
		sum_p += probas.at(i);
		if(p < sum_p){
			return i;
		}
	}
	return (probas.size() - 1);
}

int RandomGenerator::getRandomType(){
	return random_type;
}

StochasticLib1* RandomGenerator::getGenerator0(){
	return &aleatoire;
}

std::mt19937* RandomGenerator::getGenerator1(){
	return &generator;
}

int RandomGenerator::getSeed(){
	return seed;
}

} // namespace dynaland
} // namespace random

