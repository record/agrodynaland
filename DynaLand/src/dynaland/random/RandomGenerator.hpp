/*
 * random_tools.h
 *
 *  Created on: 9 nov. 2016
 *      Author: grobaldo
 */

#ifndef RANDOMGENERATOR_H_
#define RANDOMGENERATOR_H_

#include <dynaland/random/mersenne/stocc.h>
#include <random>
#include <stdlib.h>
#include <vector>

namespace dynaland { namespace random {

class RandomGenerator {
public:

	RandomGenerator();

	RandomGenerator(int seed, int random_type);

	~RandomGenerator();

	int Bernoulli(double p);

	int IRandom(double p);

	int IRandom(int min, int max);

	int Poisson(double p);

	double Random();

	int Random(std::vector<double> probas);

	int getRandomType();

	StochasticLib1* getGenerator0();

	std::mt19937* getGenerator1();

	int getSeed();

public:
	int seed;
	int random_type; // 0 = metaconnect style; 1 = c++ style
	std::mt19937 generator;// = std::mt19937(seed);
	StochasticLib1 aleatoire;// = StochasticLib1(0);
};

} // namespace dynaland
} // namespace random
#endif /* RANDOMGENERATOR_H_ */
