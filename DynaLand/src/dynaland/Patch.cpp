/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/global.hpp>
#include <dynaland/Patch.hpp>
#include <dynaland/Cell.hpp>
#include <dynaland/Map.hpp>
#include <dynaland/Logger.hpp>

#include <vector>

using namespace dynaland;

namespace dynaland {

Patch::Patch(){
	this->landscape = 0;
	this->id = 0;
	this->Kmax = 0;
}
Patch::Patch(Map* landscape, const int id, const std::vector<std::shared_ptr<Cell>> cells){
	this->landscape = landscape;
	this->id = id;
	this->cells = cells;
	if(!cells.empty()){
		this->Kmax = cells[0].get()->k;
	}else{
		this->Kmax = 0;
	}
}

Patch::~Patch(){
	this->landscape = 0;
	this->cells.clear();
}

int Patch::getId(){
	return this->id;
}

bool Patch::isEmpty(){
	return cells.size() == 0;
}

double Patch::getKmax(){
	return this->Kmax;
}

void Patch::setKmax(double kmax){
	this->Kmax = kmax;
}
std::vector<std::shared_ptr<Cell>> Patch::getCells(){
	return cells;
}

void Patch::setProperty(const CELL_PROPERTY& property, const double& value){
	for(std::vector< std::shared_ptr<Cell> >::const_iterator it = cells.begin();
				it != cells.end(); it++){
		(*it)->setProperty(property, value);
	}
}

double Patch::getMeanProperty(const CELL_PROPERTY& property){
	double val = 0;
	for(std::vector< std::shared_ptr<Cell> >::const_iterator it = cells.begin();
					it != cells.end(); it++){
		val += (*it)->getProperty(property);
	}
	return val / cells.size();
}

std::vector<std::shared_ptr<Individus>> Patch::getIndividus(){
	std::vector<std::shared_ptr<Individus>> individus;
//	Logger::debug("Patch::getIndividus, nb cells: %d\n", this->cells.size());
	for(std::vector< std::shared_ptr<Cell> >::const_iterator it = this->cells.begin();
						it != this->cells.end(); it++){
		std::vector<std::shared_ptr<Individus>> indivs = (*it)->getIndividus2();
		for(std::vector< std::shared_ptr<Individus> >::const_iterator it = indivs.begin();
				it != indivs.end(); it++){
			individus.push_back( (*it) );
		}
	}
	return individus;
}

//std::vector<std::shared_ptr<Individus>> Patch::filter(const int pop_id,
//		const std::vector<population_class>& pop_classes,
//		const INDIVIDUS_STATE& state){
//	std::vector<std::shared_ptr<Individus>> individus;
//	for(std::vector< std::shared_ptr<Cell> >::const_iterator it = cells.begin();
//						it != cells.end(); it++){
//		std::vector<std::shared_ptr<Individus>> indivs = Individus::filter((*it)->getIndividus2(), pop_id, pop_classes, state);
//		for(std::vector< std::shared_ptr<Individus> >::const_iterator it = indivs.begin();
//				it != indivs.end(); it++){
//			individus.push_back( (*it) );
//		}
//	}
//	return individus;
//}
//
//int Patch::getNbIndividus(){
//	int N = 0;
//	for(std::vector< std::shared_ptr<Cell> >::const_iterator it = cells.begin();
//			it != cells.end(); it++){
//		N += (*it)->getNbIndividus();
//	}
//	return N;
//}

} // namespace dynaland

