/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <vector>

namespace dynaland {

#if defined(_WIN16) | defined(_WIN32) | defined(_WIN64)
#define PATH_SEP "\\"
#else
#define PATH_SEP "/"
#endif

static const int SMS_ON = 0;
enum CELL_PROPERTY {CARRYING_CAPACITY = 1, COST = 2, RESSOURCE = 3, MEAN_COST = 4, MEMORY_BIAS = 5, GOAL_BIAS = 6, SMS_PROBA = 7, VISITS = 8, MEMORIZED = 9, ABUNDANCE = 10, VISITS_TOT = 11, KMAX = 12};
enum DISPERSION_TYPE {RW = 1, SMS = 2};
enum INDIVIDUS_STATE {SEDENTARY = 1, DISPERSION = 2};
enum DIRECTION {C = 0, E = 1, NE = 2, N = 3, NW = 4, W = 5, SW = 6, S = 7, SE = 8};
//std::vector<int> directions = {E, NE, N, NW, W, SW, S, SE};
enum SEX {FEMALE = 0, MALE = 1};
//enum DIRECTIONAL_BIAS_TYPE {MEMORY = 1, GOAL = 2}
struct gpoint {int x; int y;};
struct ppoint {double x; double y;};
struct p_range {double p_min; double p_max;};
enum SHAPE_TYPE {POINT = 1, LINE = 2};
} // namespace dynaland
#endif /* GLOBAL_H_ */
