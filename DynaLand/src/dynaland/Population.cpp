/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/global.hpp>
#include <dynaland/Individus.hpp>
#include <dynaland/Logger.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/random/RandomGenerator.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <random>
#include <algorithm>
#include <ctime>
#include <memory>
#include <stdexcept>

using namespace dynaland;

namespace dynaland {

Population::Population(){
	this->id = 0;
	this->nb_classes = 0;
	this->sex_ratio = 0;
//	this->class_ratio = {};
	this->sexual_reproduction = 0;
	this->pop_sex = 2;

	this->dispersion_female = {};
//	this->survival_female = NULL;
//	this->fecondity_female = NULL;
	this->harem_size_female = 0;

	this->dispersion_male = {};
//	this->survival_male = NULL;
//	this->fecondity_male = NULL;
	this->harem_size_male = 0;

	this->dispersion_density_dependent = 0;
	this->survival_density_dependent = 0;
	this->fecondity_density_dependent = 0;

	this->fecondity_contrib_female = 0;
	this->perceptual_range = 0;
	this->memory_size = 0;
	this->persistence_memory = 0;
	this->persistence_target = 0;
	this->path_max_cells = 0;
}

Population::Population(int id,
					   int nb_classes,
					   double sex_ratio,
//					   vector<double> class_ratio,
					   int sexual_reproduction,
					   int pop_sex,

					   vector<p_range> dispersion_female,
					   const Matrix& survival_female,
					   const Matrix& fecondity_female,
					   int harem_size_female,

					   vector<p_range> dispersion_male,
					   const Matrix& survival_male,
					   const Matrix& fecondity_male,
					   int harem_size_male,

					   int dispersion_density_dependent,
					   int survival_density_dependent,
					   int fecondity_density_dependent,

					   double fecondity_contrib_female,
					   int perceptual_range,
					   int memory_size,
					   double persistence_memory,
					   double persistence_target,
					   int path_max_cells){

	this->id = id;
	this->nb_classes = nb_classes;
	this->sex_ratio = sex_ratio;
//	this->class_ratio = class_ratio;
	this->sexual_reproduction = sexual_reproduction;
	this->pop_sex = pop_sex;

	this->dispersion_female = dispersion_female;
	this->survival_female = survival_female;
	this->fecondity_female = fecondity_female;
	this->harem_size_female = harem_size_female;

	this->dispersion_male = dispersion_male;
	this->survival_male = survival_male;
	this->fecondity_male = fecondity_male;
	this->harem_size_male = harem_size_male;

	this->dispersion_density_dependent = dispersion_density_dependent;
	this->survival_density_dependent = survival_density_dependent;
	this->fecondity_density_dependent = fecondity_density_dependent;

	this->fecondity_contrib_female = fecondity_contrib_female;
	this->perceptual_range = perceptual_range;
	this->memory_size = memory_size;
	this->persistence_memory = persistence_memory;
	this->persistence_target = persistence_target;
	this->path_max_cells = path_max_cells;

	this->initResults();
}

Population::~Population(){}

int Population::getId() const{
	return this->id;
}

int Population::getNbClasses() const {
	return this->nb_classes;
}


void Population::addIndividus(const shared_ptr<Individus>& individus){
	this->list_individus.push_back(individus);
}

void Population::killIndividus(const shared_ptr<Individus>& individus){
	list_individus.erase(std::remove(list_individus.begin(), list_individus.end(), individus), list_individus.end());
}

void Population::setIndividus(const vector<shared_ptr<Individus>>& individus){
	this->list_individus = individus;
}

int Population::size() const {
	return this->list_individus.size();
}

vector<shared_ptr<Individus>> Population::getIndividus(){
	return list_individus;
}

vector<shared_ptr<Individus>> Population::filter(int pop_class, SEX sex, INDIVIDUS_STATE state){
	return Individus::filter(this->getIndividus(), this->getId(), pop_class, sex, state);
}

p_range Population::getDispersion(int pop_class, SEX sex){
	switch(sex){
		case FEMALE:
			return dispersion_female.at(pop_class);
		case MALE:
			return dispersion_male.at(pop_class);
		default:
			throw std::out_of_range("Unknow population sex : sex = " + sex);
	}
}

double Population::getAutoSurvival(int pop_class, SEX sex){
	switch(sex){
		case FEMALE:
			return survival_female.at(pop_class, pop_class);
		case MALE:
			return survival_male.at(pop_class, pop_class);
		default:
			throw std::out_of_range("Unknow population sex : sex = " + sex);
	}
}

double Population::getSurvival(int pop_class_from, int pop_class_to, SEX sex){
	switch(sex){
		case FEMALE:
			return survival_female.at(pop_class_from, pop_class_to);
		case MALE:
			return survival_male.at(pop_class_from, pop_class_to);
		default:
			throw std::out_of_range("Unknow population sex : sex = " + sex);
	}
}

double Population::getFecondity(int pop_class_from, int pop_class_to, SEX sex){
	//cout << "getFecondity: class " << pop_class << ", sex " << sex << endl;
	switch(sex){
		case FEMALE:
			return fecondity_female.at(pop_class_from, pop_class_to);
		case MALE:
			return fecondity_male.at(pop_class_from, pop_class_to);
		default:
			throw std::out_of_range("Unknow population sex : sex = " + sex);
	}
}

vector<double> Population::getFeconditiesForTarget(int pop_class_to, SEX sex){
	//cout << "getFecondity: class " << pop_class << ", sex " << sex << endl;
	vector<double> values;
	switch(sex){
		case FEMALE:
			for(int i=0; i<nb_classes; ++i){
				values.push_back(fecondity_female.at(i, pop_class_to));
			}
			return values;
		case MALE:
			for(int i=0; i<nb_classes; ++i){
				values.push_back(fecondity_male.at(i, pop_class_to));
			}
			return values;
		default:
			throw std::out_of_range("Unknow population sex : sex = " + sex);
	}
}

vector<double> Population::getFeconditiesForSource(int pop_class_from, SEX sex){
	//cout << "getFecondity: class " << pop_class << ", sex " << sex << endl;
	vector<double> values;
	switch(sex){
		case FEMALE:
			for(int i=0; i<nb_classes; ++i){
				values.push_back(fecondity_female.at(pop_class_from, i));
			}
			return values;
		case MALE:
			for(int i=0; i<nb_classes; ++i){
				values.push_back(fecondity_male.at(pop_class_from, i));
			}
			return values;
		default:
			throw std::out_of_range("Unknow population sex : sex = " + sex);
	}
}

double Population::getFecontidyContrib(){
	return this->fecondity_contrib_female;
}

std::vector<int> Population::getFecondityMax(){
	return this->fecondity_max;
}

int Population::getFecondityMax(int pop_class){
	if(pop_class < this->fecondity_max.size()){
		return this->fecondity_max.at(pop_class);
	}else{
		//TODO: change aphid model behavior to manage asexual reproduction for male and female
		return 0;
	}
}

void Population::setFecondityMax(const std::vector<int> Fmax){
	this->fecondity_max = Fmax;
}

int Population::getDispersionPathMaxCells() const {
	return this->path_max_cells;
}

int Population::getDispersionDmax() {
	double dmax = 0;
	for(vector<shared_ptr<Individus>>::const_iterator it = list_individus.begin();
				it != list_individus.end(); it++){
		if((*it)->getDispersionDmax() > dmax){
			dmax = (*it)->getDispersionDmax();
		}
	}
	return dmax;
}


double Population::getInitialSexRatio(){
	return this->sex_ratio;
}

int Population::getSexualReproduction(){
	return this->sexual_reproduction;
}

int Population::getInitialHaremFemaleSize(){
	return this->harem_size_male;
}

int Population::getInitialHaremMaleSize(){
	return this->harem_size_male;
}

int Population::getPerceptualRange() const{
	return this->perceptual_range;
}

double Population::getDirectionalMemoryPersistence() const{
	return this->persistence_memory;
}

double Population::getDirectionalGoalPersistence() const{
	return this->persistence_target;
}

int Population::getMemorySize() const{
	return this->memory_size;
}

int Population::getSurvivalDD(){
	return this->survival_density_dependent;
}

int Population::getFecondityDD(){
	return this->fecondity_density_dependent;
}

int Population::getDispersionDD(){
	return this->dispersion_density_dependent;
}

void Population::clearResults(){
	for(int i = 0; i < this->getNbClasses(); i++){
		this->nb_born[FEMALE].at(i) = 0;
		this->nb_born[MALE].at(i) = 0;
		this->nb_survival[FEMALE].at(i) = 0;
		this->nb_survival[MALE].at(i) = 0;
		this->nb_auto_survival[FEMALE].at(i) = 0;
		this->nb_auto_survival[MALE].at(i) = 0;
		this->nb_dead[FEMALE].at(i) = 0;
		this->nb_dead[MALE].at(i) = 0;
	}

}

void Population::initResults(){
	this->nb_survival[FEMALE].clear();
	this->nb_survival[MALE].clear();
	this->nb_auto_survival[FEMALE].clear();
	this->nb_auto_survival[MALE].clear();
	this->nb_dead[FEMALE].clear();
	this->nb_dead[MALE].clear();
	this->nb_born[FEMALE].clear();
	this->nb_born[MALE].clear();

	for(int i = 0; i < this->getNbClasses(); i++){
		this->nb_survival[FEMALE].push_back(0);
		this->nb_survival[MALE].push_back(0);
		this->nb_auto_survival[FEMALE].push_back(0);
		this->nb_auto_survival[MALE].push_back(0);
		this->nb_dead[FEMALE].push_back(0);
		this->nb_dead[MALE].push_back(0);
		this->nb_born[FEMALE].push_back(0);
		this->nb_born[MALE].push_back(0);

	}
}

std::string Population::getFormatedResultsHeader(){
	std::stringstream results;
	results << "time" << "\t";
	switch(pop_sex) {
	case 0:
			results << "female" << "\t";
			for(int i = 0; i < this->getNbClasses(); i++){
				results << "female " << i << "\t";
				results << "born female " << i << "\t";
				results << "recruted female " << i << "\t";
				results << "keeped female " << i << "\t";
				results << "dead female " << i << "\t";
			}
			break;
	case 1:
			results << "male" << "\t";
			for(int i = 0; i < this->getNbClasses(); i++){
				results << "male " << i << "\t";
				results << "born male " << i << "\t";
				results << "recruted male " << i << "\t";
				results << "keeped male " << i << "\t";
				results << "dead male " << i << "\t";
			}
			break;
	case 2:
		results << "female" << "\t";
		results << "male" << "\t";
		results << "total" << "\t";
		for(int i = 0; i < this->getNbClasses(); i++){
			results << "female " << i << "\t";
			results << "male " << i << "\t";
			results << "total " << i << "\t";
			results << "born female " << i << "\t";
			results << "born male " << i << "\t";
			results << "born total " << i << "\t";
			results << "recruted female " << i << "\t";
			results << "recruted male " << i << "\t";
			results << "recruted total " << i << "\t";
			results << "keeped female " << i << "\t";
			results << "keeped male " << i << "\t";
			results << "keeped total " << i << "\t";
			results << "dead female " << i << "\t";
			results << "dead male " << i << "\t";
			results << "dead total " << i << "\t";
		}
		break;

	}
	results << "\n";
	return results.str();
}

std::string Population::getFormatedResults(const int t) {
	std::stringstream results;
	results << t << "\t";
	switch(pop_sex) {
	case 0:
		results << this->filter(-1, SEX(FEMALE), INDIVIDUS_STATE(-1)).size() << "\t";
		for(int i = 0; i < this->getNbClasses(); i++){
			results << this->filter(i, SEX(FEMALE), INDIVIDUS_STATE(-1)).size() << "\t";
			results << this->nb_born[FEMALE].at(i) << "\t";
			results << this->nb_survival[FEMALE].at(i) << "\t";
			results << this->nb_auto_survival[FEMALE].at(i) << "\t";
			results << this->nb_dead[FEMALE].at(i) << "\t";
		}
		break;
	case 1:
		results << this->filter(-1, SEX(MALE), INDIVIDUS_STATE(-1)).size() << "\t";
		for(int i = 0; i < this->getNbClasses(); i++){
			results << this->filter(i, SEX(MALE), INDIVIDUS_STATE(-1)).size() << "\t";
			results << this->nb_born[MALE].at(i) << "\t";
			results << this->nb_survival[MALE].at(i) << "\t";
			results << this->nb_auto_survival[MALE].at(i) << "\t";
			results << this->nb_dead[MALE].at(i) << "\t";
		}
		break;
	case 2:
		results << this->filter(-1, SEX(FEMALE), INDIVIDUS_STATE(-1)).size() << "\t";
		results << this->filter(-1, SEX(MALE), INDIVIDUS_STATE(-1)).size() << "\t";
		results << this->filter(-1, SEX(-1), INDIVIDUS_STATE(-1)).size() << "\t";
		for(int i = 0; i < this->getNbClasses(); i++){
			results << this->filter(i, SEX(FEMALE), INDIVIDUS_STATE(-1)).size() << "\t";
			results << this->filter(i, SEX(MALE), INDIVIDUS_STATE(-1)).size() << "\t";
			results << this->filter(i, SEX(-1), INDIVIDUS_STATE(-1)).size() << "\t";
			results << this->nb_born[FEMALE].at(i) << "\t";
			results << this->nb_born[MALE].at(i) << "\t";
			results << (this->nb_born[FEMALE].at(i) + this->nb_born[MALE].at(i)) << "\t";
			results << this->nb_survival[FEMALE].at(i) << "\t";
			results << this->nb_survival[MALE].at(i) << "\t";
			results << (this->nb_survival[FEMALE].at(i) + this->nb_survival[MALE].at(i)) << "\t";
			results << this->nb_auto_survival[FEMALE].at(i) << "\t";
			results << this->nb_auto_survival[MALE].at(i) << "\t";
			results << (this->nb_auto_survival[FEMALE].at(i) + this->nb_auto_survival[MALE].at(i)) << "\t";
			results << this->nb_dead[FEMALE].at(i) << "\t";
			results << this->nb_dead[MALE].at(i) << "\t";
			results << (this->nb_dead[FEMALE].at(i) + this->nb_dead[MALE].at(i)) << "\t";
		}
		break;
	}
	results << "\n";
	return(results.str());
}

void Population::printHeader(){
	Logger::info("POPULATION %d : %d individu(s)\n", this->id , this->size());
}

void Population::print(){
	printHeader();
	print(this->list_individus);
}


void Population::print(const vector<shared_ptr<Individus>>& l_individus){
	for(vector<shared_ptr<Individus>>::const_iterator it = l_individus.begin(); it != l_individus.end(); it++){
		Individus* individus  = it->get();
		individus->print();
	}
}

void Population::print2(){
	printHeader();
	Logger::info("SEDENTARY : %d\n", this->filter(-1, SEX(-1), SEDENTARY).size());
	Logger::info("DISPERSION : %d\n", this->filter(-1, SEX(-1), DISPERSION).size());

	for(int i = 0; i < getNbClasses(); i++){

		Logger::info("class %d : %d\n", i, this->filter(i, SEX(-1), INDIVIDUS_STATE(-1)).size());

		Logger::info("    male : %d\n", this->filter(i, MALE, INDIVIDUS_STATE(-1)).size());
		Logger::info("    female: %d\n", this->filter(i, FEMALE, INDIVIDUS_STATE(-1)).size());
	}
}

std::string Population::info1(){
	std::stringstream st;
	int s = 0;

	st << "N:" << this->size() << ", ";

//	s += nb_born[MALE];
//	s += nb_born[FEMALE];
	s = 0;
	for (auto& n : nb_born[MALE])
		s += n;
	for (auto& n : nb_born[FEMALE])
			s += n;
	st << "born:" << s << ", ";

	s = 0;
	for (auto& n : nb_dead[MALE])
		s += n;
	for (auto& n : nb_dead[FEMALE])
			s += n;
	st << "dead:" << s << ", ";

	s = 0;
	for (auto& n : nb_survival[MALE])
		s += n;
	for (auto& n : nb_survival[FEMALE])
			s += n;
	st << "upgraded:" << s << ", ";

	s = 0;
	for (auto& n : nb_auto_survival[MALE])
		s += n;
	for (auto& n : nb_auto_survival[FEMALE])
			s += n;
	st << "keeped:" << s;

	return st.str();
}

} // namespace dynaland
