/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/Matrix.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

namespace dynaland {

using namespace dynaland;

Matrix::Matrix(){
	values.clear();
	col = 0;
	row = 0;
}

Matrix::Matrix(const unsigned int row, const unsigned int col){
	this->row = row;
	this->col = col;
	for(unsigned int i=0; i<col; ++i){
		for(unsigned int j=0; j<col; ++j){
			values.push_back(0);
		}
	}
}


double& Matrix::at(const unsigned int x, const unsigned int y){
	return values[x + y*col];
}

void Matrix::clear(double val){
	for(std::vector<double>::iterator it = values.begin();
			it != values.end(); it++){
		(*it) = val;
	}
}

std::string Matrix::print(){
	std::stringstream txt;
	for(unsigned int i=0; i<row; i++){
		for(unsigned int j=0; j<col; j++){
			txt << this->at(i, j) << "\t";
		}
		txt << "\n";
	}
	return txt.str();
}

double Matrix::my_stod(const std::string &valueAsString) {
	std::istringstream totalSString( valueAsString );
    double valueAsDouble;
    // maybe use some manipulators
    totalSString >> valueAsDouble;
    if(!totalSString)
        throw std::ifstream::failure("Error converting to double");
    return valueAsDouble;
}

MatrixCsv::MatrixCsv(const std::string filename, const unsigned int nb_classe):Matrix(nb_classe, nb_classe){
	this->nb_classe = nb_classe;
	this->filename = filename;

	csv2matrix();
}

void MatrixCsv::csv2matrix(){
	std::ifstream  data(filename.c_str());
	if(!data.is_open()){
		throw std::ifstream::failure(("CAN'T READ FILE '" + filename + "'"));
	}
	std::string line;
	unsigned int y = 0;
	while(std::getline(data, line))
	{
		std::stringstream lineStream(line);
		std::string cell;
		unsigned int x = 0;
		while(std::getline(lineStream, cell, ' '))
		{
			this->at(x, y) = my_stod(cell);
			x++;
		}
		y++;
	}

	data.close();
}

int Matrix::size(){
	return col*row;
}

} // namespace dynaland

