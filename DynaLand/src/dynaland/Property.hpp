/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef PROPERTY_H_
#define PROPERTY_H_

#include <regex>
#include <string>
#include <dynaland/Logger.hpp>
#include <dynaland/global.hpp>

using namespace std;
using namespace dynaland;

namespace dynaland {

class Property {

public:
	vector<std::string> find_values(std::string where, std::regex pattern);

	std::string find_property(std::string txt, std::string key);

	std::string find_sproperty(std::string txt, std::string key);

	int find_iproperty(std::string txt, std::string key);

	bool find_bproperty(std::string txt, std::string key);

	double find_dproperty(std::string txt, std::string key);

	vector<double> find_vproperty(std::string txt, std::string key);

	vector<p_range> find_vvproperty(std::string txt, std::string key);

	std::string read_property_file(std::string filename);

	void print_vproperty(std::string key, vector<double> values);

	void print_vvproperty(std::string key, vector<p_range> values);


	void print_property(std::string key, std::string val){
		Logger::info("%s : %s\n", key.c_str(), val.c_str());
	}

	void print_property(std::string key, int val){
		Logger::info("%s : %d\n", key.c_str(), val);
	}

	void print_property(std::string key, double val){
		Logger::info("%s : %f\n", key.c_str(), val);
	}

	void print_property(std::string key, bool val){
		Logger::info("%s : %d\n", key.c_str(), val);
	}

	double my_stod(const std::string &valueAsString);

};

} // namespace dynaland

#endif /* PROPERTY_H_ */
