/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef INDIVIDUS_H_
#define INDIVIDUS_H_

#include <vector>
#include <iostream>
#include <algorithm>
#include <memory>
#include <deque>
#include <map>

#include <dynaland/global.hpp>

using namespace std;

namespace dynaland {

using namespace dynaland;

class Population;
class Cell;
class Patch;
struct population_class;

class Individus {

public:
	Individus();

	Individus(const Population* population,
			  const shared_ptr<Cell>& cell,
			  const int id,
			  const int age,
			  const SEX& sex,
			  const int pop_class);

	virtual ~Individus();

	friend bool operator== (const Individus& individu1, const Individus& individu2);
	friend bool operator== (const weak_ptr<Individus>& individu1, const weak_ptr<Individus>& individu2);
	int getId();
	int getAge();
	SEX getSex();
	double getTemperatureGrowthSum();
	int getState();
	int getClass();
	int getMemorySize();
	int getPerceptualRange();
	double getPersistenceMemory();
	double getPersistenceTarget();
	int getPathMaxCells();
	const Population* getPopulation();
	const std::vector<std::pair<int, weak_ptr<Cell>>>& getPath();
	const std::vector<std::shared_ptr<Cell>> getMemorizedCells();
	std::shared_ptr<Cell> getCell();
	std::shared_ptr<Patch> getPatch();
	std::shared_ptr<Patch> getLastPatch();
	int getInitialHaremSize();
	std::vector<std::shared_ptr<Individus>> getHarem();
	std::shared_ptr<Cell> getPathLastCell();
	bool canUpgrade();
	bool initAgeOnUpgrade();
	void setInitAgeOnUpgrade(bool b);

	virtual void upgradeTo(int pop_class);
	void setCanUpgrade(bool up);
	void setId(const int id);
	void setAge(const int age);
	void addAge(int age);
	void setTemperatureGrowthSum(double sdd);
	void addTemperatureGrowth(double DDday);
    void setSex(const SEX& sex);
	void setState(const int state);
	void setPopulation(const Population* population);
	//void setPath(const vector<shared_ptr<Cell>>& path);
	void setCell(const shared_ptr<Cell>& cell);
    void setClass(const int pop_class);

    void addPathCell(const int step, const shared_ptr<Cell>& cell);
    void clearPath();

	void clearHarem();
	void addHaremIndividus(const shared_ptr<Individus>& individus);
	void removeHaremIndividus(const shared_ptr<Individus>& individus);

	void addChild(shared_ptr<Individus> child);
	int getNbChildren();

	int getDispersionDmax();
	void setDispersionDmax(int dmax);

	shared_ptr<Individus> reproduce(
			const shared_ptr<Individus>& partenaire,
			const int id, const SEX& sex, const int pop_class);

	static vector<shared_ptr<Individus>> filter(
			const vector<shared_ptr<Individus>>& individus,
			const int pop_id,
			const int pop_class,
			const SEX& sex,
			const INDIVIDUS_STATE& state);

	static vector<shared_ptr<Individus>> filterByClasses(
			const vector<shared_ptr<Individus>>& individus,
			const int pop_id,
			const vector<population_class>& pop_class,
			const INDIVIDUS_STATE& state);

//	static int factory(Population* population, int nbIndividus, int id_max, shared_ptr<Cell> cell);

	void print();

protected:

	int id;
	int age;
	double sdd; // sum of developpement temperature
//	std::deque<double> dd; //history of developpement temperature
	bool canBeUpgraded;
	bool reinitAgeOnUpgrade;
	int dispersionDmax;
	int pop_class;
	SEX sex;
	int state;
	vector<shared_ptr<Individus>> children;
	vector<shared_ptr<Individus>> harem;
	const Population* population;
	weak_ptr<Cell> cell;
	std::vector<std::pair<int, std::weak_ptr<Cell>>> path;
};

} // namespace dynaland

#endif /* INDIVIDUS_H_ */
