/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/Cell.hpp>
#include <dynaland/global.hpp>
#include <dynaland/Individus.hpp>
#include <dynaland/Logger.hpp>
#include <dynaland/Map.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/Patch.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <map>
#include <memory>
#include <stdexcept>

using namespace dynaland;

namespace dynaland {

Cell::Cell(){
	this->id = -1;
	this->id_occ = 0;
	this->xy = {-1, -1};
	this->landscape = 0;
	this->k = 0;
	this->cost = 0;
	this->visits = 0;
	this->visits_tot = 0;
	this->border = false;
}

Cell::Cell(Map* landscape, const gpoint& xy, const int id, const bool isBorder){
	this->id = id;
	this->id_occ = 0;
	this->xy = xy;
	this->list_individus.clear();
	this->landscape = landscape;
	this->k = 0;
	this->cost = 0;
	this->visits = 0;
	this->visits_tot = 0;
	this->border = isBorder;
}


Cell::Cell(Map* landscape, const gpoint& xy, const int id, const int id_occ,
		const double k, const double cost, const bool isBorder){
	this->id = id;
	this->id_occ = id_occ;
	this->xy = xy;
	this->list_individus.clear();
	this->k = k;
	this->cost = cost;
	this->visits = 0;
	this->visits_tot = 0;
	this->landscape = landscape;
	this->border = isBorder;
}

Cell::~Cell(){
	landscape = 0;
	list_individus.clear();
	neighbors_cells.clear();
}

bool operator== (const Cell& cell1, const Cell& cell2){
	return((cell1.id == cell2.id));
}

bool operator== (const shared_ptr<Cell>& cell1, const shared_ptr<Cell>& cell2){
	return((cell1->id == cell2->id));
}

bool Cell::isBorder(){
	return border;
}

int Cell::getId(){
	return this->id;
}

int Cell::getIdOcc(){
	return this->id_occ;
}

std::shared_ptr<Patch> Cell::getPatch(){
	return landscape->getPatch(this->id_occ);
}

const gpoint Cell::getCoord(){
	return this->xy;
}

Map* Cell::getMap(){
	return this->landscape;
}

const vector<weak_ptr<Individus>>& Cell::getIndividus(){
	return list_individus;
}


vector<shared_ptr<Individus>> Cell::getIndividus2(){
	vector<shared_ptr<Individus>> results;
	for(vector<weak_ptr<Individus>>::iterator it = list_individus.begin(); it != list_individus.end(); it++){
		results.push_back(it->lock());
	}
	return results;
}

void Cell::addIndividus(const weak_ptr<Individus>& individus){
	this->list_individus.push_back(individus);
}

void Cell::incrementVisit(){
	this->setProperty(VISITS, (this->getProperty(VISITS)+1));
}

void Cell::initVisit(){
	this->setProperty(VISITS, 0);
}

void Cell::incrementVisitTot(){
	this->setProperty(VISITS_TOT, (this->getProperty(VISITS_TOT)+1));
}

void Cell::removeIndividus(const weak_ptr<Individus>& individus){
//	vector<weak_ptr<Individus>>::iterator position = find(list_individus.begin(), list_individus.end(), individus);
//	if (position != list_individus.end()){
//		list_individus.erase(position);
//	}else{
//		throw runtime_error("WARNING individu not founded " + individus.lock()->getId());
//	}
	list_individus.erase(std::remove(list_individus.begin(), list_individus.end(), individus), list_individus.end());
}

void Cell::setIndividus(const vector<weak_ptr<Individus>>& individus){
	this->list_individus = individus;
}

int Cell::getNbIndividus(){
	return this->list_individus.size();
}

Matrix Cell::getFecondityEffect(){
	return this->fecondityEffect;
}

Matrix Cell::getSurvialEffect(){
	return this->survivalEffect;
}

std::vector<double> Cell::getDispersionEffect(){
	return this->dispersionEffect;
}

void Cell::setDispersionEffect(const std::vector<double>& v){
	this->dispersionEffect = v;
}

void Cell::setFecondityEffect(const Matrix& m){
	this->fecondityEffect = m;
}

void Cell::setSurvivalEffect(const Matrix& m){
	this->survivalEffect = m;
}

void Cell::setDispersionDmaxEffect(int dmax){
	for(vector<weak_ptr<Individus>>::const_iterator it = this->getIndividus().begin();
			it != this->getIndividus().end(); it++){
		it->lock()->setDispersionDmax(dmax);
	}
}

vector<const Population*> Cell::getPopulations(){
	set<const Population*> pops;
	for(vector<weak_ptr<Individus>>::const_iterator it = this->getIndividus().begin();
		it != this->getIndividus().end(); it++){
		pops.insert((*it).lock()->getPopulation());
	}
	vector<const Population*> populations(pops.begin(), pops.end());
	return populations;
}

double Cell::getProperty(const CELL_PROPERTY& property){
	switch(property){
		case COST:
			return cost;
		case CARRYING_CAPACITY:
			return k;
		case VISITS:
			return visits;
		case VISITS_TOT:
			return visits_tot;
		case ABUNDANCE:
			return (double)getNbIndividus();
		case KMAX:
			return getPatch()->getKmax();
		default:
			return -1;
	}
	return -1;
}

void Cell::setProperty(const CELL_PROPERTY& property, const double& value){
	switch(property){
		case COST:
			cost = value;
			break;
		case CARRYING_CAPACITY:
			k = value;
			break;
		case VISITS:
			visits = value;
			break;
		case VISITS_TOT:
			visits_tot = value;
			break;
		default:
			break;
	}
}

void Cell::initNeighbors(){
	Logger::debug("Cell::initNeighbors start\n");
	const gpoint xy = this->getCoord();
	vector<shared_ptr<Cell>> neighbors = landscape->getCellNeighbors(xy);
	this->setNeighborsCells(neighbors);
	Logger::debug("Cell::initNeighbors end\n");
}

void Cell::initCosts(const int& perceptual_range, const CELL_PROPERTY& property){
	Logger::debug("Cell::initCosts start, per_range: %d\n", perceptual_range);
	const vector<double>& costs = landscape->computeNeighborsCosts(xy, perceptual_range, property);
	this->setNeighborsCosts(costs);
	Logger::debug("Cell::initCosts end\n");
}

void Cell::clearCosts(){
//	Logger::debug("Cell::clearCosts\n");
	this->neighbors_costs.clear();
//	Logger::debug("Cell::clearCosts end\n");
}

vector<double> Cell::getNeighborsCosts(const int& perceptual_range){
	Logger::debug("Cell::getNeighborsCosts : size:%d, perceptual range:%d\n", neighbors_costs.size(), perceptual_range);
	if(neighbors_costs.size() == 0){
//		if(list_individus.size() == 0){
//			throw std::out_of_range("can't get population information from an empty individus list");
//		}
		this->initCosts(perceptual_range, COST);
	}
	Logger::debug("Cell::getNeighborsCosts end\n");
	return this->neighbors_costs;
}

void Cell::setNeighborsCosts(const vector<double>& costs){
	this->neighbors_costs = costs;
}

vector< weak_ptr<Cell> > Cell::getNeighborsCells(){
	Logger::debug("Cell::getNeighborsCells : %d\n", neighbors_cells.size());
	if(neighbors_cells.size() == 0){
		this->initNeighbors();
	}
	Logger::debug("Cell::getNeighborsCells end\n");
	return this->neighbors_cells;
}

void Cell::setNeighborsCells(const vector<weak_ptr<Cell>>& neighbors){
	this->neighbors_cells = neighbors;
}

void Cell::setNeighborsCells(const vector<shared_ptr<Cell>>& neighbors){
	this->neighbors_cells.clear();
	for(vector<shared_ptr<Cell>>::const_iterator it = neighbors.begin();
			it != neighbors.end(); ++it){
		this->neighbors_cells.push_back((*it));
	}
}

void Cell::print(){
	Logger::debug("x: %d, y: %d\n", xy.x, xy.y);
	Logger::debug("nomenclature : %d\n", id_occ);
	Logger::debug("cost         : %f\n", cost);
	Logger::debug("k            : %f\n", k);
	Logger::debug("visits       : %d\n", (int)visits);
	Logger::debug("landscape    : %d\n", this->landscape);

}

std::string Cell::info(){
	std::stringstream st;
	st << "cell " << "(" << xy.x << ", " << xy.y << ") : N=" << getNbIndividus();
	return st.str();
}

void Cell::print(const CELL_PROPERTY& property){
	Logger::debug("x: %d, y: %d, value: %f\n", xy.x, xy.y, getProperty(property));
}

void Cell::printAbundance(){
	Logger::debug("x: %d, y: %d, N: %d\n", xy.x, xy.y, getNbIndividus());
}

void Cell::printIndividus(){
	vector<weak_ptr<Individus>>::iterator it = list_individus.begin();
	while(it != list_individus.end()){
		it->lock()->print();
		it++;
	}
}

} // namespace dynaland
