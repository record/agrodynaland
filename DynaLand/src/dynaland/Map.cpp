/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/Cell.hpp>
#include <dynaland/Individus.hpp>
#include <dynaland/Logger.hpp>
#include <dynaland/Map.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/Patch.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <vector>
#include <algorithm>
#include <math.h>
#include <memory>
#include <stdexcept>
#include <map>
#include <set>

#include "gdal.h"
#include "cpl_port.h"
#include "gdal_utils.h"
#include "gdal_alg.h"
#include "cpl_conv.h"
#include "cpl_string.h"
#include "ogrsf_frmts.h"
#include "ogr_spatialref.h"

using namespace dynaland;

namespace dynaland {

Map::Map(){
	ncol = 0;
	nrow = 0;
	this->raster_file = "";
	this->projectionRef = 0;
}

Map::~Map(){
	this->cells.clear();
}

void Map::loadMap(const std::string& raster_file, const std::string& nomenclature_file){
	this->ncol = 0;
	this->nrow = 0;
	this->raster_file = raster_file;

	loadNomenclature(nomenclature_file);
	loadRaster(raster_file);
}

void Map::checkGDALdriver(const std::string& pszFormat){
	GDALAllRegister();

	GDALDriver *poDriver = GetGDALDriverManager()->GetDriverByName(pszFormat.c_str());
	if( poDriver == NULL ){
		throw std::runtime_error( ("GDAL '" + pszFormat + "' DRIVER NOT FOUND!") );
	}

}

void Map::loadRaster(const std::string& raster_file){
    Logger::debug("Map::loadMap '%s'\n", raster_file.c_str());

    const char * pszFormat = "GTiff";
    checkGDALdriver(pszFormat);

    GDALDataset  *poDataset = (GDALDataset *) GDALOpen( raster_file.c_str(), GA_ReadOnly );
    if( poDataset == NULL )
    {
        throw std::ifstream::failure(("CAN'T OPEN FILE '" + raster_file + "'"));
    }

    if( poDataset->GetGeoTransform( adfGeoTransform ) == CE_None )
    {
    	Logger::debug( "Origin = (%.6f,%.6f)\n",
                adfGeoTransform[0], adfGeoTransform[3] );
    	Logger::debug( "Pixel Size = (%.6f,%.6f)\n",
                adfGeoTransform[1], adfGeoTransform[5] );
    }else{
    	GDALClose( (GDALDatasetH) poDataset );
    	throw std::ifstream::failure( ("CAN'T READ GeoTransform for raster '" + raster_file + "'") );

    }

    GDALRasterBand *poBand = poDataset->GetRasterBand(1);
    if( poBand == NULL )
    {
    	GDALClose( (GDALDatasetH) poDataset );
    	throw std::ifstream::failure( ("CAN'T READ band 1 for raster '" + raster_file + "'") );
    }

    ncol = poBand->GetXSize();
    nrow = poBand->GetYSize();
    Logger::debug("Size: x=%d, y=%d\n", ncol, nrow);
    int nXYsize = ncol*nrow;
    float *pafScanline;
    pafScanline = (float *) CPLMalloc(sizeof(float)*nXYsize);

    if( poBand->RasterIO( GF_Read, 0, 0, ncol, nrow,
            			  pafScanline, ncol, nrow, GDT_Float32,
                          0, 0 ) != CPLE_None ){
    	CPLFree(pafScanline);
    	GDALClose( (GDALDatasetH) poDataset );
    	throw std::ifstream::failure( ("CAN'T READ data for raster '" + raster_file + "', error") );
    }else{
		std::vector<double> values;
		std::set<double> s;
		values.reserve(nXYsize);
		for( int i = 0; i < nXYsize; ++i ){
			values.push_back((double)pafScanline[i]);
			s.insert((double)pafScanline[i]);
		}
		CPLFree(pafScanline);
		Logger::debug("Nomenclature founded:\n");
		for(std::set<double>::iterator it = s.begin(); it != s.end(); it++){
			Logger::debug("%d\n", (int)(*it));
		}
		GDALClose( (GDALDatasetH) poDataset );

		for (int y = 0; y < nrow; y++){
			for (int x = 0; x < ncol; x++){
				gpoint xy = {x, y};
				int id = (x + y * ncol);
				int val = values[id];
				std::shared_ptr<Cell> cell = getCell(id);
				std::map<std::string, double> occ = Map::getNomenclature(val);
				if(cell){
					cell->setProperty(CARRYING_CAPACITY, occ["k"]);
					cell->setProperty(COST, occ["r"]);
				}else{
					bool border = isCellBorder(xy);
					cells.push_back(std::make_shared<Cell>(this, xy, id, val, occ["k"], occ["r"], border));
				}
			}
		}
    }
    Logger::debug("Map::loadMap end!\n");
}

void Map::loadNomenclature(const std::string& filename){
	Logger::debug("Map::loadMapNomenclature...\n");
	std::ifstream  data(filename.c_str());

	if(!data.is_open()){
		throw std::ifstream::failure(("CAN'T OPEN NOMENCLATURE FILE '" + filename + "'"));
	}

	std::string line;

	// skip header
	std::getline(data, line);

	while(std::getline(data, line))
	{
		std::stringstream lineStream(line);
		std::string sid;
		std::string sk;
		std::string sr;

		std::getline(lineStream, sid, ' ');
		std::getline(lineStream, sr, ' ');
		std::getline(lineStream, sk, ' ');
		std::map<std::string, double> row;
		row.insert(std::make_pair("k", atof(sk.c_str())));
		row.insert(std::make_pair("r", atof(sr.c_str())));
		this->nomenclature.insert(std::make_pair(atoi(sid.c_str()), row));
		Logger::debug("id: %d, k: %f, r: %f\n", atoi(sid.c_str()), atof(sk.c_str()), atof(sr.c_str()));
	}

	data.close();
	Logger::debug("Map::loadMapNomenclature end!\n");
}

gpoint Map::point2grid(double* adfGeoTransform, double x, double y){
	gpoint point;
	point.x = (int)(x - adfGeoTransform[0]) / adfGeoTransform[1];
	point.y = (int)(y - adfGeoTransform[3]) / adfGeoTransform[5];
	return point;
}

ppoint Map::grid2point(double* adfGeoTransform, int x, int y){
	ppoint xy;
	xy.x = (x + 0.5) * adfGeoTransform[1] + adfGeoTransform[0];
	xy.y = (y + 0.5) * adfGeoTransform[5] + adfGeoTransform[3];
	return xy;
}


int Map::getFieldIndex(const char* shape_file, OGRFeatureDefn *poFDefn, const char* field_name){
	int iField = poFDefn->GetFieldIndex(field_name);
	if(iField == -1){
		throw std::out_of_range( ("Missing required field '" + std::string(field_name) + "' in the shapefile '" +  std::string(shape_file) + "'!") );
	}
	return iField;
}

int Map::getFieldValue(const char* shape_file, OGRFeature *poFeature, const char* field_name){
	int value = poFeature->GetFieldAsInteger( field_name );
	return value;
}

void Map::createShapeFields(OGRLayer *poLayer){
	OGRFieldDefn oField0("ID", OFTInteger64);
	oField0.SetWidth(10);
	if( poLayer->CreateField( &oField0 ) != OGRERR_NONE ){
		throw std::runtime_error( "Creating 'ID' field failed." );
	}
	OGRFieldDefn oField1("classe", OFTInteger);
	oField1.SetWidth(5);
	if( poLayer->CreateField( &oField1 ) != OGRERR_NONE ){
		throw std::runtime_error( "Creating 'classe' field failed.\n" );
	}
	OGRFieldDefn oField2("sex", OFTInteger);
	oField2.SetWidth(1);
	if( poLayer->CreateField( &oField2 ) != OGRERR_NONE ){
		throw std::runtime_error( "Creating 'sex' field failed.\n" );
	}
	OGRFieldDefn oField3("time", OFTInteger);
	oField3.SetWidth(5);
	if( poLayer->CreateField( &oField3 ) != OGRERR_NONE ){
		throw std::runtime_error( "Creating 'time' field failed.\n" );
	}
}

void Map::writeShapeIndividus(const std::string& map_filename, Population* population, int time){
	OGRFeature *poFeature;
	GDALDataset *poDS;
	OGRLayer *poLayer;

	Logger::debug("Map::writeShape start\n");
	GDALAllRegister();

	poDS = (GDALDataset*) GDALOpenEx(map_filename.c_str(), GDAL_OF_VECTOR|GDAL_OF_UPDATE, NULL, NULL, NULL );
	if( poDS == NULL )
	{
		const char *pszDriverName = "ESRI Shapefile";
		GDALDriver *poDriver = GetGDALDriverManager()->GetDriverByName(pszDriverName);
		if( poDriver == NULL ){
			throw std::runtime_error( ("GDAL 'ESRI Shapefile' DRIVER NOT FOUND!") );
		}

		poDS = poDriver->Create(map_filename.c_str(), 0, 0, 0, GDT_Unknown, NULL );
		if( poDS == NULL ){
			throw std::runtime_error( "Creation of file " + map_filename + "failed." );
		}

		Logger::debug("shape created!\n");

//		Logger::debug("setting Layer projection: %s!\n", this->projectionRef);
//		poDS->SetProjection( this->projectionRef );

		char *pszSRS_WKT = NULL;
		spatialReference.exportToWkt(&pszSRS_WKT);
		Logger::debug("spatial ref: %s", pszSRS_WKT);

		poLayer = poDS->CreateLayer( map_filename.c_str(), &spatialReference, wkbPoint, NULL );
		if( poLayer == NULL )
		{
			GDALClose(poDS);
			throw std::runtime_error( "Layer creation failed." );
		}

		Logger::debug("layer created!\n");


		OGRFieldDefn oField0("ID", OFTInteger64);
		oField0.SetWidth(10);
		if( poLayer->CreateField( &oField0 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'ID' field failed." );
		}
		OGRFieldDefn oField1("classe", OFTInteger);
		oField1.SetWidth(5);
		if( poLayer->CreateField( &oField1 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'classe' field failed.\n" );
		}

		Logger::debug("field 'classe' created!\n");

		OGRFieldDefn oField2("sex", OFTInteger);
		oField2.SetWidth(1);
		if( poLayer->CreateField( &oField2 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'sex' field failed.\n" );
		}

		Logger::debug("field 'sex' created!\n");

		OGRFieldDefn oField3("time", OFTInteger);
		oField3.SetWidth(5);
		if( poLayer->CreateField( &oField3 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'time' field failed.\n" );
		}

		Logger::debug("field 'time' created!\n");

		OGRFieldDefn oField4("sdd", OFTReal);
		oField4.SetWidth(5);
		if( poLayer->CreateField( &oField4 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'sdd' field failed.\n" );
		}

		Logger::debug("field 'sdd' created!\n");

		OGRFieldDefn oField5("canUpgrade", OFTInteger);
		oField5.SetWidth(1);
		if( poLayer->CreateField( &oField5 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'canUpgrade' field failed.\n" );
		}

		Logger::debug("field 'canUpgrade' created!\n");

		OGRFieldDefn oField6("age", OFTInteger);
		oField6.SetWidth(5);
		if( poLayer->CreateField( &oField6 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'age' field failed.\n" );
		}

		Logger::debug("field 'age' created!\n");

		Logger::debug("Fields created!\n");

	}else{
		poLayer = poDS->GetLayer(0);
	}
	OGRFeatureDefn *poFeatureDefn = poLayer->GetLayerDefn();
	const std::vector< std::shared_ptr<Individus> >& individus = population->getIndividus();

	for(std::vector< std::shared_ptr<Individus> >::const_iterator it = individus.begin();
			it != individus.end(); ++it){
		std::shared_ptr<Individus> indiv = (*it);

		poFeature = OGRFeature::CreateFeature(poFeatureDefn);
		poFeature->SetField( "id",  indiv->getId());
		poFeature->SetField( "classe",  indiv->getClass());
		poFeature->SetField( "sex",  indiv->getSex());
		poFeature->SetField( "time", time );
		poFeature->SetField( "sdd", indiv->getTemperatureGrowthSum() );
		poFeature->SetField( "canUpgrade", (indiv->canUpgrade()==1 ) );
		poFeature->SetField( "age", indiv->getAge() );
		gpoint gp;
		ppoint pp;
		OGRPoint pt;
		OGRLineString ln;

		gp = indiv->getCell()->getCoord();
		pp = grid2point(this->adfGeoTransform, gp.x, gp.y);

		pt.setX(pp.x);
		pt.setY(pp.y);
		poFeature->SetGeometry(&pt);

		if( poLayer->CreateFeature( poFeature ) != OGRERR_NONE ){
			OGRFeature::DestroyFeature(poFeature);
			GDALClose(poDS);
			throw std::runtime_error("Failed to create feature in shapefile.");
		}
		OGRFeature::DestroyFeature(poFeature);
	}
	GDALClose(poDS);
}

void Map::writeShapePaths(const std::string& map_filename, Population* population, int time){
	OGRFeature *poFeature;
	GDALDataset *poDS;
	OGRLayer *poLayer;

	Logger::debug("Map::writeShape start\n");
	GDALAllRegister();

	poDS = (GDALDataset*) GDALOpenEx(map_filename.c_str(), GDAL_OF_VECTOR|GDAL_OF_UPDATE, NULL, NULL, NULL );
	if( poDS == NULL )
	{
		const char *pszDriverName = "ESRI Shapefile";
		GDALDriver *poDriver = GetGDALDriverManager()->GetDriverByName(pszDriverName);
		if( poDriver == NULL ){
			throw std::runtime_error( ("GDAL 'ESRI Shapefile' DRIVER NOT FOUND!") );
		}

		poDS = poDriver->Create(map_filename.c_str(), 0, 0, 0, GDT_Unknown, NULL );
		if( poDS == NULL ){
			throw std::runtime_error( "Creation of file " + map_filename + "failed." );
		}
		Logger::debug("shape created!\n");

//		Logger::debug("setting Layer projection: %s!\n", this->projectionRef);
//		poDS->SetProjection( this->projectionRef );

		char *pszSRS_WKT = NULL;
		spatialReference.exportToWkt(&pszSRS_WKT);
//		Logger::debug("spatial ref: %s", pszSRS_WKT);

		poLayer = poDS->CreateLayer( map_filename.c_str(), &spatialReference, wkbLineString, NULL );

		if( poLayer == NULL )
		{
			GDALClose(poDS);
			throw std::runtime_error( "Layer creation failed." );
		}
		Logger::debug("layer created!\n");


		OGRFieldDefn oField0("ID", OFTInteger64);
		oField0.SetWidth(10);
		if( poLayer->CreateField( &oField0 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'ID' field failed." );
		}
		OGRFieldDefn oField1("classe", OFTInteger);
		oField1.SetWidth(5);
		if( poLayer->CreateField( &oField1 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'classe' field failed.\n" );
		}
		Logger::debug("field 'classe' created!\n");

		OGRFieldDefn oField2("sex", OFTInteger);
		oField2.SetWidth(1);
		if( poLayer->CreateField( &oField2 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'sex' field failed.\n" );
		}
		Logger::debug("field 'sex' created!\n");

		OGRFieldDefn oField3("time", OFTInteger);
		oField3.SetWidth(5);
		if( poLayer->CreateField( &oField3 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'time' field failed.\n" );
		}
		Logger::debug("field 'time' created!\n");

		OGRFieldDefn oField4("sdd", OFTReal);
		oField4.SetWidth(5);
		if( poLayer->CreateField( &oField4 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'sdd' field failed.\n" );
		}
		Logger::debug("field 'sdd' created!\n");

		OGRFieldDefn oField5("canUpgrade", OFTInteger);
		oField5.SetWidth(1);
		if( poLayer->CreateField( &oField5 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'canUpgrade' field failed.\n" );
		}
		Logger::debug("field 'canUpgrade' created!\n");

		OGRFieldDefn oField6("age", OFTInteger);
		oField6.SetWidth(5);
		if( poLayer->CreateField( &oField6 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'age' field failed.\n" );
		}
		Logger::debug("field 'age' created!\n");

		OGRFieldDefn oField7("patch_from", OFTInteger);
		oField7.SetWidth(5);
		if( poLayer->CreateField( &oField7 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'patch_from' field failed.\n" );
		}
		Logger::debug("field 'patch_from' created!\n");

		OGRFieldDefn oField8("patch_to", OFTInteger);
		oField8.SetWidth(5);
		if( poLayer->CreateField( &oField8 ) != OGRERR_NONE ){
			throw std::runtime_error( "Creating 'patch_to' field failed.\n" );
		}
		Logger::debug("field 'patch_to' created!\n");

		Logger::debug("Fields created!\n");

	}else{
		poLayer = poDS->GetLayer(0);
	}
	OGRFeatureDefn *poFeatureDefn = poLayer->GetLayerDefn();
	const std::vector< std::shared_ptr<Individus> >& individus = population->getIndividus();

	for(std::vector< std::shared_ptr<Individus> >::const_iterator it = individus.begin();
			it != individus.end(); ++it){
		std::shared_ptr<Individus> indiv = (*it);

		poFeature = OGRFeature::CreateFeature(poFeatureDefn);
		poFeature->SetField( "id",  indiv->getId());
		poFeature->SetField( "classe",  indiv->getClass());
		poFeature->SetField( "sex",  indiv->getSex());
		poFeature->SetField( "time", time );
		poFeature->SetField( "sdd", indiv->getTemperatureGrowthSum() );
		poFeature->SetField( "canUpgrade", (indiv->canUpgrade()==1 ) );
		poFeature->SetField( "age", indiv->getAge() );
		poFeature->SetField( "patch_from", indiv->getLastPatch()->getId() );
		poFeature->SetField( "patch_to", indiv->getPatch()->getId() );

		gpoint gp;
		ppoint pp;
		OGRPoint pt;
		OGRLineString ln;

		const std::vector<std::pair<int, std::weak_ptr<Cell>>>& cells = indiv->getPath();
		if(cells.size() > 1){
			for(std::vector<std::pair<int, std::weak_ptr<Cell>>>::const_iterator it = cells.begin(); it != cells.end(); ++it){
				gp = it->second.lock()->getCoord();
				pp = grid2point(this->adfGeoTransform, gp.x, gp.y);
				ln.addPoint(pp.x, pp.y);
			}
			poFeature->SetGeometry(&ln);
			if( poLayer->CreateFeature( poFeature ) != OGRERR_NONE ){
				OGRFeature::DestroyFeature(poFeature);
				GDALClose(poDS);
				throw std::runtime_error("Failed to create feature in shapefile.");
			}
			OGRFeature::DestroyFeature(poFeature);
		}
	}
	GDALClose(poDS);
}

int Map::loadIndividus(const std::string& shape_file, Population* population){
	Logger::debug("Map::loadIndividu start\n");

	int id_max = 0;

	int id = 0;
	int classe = 0;
	SEX sex;
	int age = 0;
	std::shared_ptr<Cell> cell;

//	population->list_individus.reserve(nbIndividus*2);
//	cell->list_individus.reserve(nbIndividus*2);

	GDALDataset *poDS;
	poDS = (GDALDataset*) GDALOpenEx(shape_file.c_str(), GDAL_OF_VECTOR, NULL, NULL, NULL );
	if( poDS == NULL )
	{
		throw std::ifstream::failure(("CAN'T READ FILE '" + shape_file + "'"));
	}
	OGRLayer *poLayer = poDS->GetLayer(0);
	OGRFeatureDefn *poFDefn = poLayer->GetLayerDefn();

	this->projectionRef = poDS->GetProjectionRef();

	Logger::debug("Projection Ref: %s\n", this->projectionRef);

	spatialReference = (*poLayer->GetSpatialRef());
	char *pszSRS_WKT = NULL;
	spatialReference.exportToWkt(&pszSRS_WKT);
	std::cout << "spatial ref: " << pszSRS_WKT << std::endl;

	getFieldIndex(shape_file.c_str(), poFDefn, "id");
	getFieldIndex(shape_file.c_str(), poFDefn, "classe");
	getFieldIndex(shape_file.c_str(), poFDefn, "sex");

	poLayer->ResetReading();
	OGRFeature *poFeature;
	int nbGeom = poLayer->GetFeatureCount();
	Logger::debug("%d INDIVIDU(S) FOUNDED!\n", nbGeom);
	while( (poFeature = poLayer->GetNextFeature()) != NULL )
	{
		OGRGeometry *poGeometry = poFeature->GetGeometryRef();

		if( poGeometry != NULL && wkbFlatten(poGeometry->getGeometryType()) == wkbPoint )
		{
			OGRPoint *poPoint = (OGRPoint *) poGeometry;
			if(adfGeoTransform){
				gpoint gPoint = point2grid(adfGeoTransform, poPoint->getX(), poPoint->getY());
				id = getFieldValue(shape_file.c_str(), poFeature, "id");
				classe = getFieldValue(shape_file.c_str(), poFeature, "classe");
				sex = SEX(getFieldValue(shape_file.c_str(), poFeature, "sex"));
				cell = getCell(gPoint);
				id_max = std::max(id, id_max);
//				printf("id: %d\n", id);
//				printf("classe: %d\n", classe);
//				printf("sex: %d\n", sex);
//				printf( "(x=%.3f, y=%3.f) -> (x=%d, y=%d)\n", poPoint->getX(), poPoint->getY(), gPoint.x, gPoint.y);

				std::shared_ptr<Individus> individu = std::make_shared<Individus>(
						population,
						cell,
						id,
						age,
						sex,
						classe);
				population->addIndividus(individu);
				cell->addIndividus(individu);
			}else{
				OGRFeature::DestroyFeature( poFeature );
				GDALClose( poDS );
				throw std::runtime_error("adfGeoTransform not defined!");
			}
		}
		OGRFeature::DestroyFeature( poFeature );
	}
	GDALClose( poDS );

	Logger::debug("Map::loadIndividu end.\n");

	return id_max;
}

std::map<std::string, double> Map::getNomenclature(const unsigned int id){
	try{
		return this->nomenclature.at(id);
	}catch(const std::out_of_range& e){
		Logger::debug("ID: %d\n", id);
		throw std::out_of_range( ("Nomenclature not found for id : " + std::to_string(id)));
	}
	std::map<std::string, double> empty;
	return empty;
}

bool Map::isCellBorder(gpoint& xy){
	if(xy.x == 0 || xy.x == (ncol-1) || xy.y == 0 || xy.y == (nrow-1)){
		return true;
	}else{
		return false;
	}
}

double Map::cellsMean(const std::vector<std::shared_ptr<Cell>>& cells, const CELL_PROPERTY& property) {
	if(cells.size() == 0){
		return 0.0;
	}else{
		double result = 0.0;
		for(std::vector<std::shared_ptr<Cell>>::const_iterator it = cells.begin();
				it != cells.end(); ++it){
			result += (*it)->getProperty(property);
		}
		return( (result / cells.size()) );
	}
}

std::shared_ptr<Cell> Map::getCell(const gpoint& xy) {
	const int idx = (xy.x + xy.y * ncol);
	std::shared_ptr<Cell> cell = getCell(idx);
	return( cell );
}

std::shared_ptr<Cell> Map::getCell(const int idx) {
	if( ((unsigned int)idx) < cells.size() ) {
		return cells.at(idx);
	} else {
		return 0;
	}
}

std::shared_ptr<Cell> Map::getCellNeighbor(const gpoint& xy, const DIRECTION& direction) {
	switch(direction){
		case E:
			return getCell({(xy.x + 1), xy.y});
		case NE:
			return getCell({xy.x + 1, (xy.y - 1)});
		case N:
			return getCell({xy.x, (xy.y - 1)});
		case NW:
			return getCell({(xy.x -1), (xy.y - 1)});
		case W:
			return getCell({(xy.x - 1), xy.y});
		case SW:
			return getCell({(xy.x - 1), (xy.y + 1)});
		case S:
			return getCell({xy.x, (xy.y + 1)});
		case SE:
			return getCell({(xy.x + 1), (xy.y + 1)});
		default:
			return 0;
	}

}

std::vector<std::shared_ptr<Cell>> Map::getCellNeighbors(const gpoint& xy) {
	std::vector<std::shared_ptr<Cell>> neighbors;
	const std::vector<DIRECTION> directions = {E, NE, N, NW, W, SW, S, SE};
	for(std::vector<DIRECTION>::const_iterator it = directions.begin(); it != directions.end(); it++){
		std::shared_ptr<Cell> cell = this->getCellNeighbor(xy, (*it));
		if(cell){
			neighbors.push_back(cell);
		}else{
			cell = std::make_shared<Cell>(this, (gpoint){-1, -1}, -1, false);
			cell->setProperty(COST, -1);
			neighbors.push_back(cell);
			Logger::debug("Map::getCellNeighbors : neighbor not found for cell (%d, %d) on direction %d\n", xy.x, xy.y, (*it));
		}
	}
	return neighbors;
}

std::vector<std::shared_ptr<Cell>> Map::getCellNeighbors(
		const gpoint& xy,
		const DIRECTION& direction,
		const int& perceptual_range)
{
	gpoint xy_min;
	gpoint xy_max;
	int ortho_perceptual_range;

	if(perceptual_range%2 == 0){
		ortho_perceptual_range = perceptual_range / 2;
	}else{
		ortho_perceptual_range = (perceptual_range - 1) / 2;
	}

	switch(direction){
		case E:
			xy_min = {(xy.x + 1), (xy.y - ortho_perceptual_range)};
			xy_max = {(xy.x + perceptual_range), (xy.y + ortho_perceptual_range)};
			break;
		case NE:
			xy_min = {(xy.x + 1), (xy.y - perceptual_range)};
			xy_max = {(xy.x + perceptual_range), (xy.y - 1)};
			break;
		case N:
			xy_min = {(xy.x - ortho_perceptual_range), (xy.y - perceptual_range)};
			xy_max = {(xy.x + ortho_perceptual_range), (xy.y - 1)};
			break;
		case NW:
			xy_min = {(xy.x - perceptual_range), (xy.y - perceptual_range)};
			xy_max = {(xy.x - 1), (xy.y - 1)};
			break;
		case W:
			xy_min = {(xy.x - perceptual_range), (xy.y - ortho_perceptual_range)};
			xy_max = {(xy.x - 1), (xy.y + ortho_perceptual_range)};
			break;
		case SW:
			xy_min = {(xy.x - perceptual_range), (xy.y + 1)};
			xy_max = {(xy.x - 1), (xy.y + perceptual_range)};
			break;
		case S:
			xy_min = {(xy.x - ortho_perceptual_range), (xy.y + 1)};
			xy_max = {(xy.x + ortho_perceptual_range), (xy.y + perceptual_range)};
			break;
		case SE:
			xy_min = {(xy.x + 1), (xy.y + 1)};
			xy_max = {(xy.x + perceptual_range), (xy.y + perceptual_range)};
			break;
		default:
			throw std::runtime_error("unknow direction : " + direction);
			xy_min = {-1, -1};
			xy_max = {-1, -1};
			break;
	}

	return getCells(xy_min, xy_max);;
}

std::vector<std::shared_ptr<Cell>> Map::getCells() {
	return cells;
}

std::vector<std::shared_ptr<Patch>> Map::getPatches() {
	return patches;
}

std::vector<std::shared_ptr<Cell>> Map::getCells(const gpoint& xy_min, const gpoint& xy_max) {
//	Logger::debug("Map::getCells start : from (%d, %d) to (%d, %d)\n", xy_min.x, xy_min.y, xy_max.x, xy_max.y);
	std::vector<std::shared_ptr<Cell>> selected;
	std::shared_ptr<Cell> cell;
	for(int j = xy_min.y; j <= xy_max.y; j++){
		for(int i = xy_min.x; i <= xy_max.x; i++){
			gpoint xy = {i, j};
			cell = getCell(xy);
			if(cell){
				selected.push_back(cell);
			}else{
				Logger::debug("getCells : (%d, %d) not founded!\n", i, j);
			}
		}
	}
	return selected;
}

std::vector<std::shared_ptr<Cell>> Map::getCellsByOcc(const int id_occ){
	std::vector<std::shared_ptr<Cell>> selected;
	for(std::vector< std::shared_ptr<Cell> >::const_iterator it = cells.begin();
			it != cells.end(); it++){
		if((*it)->getIdOcc() == id_occ){
			selected.push_back((*it));
		}
	}
	return selected;
}

void Map::initPatches(){
	for(std::map<int, std::map<std::string, double>>::iterator it = nomenclature.begin();
			it != nomenclature.end(); it++){
		int id = it->first;
		Logger::debug("building patch %d ...\n", id);
		std::vector< std::shared_ptr<Cell> > cs = this->getCellsByOcc(id);
		if(cs.size() > 0){
			Logger::debug("nb cells: %d\n", cs.size());
			std::shared_ptr<Patch> p = std::make_shared<Patch>(this, id, cs);
			patches.push_back(p);
		}else{
			Logger::debug("Patch %d is empty!\n", id);
		}
	}
}

std::shared_ptr<Patch> Map::getPatch(const int id){
//	Logger::debug("Map::getPatch with id= %d\n", id);
	for(std::vector< std::shared_ptr<Patch> >::const_iterator it = patches.begin();
			it != patches.end(); it++){
		if( (*it)->getId() == id){
//			Logger::debug("Patch founded!\n");
			return (*it);
		}
	}
	Logger::debug("Patch not founded!\n");
	throw std::runtime_error("Patch was not found");
	return 0;
}

void Map::clearCosts(){
	for(std::vector< std::shared_ptr<Cell> >::const_iterator it = cells.begin();
				it != cells.end(); it++){
		(*it)->clearCosts();
	}
}

std::vector<double> Map::computeNeighborsCosts(const gpoint& xy,
		const int& perceptual_range, const CELL_PROPERTY& property)
{
	Logger::debug("Map::computeNeighborsCosts start for cell (%d, %d)\n", xy.x, xy.y);
	std::vector<double> costs;
	double diagonal_bias = 1;
	int direction;
	const std::vector<int>& directions = {E, NE, N, NW, W, SW, S, SE};
	for(std::vector<int>::const_iterator it = directions.begin(); it != directions.end(); it++){
		direction = (*it);
		std::vector<std::shared_ptr<Cell>> selected = this->getCellNeighbors(xy, DIRECTION(direction), perceptual_range);
		if(selected.size() > 0){
			if( (direction % 2) == 0){ // NE, NW, SW, SE
				diagonal_bias = M_SQRT2;
			}else{
				diagonal_bias = 1;
			}
			costs.push_back( (this->cellsMean(selected, property) * diagonal_bias) );
		}else{
			costs.push_back(0);
			Logger::info(
				"Map::computeNeighborsCosts : neighbor(s) not found for cell (%d, %d) on direction %d\n",
				xy.x, xy.y, direction);
		}
	}
	return costs;
}

std::vector<double> Map::computeDirectionalBias(const std::shared_ptr<Cell>& cell_source,
		const std::shared_ptr<Cell>& cell_target, const double& directional_persitence,
		const CELL_PROPERTY& bias_type){
	Logger::debug("Map::computeDirectionalBias start, dir_per: %f\n", directional_persitence);
	std::vector<double> bias = {1, 1, 1, 1, 1, 1, 1, 1};

	if(cell_source && cell_target){
		if(cell_source == cell_target){
			Logger::info("Map::computeDirectionalBias : CELL SOURCE == CELL TARGET\n");
			cell_source->print();
		}
		const gpoint& xy0 = cell_source->getCoord();
		const gpoint& xy1 = cell_target->getCoord();

		int dx0 = xy1.x - xy0.x;
		int dy0 = xy1.y - xy0.y;

		if(dx0 != 0){
			dx0 = dx0 / abs(dx0);
		}
		if(dy0 != 0){
			dy0 = dy0 / abs(dy0);
		}

		//{E, NE, N, NW, W, SW, S, SE}
		DIRECTION start = C;
		switch(dx0){
			case -1:
				switch(dy0){
					case -1:
						start = NW;
						break;
					case 0:
						start = W;
						break;
					case 1:
						start = SW;
						break;
				}
				break;
			case 0:
				switch(dy0){
					case -1:
						start = N;
						break;
					case 1:
						start = S;
						break;
				}
				break;
			case 1:
				switch(dy0){
					case -1:
						start = NE;
						break;
					case 0:
						start = E;
						break;
					case 1:
						start = SE;
						break;
				}
				break;
		}
		double b = 1;
		const std::vector<int>& po = {0, 1, 2, 3, 4, 3, 2, 1};
		int idx = 0;
		for(int d = (start-1); d < SE; d++){
			b = pow(directional_persitence, po[idx]);
			bias[d] = b;
			idx++;
			Logger::debug("bias %d: %f\n", d, b);
		}
		for(int d = (E-1); d < (start-1); d++){
			b = pow(directional_persitence, po[idx]);
			bias[d] = b;
			idx++;
			Logger::debug("bias %d: %f\n", d, b);
		}
		Logger::debug("dx: %d, dy: %d, start: %d\n", dx0, dy0, (start-1));
	}else{
//		for(int d = E; d <= SE; d++){
//			bias.push_back(1);
//		}
		Logger::debug("missing cell source (%d) or target (%d)\n", (cell_source==0), (cell_target==0));
	}
	Logger::debug("Map::computeDirectionalBias end\n");
	return bias;
}

std::vector<double> Map::computeSMSprobabilities(
		const std::shared_ptr<Cell>& cell,
		const std::shared_ptr<Cell>& previous,
		const std::shared_ptr<Cell>& goal,
		const double& mem_dir_persistence,
		const double& goal_dir_persistence,
		const std::vector<std::shared_ptr<Cell>>& memorizedCells,
		const int& perceptual_range)
{
	Logger::debug("Map::computeSMSprobabilities start for cell (%d, %d)\n", cell->getCoord().x, cell->getCoord().y);
	std::vector<double> proba = {0, 0, 0, 0, 0, 0, 0, 0};
	std::vector<double> not_mem  = {1, 1, 1, 1, 1, 1, 1, 1};
	const std::vector<double>& costs = cell->getNeighborsCosts(perceptual_range);
	const std::vector<double>& mem_bias = computeDirectionalBias(previous, cell, mem_dir_persistence, MEMORY_BIAS);
	const std::vector<std::weak_ptr<Cell>>& neighbors = cell->getNeighborsCells();

	Logger::debug("memorized cells : %d\n", memorizedCells.size());
	if(!memorizedCells.empty()){
		for(std::vector<std::shared_ptr<Cell>>::const_iterator it = memorizedCells.begin(); it != memorizedCells.end(); it++){
			Logger::debug("memorized cell : %d, %d\n", (*it)->getCoord().x, (*it)->getCoord().y);
		}
		Logger::debug("---\n");
		Logger::debug("neighbors: %d\n", neighbors.size());
		int i = 0;

		for(std::vector<std::weak_ptr<Cell>>::const_iterator it = neighbors.begin(); it != neighbors.end(); ++it){
			std::weak_ptr<Cell> wcell = (*it);
			std::shared_ptr<Cell> cell = wcell.lock();
			if(cell){
				gpoint xy = cell->getCoord();
				Logger::debug("neighbor : %d, %d\n", xy.x, xy.y);
				const std::vector<std::shared_ptr<Cell>>::const_iterator& position = find(memorizedCells.begin(), memorizedCells.end(), cell);
				if(position != memorizedCells.end()){
					not_mem[i] = 0;
					Logger::debug("memorized at %d\n", i+1);
				}
			}
			i++;
		}
	}

	double costs_sum = 0;
	double mean_cost = 0;
	Logger::debug("---\n");
	for(unsigned int i=0; i < 8; i++){
		Logger::debug("cost %d : %f\n", i, costs[i]);
		mean_cost = costs[i] * mem_bias[i] * not_mem[i];
		if(mean_cost > 0){
			proba[i] = 1 / mean_cost;
		}
		costs_sum += proba[i];
	}
	Logger::debug("proba sum : %f\n", costs_sum);
	if(costs_sum > 0){
		for(unsigned int i=0; i < 8; i++){
			proba[i] = proba[i] / costs_sum;
		}
	}
	return proba;
}

void Map::writeRaster(const std::string& map_filename, std::vector<double> values){

	const char * pszFormat = "GTiff";
    checkGDALdriver(pszFormat);

    GDALDriver *poDriver = GetGDALDriverManager()->GetDriverByName(pszFormat);
	GDALDataset *poSrcDS = (GDALDataset *) GDALOpen( this->raster_file.c_str(), GA_ReadOnly );

	GDALDataset *poDstDS = poDriver->CreateCopy( map_filename.c_str(), poSrcDS, FALSE,
	                                NULL, NULL, NULL );
	if( poDstDS != NULL ){
		GDALClose( (GDALDatasetH) poSrcDS );
		Logger::debug("CreateCopy done !\n");
	}else{
		GDALClose( (GDALDatasetH) poSrcDS );
		throw std::istream::failure("No createCopy result !\n");
	}

	GDALRasterBand *poBand = poDstDS->GetRasterBand(1);

	float* abyRaster = (float *) CPLMalloc(sizeof(float) * values.size());

	for( unsigned int i = 0; i < values.size(); ++i ){
		abyRaster[i] = (float)(values[i]);
	}

	CPLErr err = poBand->RasterIO( GF_Write, 0, 0, ncol, nrow,
	                      abyRaster, ncol, nrow, GDT_Float32, 0, 0 );

	if(err == CPLE_None){
		Logger::debug("Raster writed!\n");
	}else{
		throw std::istream::failure( ("CAN'T WRITE RASTER '" + map_filename + "'\n") );
	}

    CPLFree( abyRaster );
    GDALClose( (GDALDatasetH) poDstDS );
}

std::vector<double> Map::getValues(const CELL_PROPERTY& property){
	std::vector<double> values;
	for (int y = 0; y < nrow; y++){
		for (int x = 0; x < ncol; x++){
			double value = this->getCell({x, y})->getProperty(property);
			values.push_back(value);
		}
	}
	return values;
}

std::vector<double> Map::getAbundances(){
	std::vector<double> values;
	for (int y = 0; y < nrow; y++){
		for (int x = 0; x < ncol; x++){
			double value = (double) this->getCell({x, y})->getNbIndividus();
			values.push_back(value);
		}
	}
	return values;
}


std::string Map::print(const CELL_PROPERTY& property){
	std::stringstream map;
	for (int y = 0; y < nrow; y++){
		for (int x = 0; x < ncol; x++){
			double value = this->getCell({x, y})->getProperty(property);
			map << value << "\t";
		}
		map << "\n";
	}
	return( map.str() );
}

std::string Map::printAbundance(){
	std::stringstream map;
	for (int y = 0; y < nrow; y++){
		for (int x = 0; x < ncol; x++){
			int value = this->getCell({x, y})->getNbIndividus();
			map << value << "\t";
		}
		map << "\n";
	}
	return( map.str() );
}

} // namespace dynaland
