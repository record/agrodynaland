/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef PROCESS_H_
#define PROCESS_H_

#include <dynaland/Cell.hpp>
#include <dynaland/Individus.hpp>
#include <dynaland/Map.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/random/RandomGenerator.hpp>
#include <vector>
#include <memory>
#include <string>

using namespace dynaland;

namespace dynaland {

struct dispersion_step {int i; std::shared_ptr<Individus> who; std::shared_ptr<Cell> from; std::shared_ptr<Cell> to;};

class Process {

public:
	Process();
	~Process();
	Process(int seed, int random_type, int dispersion_on, int survival_on, int reproduction_on, std::string dir_results);
	int dispersion(Map* amap, Population* pop, int step);
	void survival(Map* amap, Population* pop);
	int reproduction(Map* amap, Population* pop, int last_individus_id);
	int sexual_reproduction(Map* amap, Population* pop, int last_individus_id);
	int asexual_reproduction(Map* amap, Population* pop, int last_individus_id);
	void setStatus(int dispersion_on, int survival_on, int reproduction_on);
	void initResults(Population* pop);
	void initPatchResults(Population* pop);
	void clearResults(Population* pop);
	void setDirResults(std::string dir_results);
	std::string getDirResults();
	std::string getPopFileResults(Population* pop);
	std::string getPatchPopFileResults(Population* pop);
	void writeDynPop(const int t, Population* pop);
	void writePatchPop(const int t, Map* amap, Population* pop);
	void writePop(Map* amap, Population* pop);
	void writePaths(Map* amap, Population* pop);
	void writeCosts(Map* amap, Population* pop);
	void writeKs(Map* amap, Population* pop);
	void writeKPatches(Map* amap, Population* pop);
	void writeVisits(Map* amap, Population* pop);
	void writeVisitsTot(Map* amap, Population* pop);
	void writeDensity(Map* amap, Population* pop);

public:
	dynaland::random::RandomGenerator myRandom;
	int dispersion_on, survival_on, reproduction_on;
	std::string dir_results;
	int time;
};

} // namespace dynaland

#endif /* PROCESS_H_ */
