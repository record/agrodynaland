/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef POPULATION_H_
#define POPULATION_H_

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <set>
#include <random>
#include <algorithm>
#include <memory>

#include <dynaland/global.hpp>
#include <dynaland/Matrix.hpp>

using namespace std;
using namespace dynaland;

namespace dynaland {

class Individus;

struct population_class {int pop_class; SEX sex;};

class Population {

public:
	Population();

	Population(int id,
			   int nb_classes,
			   double sex_ratio,
//			   vector<double> class_ratio,
			   int sexual_reproduction,
			   int pop_sex,

			   vector<p_range> dispersion_female,
			   const Matrix& survival_female,
			   const Matrix& fecondity_female,
			   int harem_size_female,

			   vector<p_range> dispersion_male,
			   const Matrix& survival_male,
			   const Matrix& fecondity_male,
			   int harem_size_male,

			   int dispersion_density_dependent,
			   int survival_density_dependent,
			   int fecondity_density_dependent,

			   double fecondity_contrib_female,
			   int perceptual_range,
			   int memory_size,
			   double persistence_memory,
			   double persistence_target,
			   int path_max_cells
	);

	~Population();

	int size() const;
	int getId() const;
	int getNbClasses() const;
	double getFecontidyContrib();

	vector<shared_ptr<Individus>> getIndividus();
	p_range getDispersion(int pop_class, SEX sex);
	double getAutoSurvival(int pop_class, SEX sex);
	double getSurvival(int pop_class_from, int pop_class_to, SEX sex);
	double getFecondity(int pop_class_from, int pop_class_to, SEX sex);
	vector<double> getFeconditiesForTarget(int pop_class_to, SEX sex);
	vector<double> getFeconditiesForSource(int pop_class_from, SEX sex);
	int getDispersionPathMaxCells() const;
	double getInitialSexRatio();
//	vector<double> getInitialClassRatio();
	int getSexualReproduction();

    int getInitialHaremFemaleSize();
	int getInitialHaremMaleSize();
	int getPerceptualRange() const;
	double getDirectionalMemoryPersistence() const;
	double getDirectionalGoalPersistence() const;
	int getMemorySize() const;
	int getDispersionDD();
	int getSurvivalDD();
	int getFecondityDD();
	int getFecondityMax(int pop_class);
	std::vector<int> getFecondityMax();
	void setFecondityMax(const std::vector<int> Fmax);
	int getDispersionDmax();

	std::string getFormatedResultsHeader();
	std::string getFormatedResults(const int t);

	void initResults();
	void clearResults();
	std::string info1();

	void addIndividus(const shared_ptr<Individus>& individus);
	void killIndividus(const shared_ptr<Individus>& individus);
	void setIndividus(const vector<shared_ptr<Individus>>& individus);
	vector<shared_ptr<Individus>> filter(int pop_class, SEX sex, INDIVIDUS_STATE state);

	void printHeader();
	void print();
	void print(const vector<shared_ptr<Individus>>& l_individus);
	void print2();

public:
	vector<shared_ptr<Individus>> list_individus;
	int id;

	int nb_classes;
	int pop_sex; // 0: only female, 1: only male, 2: male and female

	std::vector<p_range> dispersion_female;
	Matrix survival_female;
	Matrix fecondity_female;
	double fecondity_contrib_female;
	int harem_size_female;
	std::vector<int> fecondity_max;

	std::vector<p_range> dispersion_male;
	Matrix survival_male;
	Matrix fecondity_male;
	int harem_size_male;

	int dispersion_density_dependent;
	int survival_density_dependent;
	int fecondity_density_dependent;

	double sex_ratio;
	int sexual_reproduction;

	int perceptual_range;
	int memory_size;
	double persistence_memory;
	double persistence_target;
	int path_max_cells;

	vector<double> persistence_memory_bias;

	vector<int> nb_dead[2];
	vector<int> nb_born[2];
	vector<int> nb_survival[2];
	vector<int> nb_auto_survival[2];
};

} // namespace dynaland

#endif /* POPULATION_H_ */
