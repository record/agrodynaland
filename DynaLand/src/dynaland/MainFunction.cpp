/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/global.hpp>
#include <dynaland/Logger.hpp>
#include <dynaland/MainFunction.hpp>
#include <dynaland/Map.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/Process.hpp>
#include <dynaland/Property.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <memory>
#include <fstream>
#include <omp.h>
#include <ctime>
#include <locale>

using namespace dynaland;

namespace dynaland {

bool MainFunction::writeDynPop;
bool MainFunction::writePatchPop;
bool MainFunction::writePop;
bool MainFunction::writeVisits;
bool MainFunction::writePaths;
bool MainFunction::writeDensity;

void MainFunction::initFromFile(std::string parameters_file, std::string data_dir, Population* pop01, Map* map01, Process* myProcess, int* duration, int* individus_last_id){
	Logger::debug("MainFunction::init start\n");

//	std::wcout << "User-preferred locale setting is " << std::locale("").name().c_str() << '\n';

	// parameters
	int seed = 123456789;
	int random_type = 0;

	int nb_classes = 3;
	double sex_ratio = 0.5;
//	vector<double> class_ratio = {0., 0.2, 0.8};
	int sexual_reproduction = 0;
	int pop_sex = 2;

	vector<p_range> dispersion_female = {{0., 0.}, {0.2, 0.4}, {0.2, 0.4}};

	std::string s_female_file = "survival_female.csv";
	std::string f_female_file = "fecondity_female.csv";

	int harem_size_female = 1;

	double fecondity_contrib_female = 0.5;

	vector<p_range> dispersion_male = {{0., 0.}, {0.2, 0.4}, {0.2, 0.4}};

	std::string s_male_file = "survival_male.csv";
	std::string f_male_file = "fecondity_male.csv";

	int harem_size_male = 1;

	int dispersion_density_dependent = 0;
	int survival_density_dependent = 0;
	int fecondity_density_dependent = 0;

	double dispersion_path_max_cells = 20; //pow(150, 2); // 22500
	int perceptual_range = 1;
	int memory_size = 2;
	double persistence_memory = 3;
	double persistence_target = 1;

	std::string file_landcover01 = "landcover.tif";
	std::string file_nomenclature01 = "nomenclature.csv";
	std::string file_pop01 = "pop01.shp";
	std::string dir_results01 = "./";

	bool writeDynPop = true;
	bool writePatchPop = true;
	bool writePop = true;
	bool writeVisits = true;
	bool writePaths = true;
	bool writeDensity = false;

	int log_level = 0;

	int dispersion_on = 1;
	int survival_on = 1;
	int reproduction_on = 1;

	Property p;
	std::string properties = p.read_property_file(parameters_file);
	(*duration) = p.find_iproperty(properties, "duration");
	nb_classes = p.find_iproperty(properties, "nb_classes");
	sexual_reproduction = p.find_iproperty(properties, "sexual_reproduction");
	pop_sex = p.find_iproperty(properties, "pop_sex");

	sex_ratio = p.find_dproperty(properties, "sex_ratio");
//	class_ratio = p.find_vproperty(properties, "class_ratio");
	seed = p.find_iproperty(properties, "seed");
	random_type = p.find_iproperty(properties, "random_type");
	dispersion_female = p.find_vvproperty(properties, "dispersion_female");
	s_female_file = data_dir + PATH_SEP + p.find_sproperty(properties, "survival_female");
	f_female_file = data_dir + PATH_SEP + p.find_sproperty(properties, "fecondity_female");
	harem_size_female = p.find_iproperty(properties, "harem_size_female");
	fecondity_contrib_female = p.find_dproperty(properties, "fecondity_contrib_female");
	dispersion_male = p.find_vvproperty(properties, "dispersion_male");
	s_male_file = data_dir + PATH_SEP + p.find_sproperty(properties, "survival_male");
	f_male_file = data_dir + PATH_SEP + p.find_sproperty(properties, "fecondity_male");
	harem_size_male = p.find_iproperty(properties, "harem_size_male");
	dispersion_density_dependent = p.find_iproperty(properties, "dispersion_density_dependent");
	survival_density_dependent = p.find_iproperty(properties, "survival_density_dependent");
	fecondity_density_dependent = p.find_iproperty(properties, "fecondity_density_dependent");
	dispersion_path_max_cells = p.find_dproperty(properties, "dispersion_path_max_cells"); //pow(150, 2); // 22500
	perceptual_range = p.find_iproperty(properties, "perceptual_range");
	memory_size = p.find_iproperty(properties, "memory_size");
	persistence_memory = p.find_dproperty(properties, "persistence_memory");
//	persistence_target = p.find_dproperty(properties, "persistence_target");
	file_landcover01 =  data_dir + PATH_SEP + p.find_sproperty(properties, "file_landcover");
	file_nomenclature01 =  data_dir + PATH_SEP + p.find_sproperty(properties, "file_nomenclature");
	file_pop01 = data_dir + PATH_SEP + p.find_sproperty(properties, "file_population");
	dir_results01 = p.find_sproperty(properties, "dir_results") + PATH_SEP;

	writeDynPop = p.find_bproperty(properties, "writeDynPop");
	writePatchPop = p.find_bproperty(properties, "writePatchPop");
	writePop = p.find_bproperty(properties, "writePop");
	writeVisits = p.find_bproperty(properties, "writeVisits");
	writePaths = p.find_bproperty(properties, "writePaths");
	writeDensity = p.find_bproperty(properties, "writeDensity");

	MainFunction::writeDensity = writeDensity;
	MainFunction::writePatchPop = writePatchPop;
	MainFunction::writePop = writePop;
	MainFunction::writeVisits = writeVisits;
	MainFunction::writePaths = writePaths;
	MainFunction::writeDensity = writeDensity;


	log_level = p.find_iproperty(properties, "log_level");

	dispersion_on = p.find_iproperty(properties, "dispersion_on");;
	survival_on = p.find_iproperty(properties, "survival_on");;
	reproduction_on = p.find_iproperty(properties, "reproduction_on");;

	FILE* afile = fopen( std::string(dir_results01 + PATH_SEP + "init.log" ).c_str(), std::string("w").c_str() );
	if(afile == NULL){
		throw ofstream::failure( std::string("dir_results '")
		+ dir_results01
		+ std::string("' doesn't exists, please create it before launching DynaLand!"));
	}

	Logger::stream = afile;

	Logger::info("-----------------------------------------\n");
	Logger::info(("parameters_file : " + parameters_file + "\n").c_str());
	p.print_property("duration", *duration);
	p.print_property("nb_classes", nb_classes);
	p.print_property("sex_ratio", sex_ratio);
	p.print_property("pop_sex", pop_sex);
//	p.print_vproperty("class_ratio", class_ratio);
	p.print_property("sexual_reproduction", sexual_reproduction);
	p.print_property("seed", seed);
	p.print_property("random_type", random_type);
	p.print_vvproperty("dispersion_female", dispersion_female);
	p.print_vvproperty("dispersion_male", dispersion_male);
	p.print_property("dispersion_density_dependent", dispersion_density_dependent);
	p.print_property("survival_female", s_female_file);
	p.print_property("survival_male", s_male_file);
	p.print_property("survival_density_dependent", survival_density_dependent);
	p.print_property("fecondity_female", f_female_file);
	p.print_property("fecondity_male", f_male_file);
	p.print_property("harem_size_female", harem_size_female);
	p.print_property("harem_size_male", harem_size_male);
	p.print_property("fecondity_density_dependent", fecondity_density_dependent);
	p.print_property("dispersion_path_max_cells", dispersion_path_max_cells);
	p.print_property("perceptual_range", perceptual_range);
	p.print_property("memory_size", memory_size);
	p.print_property("persistence_memory", persistence_memory);
//	p.print_property("persistence_target", persistence_target);
	p.print_property("file_landcover", file_landcover01);
	p.print_property("file_nomenclature", file_nomenclature01);
	p.print_property("file_population", file_pop01);
	p.print_property("dir_result", dir_results01);
	p.print_property("writeDynPop", writeDynPop);
	p.print_property("writePatchPop", writePatchPop);
	p.print_property("writePop", writePop);
	p.print_property("writeVisits", writeVisits);
	p.print_property("writePaths", writePaths);
	p.print_property("writeDensity", writeDensity);
	p.print_property("log_level", log_level);
	p.print_property("dispersion_on", dispersion_on);
	p.print_property("survival_on", survival_on);
	p.print_property("reproduction_on", reproduction_on);

	MatrixCsv survival_female = MatrixCsv(s_female_file, nb_classes);
	MatrixCsv fecondity_female = MatrixCsv(f_female_file, nb_classes);
	MatrixCsv survival_male = MatrixCsv(s_male_file, nb_classes);
	MatrixCsv fecondity_male = MatrixCsv(f_male_file, nb_classes);

	p.print_property("survival_female", s_female_file);
	Logger::info("%s\n", survival_female.print().c_str());
	p.print_property("fecondity_female", f_female_file);
	Logger::info("%s\n", fecondity_female.print().c_str());

	p.print_property("survival_male", s_male_file);
	Logger::info("%s\n", survival_male.print().c_str());
	p.print_property("fecondity_male", f_male_file);
	Logger::info("%s\n", fecondity_male.print().c_str());

	fclose(afile);

	init(pop01, map01, myProcess, duration, individus_last_id,
		 log_level,
		 0,
		 nb_classes,
		 sex_ratio,
		 sexual_reproduction,
		 pop_sex,

		 dispersion_female,
		 survival_female,
		 fecondity_female,
		 harem_size_female,

		 dispersion_male,
		 survival_male,
		 fecondity_male,
		 harem_size_male,

		 dispersion_density_dependent,
		 survival_density_dependent,
		 fecondity_density_dependent,

		 fecondity_contrib_female,
		 perceptual_range,
		 memory_size,
		 persistence_memory,
		 persistence_target,
		 dispersion_path_max_cells,

		 file_landcover01, file_nomenclature01, file_pop01,

		 seed, random_type, dispersion_on, survival_on, reproduction_on, dir_results01);

	Logger::debug("MainFunction::init end\n");
}

void MainFunction::init(
		Population* pop01, Map* map01, Process* myProcess, int* duration, int* individus_last_id,
		int log_level,

		int pop_id,
		int nb_classes,
		double sex_ratio,
		int sexual_reproduction,
		int pop_sex,

		vector<p_range> dispersion_female,
		const Matrix& survival_female,
		const Matrix& fecondity_female,
		int harem_size_female,

		vector<p_range> dispersion_male,
		const Matrix& survival_male,
		const Matrix& fecondity_male,
		int harem_size_male,

		int dispersion_density_dependent,
		int survival_density_dependent,
		int fecondity_density_dependent,

		double fecondity_contrib_female,
		int perceptual_range,
		int memory_size,
		double persistence_memory,
		double persistence_target,
		int dispersion_path_max_cells,

		const string& file_landcover01, const string& file_nomenclature01, const std::string& file_pop01,

		int seed, int random_type, int dispersion_on, int survival_on, int reproduction_on, std::string dir_results)
{

	Logger::stream = stdout;
	Logger::__log_level = LOG_LEVEL(log_level);

	(*pop01) = initPopulation(
					0, nb_classes, sex_ratio, sexual_reproduction, pop_sex,
					dispersion_female, survival_female, fecondity_female, harem_size_female,
					dispersion_male, survival_male, fecondity_male, harem_size_male,
					dispersion_density_dependent, survival_density_dependent, fecondity_density_dependent,
					fecondity_contrib_female,
					perceptual_range, memory_size, persistence_memory, persistence_target, dispersion_path_max_cells);

	(*map01) = Map();
	(*map01) = initMap(map01, file_landcover01, file_nomenclature01);

	(*individus_last_id) = initIndividus(map01, file_pop01, pop01);

	(*myProcess) = initProcess(map01, pop01, seed, random_type, dispersion_on, survival_on, reproduction_on, dir_results);

	pop01->print2();

}

Population MainFunction::initPopulation(
		   int pop_id,
		   int nb_classes,
		   double sex_ratio,
		   int sexual_reproduction,
		   int pop_sex,

		   vector<p_range> dispersion_female,
		   const Matrix& survival_female,
		   const Matrix& fecondity_female,
		   int harem_size_female,

		   vector<p_range> dispersion_male,
		   const Matrix& survival_male,
		   const Matrix& fecondity_male,
		   int harem_size_male,

		   int dispersion_density_dependent,
		   int survival_density_dependent,
		   int fecondity_density_dependent,

		   double fecondity_contrib_female,
		   int perceptual_range,
		   int memory_size,
		   double persistence_memory,
		   double persistence_target,
		   int path_max_cells){
	return Population(
			pop_id, nb_classes, sex_ratio, sexual_reproduction, pop_sex,
			dispersion_female, survival_female, fecondity_female, harem_size_female,
			dispersion_male, survival_male, fecondity_male, harem_size_male,
			dispersion_density_dependent, survival_density_dependent, fecondity_density_dependent,
			fecondity_contrib_female,
			perceptual_range, memory_size, persistence_memory, persistence_target, path_max_cells);
}

int MainFunction::initIndividus(Map* aMap, const std::string& shape_file, Population* population){
	int last_id = aMap->loadIndividus(shape_file, population);
	return last_id;
}

Map MainFunction::initMap(Map* aMap, const string& file_landcover01, const string& file_nomenclature01){
	aMap->loadMap(file_landcover01, file_nomenclature01);
	aMap->initPatches();
	return (*aMap);
}

Process MainFunction::initProcess(Map* aMap, Population* pop, int seed, int random_type, int dispersion_on, int survival_on, int reproduction_on, std::string dir_results){
	Process aProcess = Process(seed, random_type, dispersion_on, survival_on, reproduction_on, dir_results);
	aProcess.initResults(pop);
	aProcess.writeDynPop(0, pop);
	aProcess.initPatchResults(pop);
	aProcess.writePatchPop(0, aMap, pop);
	return aProcess;
}

void MainFunction::compute(Population* pop01, Map* map01, Process* myProcess, int* individus_last_id){
	Logger::debug("MainFunction::compute start\n");

	double c_elapsed_secs;
	double t_elapsed_secs = -1;

	clock_t c_begin = clock();
	double t_begin = MainFunction::get_wtime();

	if(myProcess->dispersion_on){
		Logger::info("---------------\n");
		Logger::info("DISPERSION\n");
		int path_max_cells = pop01->getDispersionPathMaxCells();
		for(int step=0; step < path_max_cells; step++)
		{
			Logger::info("\n--> SMS STEP %d\n", step);
			int nb_dispersers = myProcess->dispersion(map01, pop01, step);
			if(nb_dispersers == 0){
				break;
			}
		}
		pop01->print2();
	}

	clock_t c_disp = clock();
	double t_disp = MainFunction::get_wtime();
	c_elapsed_secs = double(c_disp - c_begin) / CLOCKS_PER_SEC;
	t_elapsed_secs = t_disp - t_begin;
	Logger::info("dispersion : elapsed time : %f s (cpu: %f s)\n", t_elapsed_secs, c_elapsed_secs);

	if(myProcess->survival_on){
		Logger::info("---------------\n");
		Logger::info("SURVIVAL\n");
		myProcess->survival(map01, pop01);
		pop01->print2();
	}

	clock_t c_surv = clock();
	double t_surv = MainFunction::get_wtime();
	c_elapsed_secs = double(c_surv - c_disp) / CLOCKS_PER_SEC;
	t_elapsed_secs = t_surv - t_disp;
	Logger::info("survival : elapsed time : %f s (cpu: %f s)\n", t_elapsed_secs, c_elapsed_secs);

	if(myProcess->reproduction_on){
		Logger::info("---------------\n");
		Logger::info("REPRODUCTION\n");
		(*individus_last_id) = myProcess->reproduction(map01, pop01, (*individus_last_id));
		pop01->print2();
	}

	myProcess->time++;

	clock_t c_repro = clock();
	double t_repro = MainFunction::get_wtime();
	c_elapsed_secs = double(c_repro - c_surv) / CLOCKS_PER_SEC;
	t_elapsed_secs = t_repro - t_surv;
	Logger::info("reproduction : elapsed time : %f s (cpu: %f s)\n", t_elapsed_secs, c_elapsed_secs);

	Logger::debug("MainFunction::compute end\n");
}

void MainFunction::finish(Population* pop01, Map* map01, Process* myProcess){
	Logger::debug("MainFunction::finish start\n");

	double c_elapsed_secs;
	double t_elapsed_secs = -1;

	clock_t c_begin = clock();
	double t_begin = MainFunction::get_wtime();

	myProcess->writeVisitsTot(map01, pop01);

	clock_t c_end = clock();
	double t_end = MainFunction::get_wtime();
	c_elapsed_secs = double(c_end - c_begin) / CLOCKS_PER_SEC;
	t_elapsed_secs = t_end - t_begin;
	Logger::debug("elapsed time : %f s (cpu: %f s)\n", t_elapsed_secs, c_elapsed_secs);

	Logger::debug("MainFunction::finish end\n");
}

double MainFunction::get_wtime(){
	double t_begin;
	#if defined(_OPENMP)
		t_begin = omp_get_wtime();
	#else
		t_begin = -1;
//		Logger::debug("WARNING : OMP NOT DEFINED!\n");
	#endif
	return t_begin;
}

} // namespace dynalands
