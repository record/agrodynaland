/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <stdexcept>
#ifndef EOF
#define EOF (-1)
#endif

#include <dynaland/global.hpp>
#include <dynaland/Property.hpp>
#include <dynaland/Logger.hpp>

using namespace dynaland;

namespace dynaland {

std::vector<std::string> Property::find_values(std::string where, std::regex pattern){
	std::vector<std::string> values;
	std::regex_iterator<std::string::iterator> it (where.begin(), where.end(), pattern);
	std::regex_iterator<std::string::iterator> end;
	while (it != end){
//		cout << it->str() << endl;
		values.push_back(it->str());
		++it;
	}
	return values;
}

std::string Property::find_property(std::string txt, std::string key){
	std::string values_str = "NA";
	std::regex rkey(key);
	std::regex rvalue("^[\\w]+[\\s]*[=][\\s]*([\\d\\s{},.-]+)");
	std::smatch match;
	std::string line;
	std::stringstream properties;
	properties << txt;
	bool founded = false;
	while(!founded && std::getline(properties, line)){
		std::regex_search(line, rkey);
		if(std::regex_search(line, rkey)){
			founded = true;
		}
	}
	if(founded){
		std::regex_search(line, match, rvalue);
		if(match.size() != 2){
			std::string exp = "Property not found for '" + key + "'";
			throw std::runtime_error(exp);
		}
//		cout << "Property found : " << match.str(1);
		values_str = match.str(1);
	}else{
		std::string exp = "Property not found for '" + key + "'";
		throw std::runtime_error(exp);
	}
//	cout << " : " << values_str << endl;
	return values_str;
}

std::string Property::find_sproperty(std::string txt, std::string key){
	std::string values_str = "NA";
	std::regex rkey(key);
	std::regex rvalue("^[\\w]+[\\s]*[=][\\s]*([\\w\\.\\/]+)");
	std::smatch match;
	std::string line;
	std::stringstream properties;
	properties << txt;
	bool founded = false;
	while(!founded && std::getline(properties, line)){
		if(std::regex_search(line, rkey)){
			founded = true;
		}
	}
	if(founded){
		std::regex_search(line, match, rvalue);
		if(match.size() != 2){
			std::string exp = "Property not found for '" + key + "'";
			throw std::runtime_error(exp);
		}
//		cout << "Property found : " << match.str(1);
		values_str = match.str(1);
	}else{
		std::string exp = "Property not found for '" + key + "'";
		throw std::runtime_error(exp);
	}
//	cout << " : " << values_str << endl;
	return values_str;
}

int Property::find_iproperty(std::string txt, std::string key){
	std::string values_str = find_property(txt, key);
	return stoi(values_str);
}

bool Property::find_bproperty(std::string txt, std::string key){
	return (find_iproperty(txt, key) == 1);
}

double Property::find_dproperty(std::string txt, std::string key){
	std::string values_str = find_property(txt, key);
	return my_stod(values_str);
}

std::vector<double> Property::find_vproperty(std::string txt, std::string key){
	std::stringstream properties;
	properties << txt;
	std::regex rvalues(R"([\d\.\-]+)");
	std::string values_str;
	std::vector<std::string> values;
	std::vector<double> dvalues;
	std::vector<std::string>::iterator it;

	values_str = find_property(txt, key);
	values = find_values(values_str, rvalues);
	it = values.begin();
	if(it == values.end()){
		std::string exp = "Value(s) not found for '" + key + "'";
		throw std::runtime_error(exp);
	}
	while( it != values.end()){
		dvalues.push_back(my_stod((*it)));
		it++;
	}

	return(dvalues);
}

std::vector<p_range> Property::find_vvproperty(std::string txt, std::string key){
	std::stringstream properties;
	properties << txt;

	std::regex rmulti_values("(\\{[\\d\\s,.-]+\\})");
	std::regex rvalues(R"([\d\.\-]+)");
	std::string values_str;
	std::vector<std::string> values;
	std::vector<std::string> values_tmp;
	std::vector<double> dvalues;
	std::vector<p_range> vvalues;
	std::vector<std::string>::iterator it;
	std::vector<std::string>::iterator it2;
	p_range prange;
	values_str = find_property(txt, key);
	values = this->find_values(values_str, rmulti_values);
	it = values.begin();
	if(it == values.end()){
		std::string exp = "Value(s) not found for '" + key + "'";
		throw std::runtime_error(exp);
	}
	while(it != values.end()){
		values_tmp = this->find_values((*it), rvalues);
		if(values_tmp.size() != 2){
			std::string exp = "Value(s) not found for '" + key + "'";
			throw std::runtime_error(exp);
		}
		prange.p_min = my_stod(values_tmp.at(0));
		prange.p_max = my_stod(values_tmp.at(1));
		vvalues.push_back(prange);
		it++;
	}

	return(vvalues);
}

std::string Property::read_property_file(std::string filename) {
	std::stringstream str;
	std::ifstream stream(filename);
	if(stream.is_open())
	{
		while(stream.peek() != EOF)
		{
			str << (char) stream.get();
		}
		stream.close();
		return str.str();
	}else{
		std::string exp = "File not found : '" + filename + "'";
		throw std::runtime_error(exp);
	}
}

void Property::print_vproperty(std::string key, std::vector<double> values){
	std::vector<double>::iterator it = values.begin();
	std::stringstream str;
	str << key << " : ";
	while(it != values.end()){
		 str << (*it) << ", ";
		 it++;
	}
	str << "\n";
	Logger::info(str.str().c_str());
}

void Property::print_vvproperty(std::string key, std::vector<p_range> values){
	std::vector<p_range>::iterator it = values.begin();
	std::stringstream str;
	str << key << " : ";
	while(it != values.end()){
		 str << "(min: " << it->p_min << ", max: " << it->p_max << ")" << ", ";
		 it++;
	}
	str << "\n";
	Logger::info(str.str().c_str());
}

double Property::my_stod(const std::string &valueAsString) {
	std::istringstream totalSString( valueAsString );
    double valueAsDouble;
    // maybe use some manipulators
    totalSString >> valueAsDouble;
    if(!totalSString)
        throw std::runtime_error("Error converting to double");
    return valueAsDouble;
}

} // namespace dynaland
