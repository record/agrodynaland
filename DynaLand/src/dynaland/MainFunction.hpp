/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#ifndef MAINFUNCTION_H_
#define MAINFUNCTION_H_

#include <string>
#include <dynaland/Map.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/Process.hpp>

using namespace dynaland;

namespace dynaland {

class MainFunction {

public:

	static bool writeDynPop;
	static bool writePatchPop;
	static bool writePop;
	static bool writeVisits;
	static bool writePaths;
	static bool writeDensity;

	static void initFromFile(std::string parameters_file, std::string data_dir,
			Population* pop01, Map* map01, Process* myProcess,
			int* duration, int* individus_last_id);

	static void init(
			Population* pop01, Map* map01, Process* myProcess, int* duration, int* individus_last_id,
			int log_level,
			int pop_id,
			int nb_classes,
			double sex_ratio,
			int sexual_reproduction,
			int pop_sex,

			vector<p_range> dispersion_female,
			const Matrix& survival_female,
			const Matrix& fecondity_female,
			int harem_size_female,

			vector<p_range> dispersion_male,
			const Matrix& survival_male,
			const Matrix& fecondity_male,
			int harem_size_male,

			int dispersion_density_dependent,
			int survival_density_dependent,
			int fecondity_density_dependent,

			double fecondity_contrib_female,
			int perceptual_range,
			int memory_size,
			double persistence_memory,
			double persistence_target,
			int dispersion_path_max_cells,

			const string& file_landcover01, const string& file_nomenclature01, const std::string& file_pop01,

			int seed, int random_type, int dispersion_on, int survival_on, int reproduction_on, std::string dir_results);

	static Population initPopulation(
			   int pop_id,
			   int nb_classes,
			   double sex_ratio,
			   int sexual_reproduction,
			   int pop_sex,

			   vector<p_range> dispersion_female,
			   const Matrix& survival_female,
			   const Matrix& fecondity_female,
			   int harem_size_female,

			   vector<p_range> dispersion_male,
			   const Matrix& survival_male,
			   const Matrix& fecondity_male,
			   int harem_size_male,

			   int dispersion_density_dependent,
			   int survival_density_dependent,
			   int fecondity_density_dependent,

			   double fecondity_contrib_female,
			   int perceptual_range,
			   int memory_size,
			   double persistence_memory,
			   double persistence_target,
			   int path_max_cells);

	static int initIndividus(Map* aMap, const std::string& shape_file, Population* population);
	static Map initMap(Map* aMap, const string& file_landcover01, const string& file_nomenclature01);
	static Process initProcess(Map* aMap, Population* pop, int seed, int random_type, int dispersion_on, int survival_on, int reproduction_on, std::string dir_results);

	static void compute(Population* pop01, Map* map01,
			Process* myProcess, int* individus_last_id);
	static void print_maps(Map* map01);
	static void finish(Population* pop01, Map* map01, Process* myProcess);
	static double get_wtime();
};

} // namespace dynaland;


#endif /* MAINFUNCTION_H_ */
