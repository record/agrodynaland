# -*- coding: utf-8 -*-
"""
/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
 """
from qgis.core import *
from PyQt4.QtCore import QFileInfo
from PyQt4 import QtGui
import os
import shutil
from qgis.utils import iface

#print '__DYNALAND_PYTHON_DIR : ' + __DYNALAND_PYTHON_DIR
execfile(os.path.join(__DYNALAND_PYTHON_DIR, 'layer_style.py'))

def initResultDir():
    wd_path = os.getcwd()
    result_dir = 'results'
    result_path = os.path.join(wd_path, result_dir)

    if os.path.isdir(result_path):
        reply = QtGui.QMessageBox.question(iface.mainWindow(), 'continue?', 
                 'An existing "results" directory will be destroyed, ', QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            shutil.rmtree(result_path)
            os.mkdir(result_path)
    else:
        os.mkdir(result_path)

def runDynaLand():

    initResultDir()
            
    prop_file = './data/Lusignan/properties.ini'
    prop_path = os.path.join(wd_path, prop_file)

    dynaland_cmd = 'DynaLand '
    dynaland_cmd += prop_path
    dynaland_cmd += '> mylog.txt'

    print "cmd : " + dynaland_cmd
    result = os.system(dynaland_cmd)
    print "cmd end : " + str(result)
    
def loadDynaLandResult():
    wd_path = os.getcwd()
    
    # load RASTER
    result_visit_file = "results/visits_pop0.tif"
    result_visit_path = os.path.join(wd_path, result_visit_file)

    print "raster layer : " + result_visit_path
    
    fileInfo = QFileInfo(result_visit_path)
    baseName = fileInfo.baseName()
    
    print "baseName : " + baseName
    rlayer = QgsRasterLayer(result_visit_path, baseName)

    if not rlayer.isValid():
        print "Layer visits failed to load!"
    else:
        print "Layer visits loaded!"
        QgsMapLayerRegistry.instance().addMapLayer(rlayer)

    result_pop_file = 'results/population0.shp'
    result_pop_layer = 'population0'
    result_pop_path = os.path.join(wd_path, result_pop_file)

    # load VECTOR

    print "vector layer : " + result_pop_path

    vlayer = QgsVectorLayer(result_pop_file, result_pop_layer, "ogr")
    if not vlayer.isValid():
        print 'Layer population failed to load!'
    else:
        print 'Layer population loaded!'
        QgsMapLayerRegistry.instance().addMapLayer(vlayer)

def unloadDynaLandResult():
    rlayers = QgsMapLayerRegistry.instance().mapLayersByName('visits_pop0')
    if rlayers:
        rlayer = rlayers[0]
        if rlayer.isValid():
            QgsMapLayerRegistry.instance().removeMapLayer(rlayer)
    
    vlayers = QgsMapLayerRegistry.instance().mapLayersByName('population0')
    if vlayers:
        vlayer = vlayers[0]
        if vlayer.isValid():
            QgsMapLayerRegistry.instance().removeMapLayer(vlayer)

def setColorToDynaLandResult():
    
    rlayers = QgsMapLayerRegistry.instance().mapLayersByName('visits_pop0')
    if rlayers:
        rlayer = rlayers[0]
        if rlayer.isValid():
            setRasterBandColor(rlayer)

    vlayers = QgsMapLayerRegistry.instance().mapLayersByName('population0')
    if vlayers:
        vlayer = vlayers[0]
        if vlayer.isValid():
            setVectorColor(vlayer)
    