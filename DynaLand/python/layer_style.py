# -*- coding: utf-8 -*-
"""
/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
 """
from qgis.core import *
from PyQt4.QtCore import *
from PyQt4.QtGui import *

def setVectorColor(layer):
    symbols = layer.rendererV2().symbols()
    sym = symbols[0]
    sym.setColor(QColor.fromRgb(255,0,0))
    sym.setSize(1)
    layer.setLayerTransparency(40)
    layer.triggerRepaint()

def setRasterBandColor(layer):
    renderer = layer.renderer()
    provider = layer.dataProvider()
    extent = layer.extent()

    ver = provider.hasStatistics(1, QgsRasterBandStats.All)

    stats = provider.bandStatistics(1, QgsRasterBandStats.All,extent, 0)

    if ver is not False:
        print "minimumValue = ", stats.minimumValue

        print "maximumValue = ", stats.maximumValue

    if (stats.minimumValue < 0):
        min = 0  

    else: 
        min= stats.minimumValue

    max = stats.maximumValue
    range = max - min
    add = range//5
    interval = min + add

    # colDic = {'red':'#ff0000', 'yellow':'#ffff00','blue':'#0000ff'}

    colDic = {'0':'#ffffd4', '1':'#fed98e', '2':'#fe9929', '3':'#d95f0e', '4':'#993404'}
    valueList =[min, (min + add), (min + 2 * add), (min + 3 * add), max]

    lst = [ QgsColorRampShader.ColorRampItem(valueList[0], QColor(colDic['0']), str(valueList[0])), 
            QgsColorRampShader.ColorRampItem(valueList[1], QColor(colDic['1']), str(valueList[1])), 
            QgsColorRampShader.ColorRampItem(valueList[2], QColor(colDic['2']), str(valueList[2])), 
            QgsColorRampShader.ColorRampItem(valueList[3], QColor(colDic['3']), str(valueList[3])), 
            QgsColorRampShader.ColorRampItem(valueList[4], QColor(colDic['4']), str(valueList[4]))]

    myRasterShader = QgsRasterShader()
    myColorRamp = QgsColorRampShader()

    myColorRamp.setColorRampItemList(lst)
    myColorRamp.setColorRampType(QgsColorRampShader.INTERPOLATED)
    myRasterShader.setRasterShaderFunction(myColorRamp)

    myPseudoRenderer = QgsSingleBandPseudoColorRenderer(layer.dataProvider(), 
                                                        layer.type(),  
                                                        myRasterShader)
    layer.setRenderer(myPseudoRenderer)
       
    rt = QgsRasterTransparency()
    rt.initializeTransparentPixelList(0)
    layer.renderer().setOpacity(0.8)
    layer.renderer().setRasterTransparency(rt)
    
    layer.triggerRepaint()