# -*- coding: utf-8 -*-
"""
/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
 """
from qgis.core import *
#import qgis.utils
from PyQt4.QtCore import QFileInfo
import os
import sys
import shutil

print 'WORKING DIRECTORY : ' + os.getcwd()

global __DYNALAND_PYTHON_DIR
__DYNALAND_PYTHON_DIR = '/home/grobaldo/.vle/pkgs-1.1/VleDynaLand/python'

print '__DYNALAND_PYTHON_DIR : ' + __DYNALAND_PYTHON_DIR
execfile(os.path.join(__DYNALAND_PYTHON_DIR, 'vledynaland.py'))
#execfile(os.path.join(__DYNALAND_PYTHON_DIR, 'dynaland.py'))

#unloadDynaLandResult()
runVleDynaLand()
loadDynaLandResult()
setColorToDynaLandResult()
