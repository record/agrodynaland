/*******************************************************************************
 *  This file is part of AgroDynaLand, a dynamic model for simulation of species life cycle
 *  and dispersion in heterogeneous and dynamic landscape and interractions with field crops.
 *  http://mulcyber.toulouse.inra.fr/anonscm/git/agrodynaland/agrodynaland.git
 *
 *  Copyright (c) 2017 Guillaume Robaldo <guillaume.robaldo@inra.fr>
 *  Copyright (c) 2017 INRA http://www.inra.fr
 *
 *  See the AUTHORS or Authors.txt file for copyright owners and
 *  contributors
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *******************************************************************************/
#include <dynaland/Logger.hpp>
#include <dynaland/MainFunction.hpp>
#include <dynaland/Map.hpp>
#include <dynaland/Population.hpp>
#include <dynaland/Process.hpp>
#include <dynaland/random/RandomGenerator.hpp>

#include <vle/extension/DifferenceEquation.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Trace.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>

#if defined(_WIN16) | defined(_WIN32) | defined(_WIN64)
#define PATH_SEP "\\"
#else
#define PATH_SEP "/"
#endif

namespace tmp = vle;
namespace vd = vle::devs;
namespace ve = vle::extension;
namespace vv = vle::value;
namespace vu = vle::utils;

using namespace std;
using namespace dynaland;

namespace VleDynaLand {

class DynaLandDynamics : public ve::DifferenceEquation::Multiple
{
public:
	DynaLandDynamics(
       const vd::DynamicsInit& atom,
       const vd::InitEventList& evts)
        : ve::DifferenceEquation::Multiple(atom, evts)
    {
		TraceModel("DynaLand constructing...");
//    	cout << "Running..." << endl;
//    	vu::Trace::setLogFile("test.log");
//    	vu::Trace::send("MetaConnectDyna", vu::TraceLevelOptions::TRACE_LEVEL_MODEL);

    	// PARAMETERS
    	parameters_file = vv::toString(evts.get("parameters_file"));

    	TraceModel("parameters_file : " + parameters_file);

        // VAR
    	int duration = 0;
    	individus_last_id = 0;

    	std::string data_dir = vu::Package("VleDynaLand").getDataDir(vu::VLE_PACKAGE_TYPE::PKG_BINARY);

    	TraceModel("DATA DIR: " + data_dir);

    	std::string param_file =  data_dir + PATH_SEP + parameters_file;

    	TraceModel("param_file : " + param_file);

        MainFunction::initFromFile(param_file, data_dir, &pop01, &map01, &myProcess, &duration, &individus_last_id);

        counter = 0;

        TraceModel("DynaLand constructed!");
    }

    virtual ~DynaLandDynamics()
    {
    	MainFunction::finish(&pop01, &map01, &myProcess);

    	TraceModel("DynaLand destructing...");
    }

virtual void compute(const vd::Time& /*time*/)
{
	counter++;

	Logger::info("----------------------------------\n");
	Logger::info("----- time %d\n\n", counter);

	myProcess.clearResults(&pop01);

	MainFunction::compute(&pop01, &map01, &myProcess, &individus_last_id);

	myProcess.writeResults(counter, &pop01);
	myProcess.writeShapeResults(&map01, &pop01);
}

virtual void initValue(const vd::Time& /*time*/)
{
}

public:
	Process myProcess;
	Map map01;
	Population pop01;
	int individus_last_id;

	// vpz params
	string parameters_file;

	int counter;
};

} // namespace VleDynaLand

DECLARE_DYNAMICS(VleDynaLand::DynaLandDynamics)

